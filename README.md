# **Car Doctors**

# 1. Information :

***The project was created by [Antoan Vassilev](https://gitlab.com/antoanvass) and  [Georgi Dobrilov](https://gitlab.com/Gosharski)***

- Git Issues Board: https://gitlab.com/car-doctors/car-doctors/-/boards

# 2. Used Technologies :

- Language: C# 8
- ASP.NET Core 5.0
- Entity Framework Core
- MS SQL Server
- SQL
- Automapper
- RESTful API
- JWT Token
- HTML
- CSS
- Razor 
- jQuery
- Bootstrap

# 3. Program Functionality :

 - Car Doctors is a web application designed to serve the needs of a car repair service.

 - It supports all the functionalities that the owner of such a garage will need - CRUD operations with vehicles, services that the garage offers and visits to the garage. There is pagination implemented for all the pages in the MVC part and it is supported in the API too. There are plenty of different filters as well.

  - There is an anonymus part where future clients can see the services that the garage offers. The registration of a customer is made by an employee from the admin panel. Once logged in, customers can see all their vehicles and history of their visits to the garage. Employees have a wide range of capabilities - they can perform CRUD operations with all the models in the application.

# 4. How Car Doctors program works : 

Car Doctors provides the clients with a database that stores different collections of elements - users, roles, vehicles, services offered, vehicle models, car manufacturers, vehicle types, visits to the garage and others.

When getting registered, each customers provides the garage with the following Information - first and last name, phone number and email address. The employee registers the user through the admin portal on the register page and the customer recieves and email with the account credentials that he shall use to login. Once logged in, he may change his password by clicking "Change password" on the right part of the navigation bar.

The company's employee can create a vehicle for the user by entering its type (car, motorcycle, bus...), manufacturer, model, year of production, VIN and registration plate. Also, they can create a visit to the garage - they shall provide the server with a vehicle to be repaired, pick-up date, services that will be done to the vehicle and status of the visit (not started, in progress and ready for pick-up).
 
Through his account, every customer can browse between all his cars and his history of visits to the garage. What is more, he may generate a pdf report of certain visit/visits to the garage. The report is sent via email and the customer may request it in differet currencies. The customers can also filter their vehicles and visits by setting up different properties and can personalize the page size, orderby and sort them by model. 

# 5. How to run Car Doctors :

### What things you need to install the software 

 - [Visual Studio 2019](https://visualstudio.microsoft.com/vs/) 
 - [Entity Framework](https://www.nuget.org/packages/EntityFramework/)
 - [SQL Server](https://www.microsoft.com/en-us/sql-server/)

1. Run SQLEXPRESS
2. Run solution: "CarDoctors.sln"
3. Check connection string in CarDoctors.MVC => startup.cs
4. If folder "Migration" is empty add migration (if folder is not empty go to 4.3) from 
* 	Package Manager Console:
* 	4.1 before start typing check "Defaul project to be: CarDoctors.Data".
* 	4.2 type command in PM> "add-migration Initial" (PM> add-migration Initial).
* 	4.3 next command is to create Database "CarDoctors" on your Database Server SQLEXPRESS
* 	  PM> "update-database" (PM> update-database). 

5. Select Start Project in Visual Studio -> Solution Explorer -> 
* 	right click on "CarDoctors.MVC" and choose "Set as Startup Project"

 ### Main advantages discovered

- Simple frontend design
- Follow OOP’s best practices and the SOLID principles
- Implement a security mechanism for managing users and organizers
- The application has appropriate customer and server validations
- Follow the best practices when designing a REST API
- Use ASP.NET Core MVC with Razor as the template engine and bootstrap - CSS framework
- The API is documented with swagger

# 6. Database :

![Database](https://gitlab.com/car-doctors/car-doctors/-/raw/master/CarDoctorsContext.png)


# 7. How to use Car Doctors:

### 1. Public area :

- https://localhost:5001/Home/Index : This is the main page of the website in the public area. From here you can navigate to the following pages:

- https://localhost:5001/Home/AboutUs : By clicking the About Us button you can see information about how to register and what the location of the garage  is.

- https://localhost:5001/Services/Index : The Services button in the navigation bar displays a paginated list with all the services that the company provides. There are plenty of filter options that can be used: by service name, service price. The customer can customize the page by managing the paging options - choosing the page index, size of the page and the property that he wants to sort the page by and the order. The user can choose a currency in which to visualize the page.

### 2. Identyty area :

- https://localhost:5001/Identity/Account/Login : Login button enables users to log in in thier profiles. There is proper error handling that returns user friendly errors. The method returns a token. Users can also invoke the forgotten password button, which will redirect them to another page where they can enter their email and receive a reset password link as an email. An error stating that the email address is invalid will be displayed if the email is not found in the database. The method will redirect the customers to an informational page, if the operation succeeds.

### 3. Customer area :

- https://localhost:5001/Customer/Home/Index : The same page as the public Home page.

- https://localhost:5001/Customer/Home/AboutUs : The same page as the public About Us page.

- https://localhost:5001/Customer/Service : The same page as the public Services page.

- https://localhost:5001/Customer/Vehicle : The vehicles button on the navigation bar displays a paginated list with the customer's registered vehicles. On every item card there is a Details button by which you can see a detailed information about the vehicle in a pop-up model. The user can customize the page by managing the paging options - choosing the page index, size of the page and the property that he wants to sort the page by and the order.

- https://localhost:5001/Customer/Visit : By clicking the Visit button on the navigation bar. A paginated list with the customer's visits to the garage is displayed. On every item card there is a Details button by which you can see detailed information about the visit in a pop-up model. Under the paginated list of items, there is a GeneratePDF report button. By clicking it you get a pop-up model in which we can choose the currency of the report and the visits that we want to include. There are plenty of filter options that can be used: by vehicle id, date from and date to. The user can customize the page by managing the paging options - choosing the page index, size of the page and the property that he wants to sort the page by and the order. 

- https://localhost:5001/Identity/Account/ChangePassword : The Change Password button redirects to a page which provides users with the functionality to change their password. The user should input his old password and two matches of their new password in order to change it. They will be redirected to an informational page if the operation is successful. A proper validation is implemented. 

- https://localhost:5001/Identity/Account/Logout : By clicking the Logout button, the user ends the session; the system logs him out of his account.

### 4. Admin area :

- https://localhost:5001/Admin/Home/Index : The same page as the public Home page.

- https://localhost:5001/Admin/Home/AboutUs : The same page as the public About Us page.

- https://localhost:5001/Admin/Service : By clicking the Services button in the navigation bar. A paginated list with all the services that the company provides. There is a New Service button on the top of the page. By clicking it an empty pop-up model is visualized in which the employee can enter the data for the service - service name and price. On every item there is an Update button. By clicking it the employee can edit the service properties in a pop-up model. There is also a Delete button. By clicking it the employee can delete a service from the database. A pop-up validation is provided to make shure that the employee won't delete a service by accident. There are plenty of filter options that can be admin: by service name, service price. The employee can customize the page by managing the paging options - choosing the page index, size of the page and the property that he wants to sort the page by and the order. The employee can choose a currency in which to visualize the page.

- https://localhost:5001/Admin/Vehicle : The Vehicles button on the navigation bar displays a paginated list with the customer's vehicles registered . On every item card, there is a Details button by which you can see detailed information about the vehicle in a pop-up model. There is a New Vehicle button on the top of the page. After clicking it, an empty pop-up model appears in which the employee can enter the data for the vehicle - vehicle type, manufacturer, model, VIN, owner and registration plate. On every item there is an Update button. By clicking it the employee can edit the vehicle properties in a pop-up model. There is also a Delete button. By clicking it the employee can delete a vehicle from the database. A pop-up validation is provided to make shure that the employee won't delete a vehicle by accident. The employee can customize the page by managing the paging options - choosing the page index, size of the page and the property that he wants to sort the page by and the order.

- https://localhost:5001/Admin/Visit : Clicking the Visit button on the navigation bar displays a paginated list with all the visits to the garage. On every item card there is a Details button by which you can see detailed information about the visit in a pop-up model. There is a New Visit button on the top of the page. Clicking it opens an empty pop-up model in which the employee can enter the data for the visit - pick up date, visit date, vehicle and status of the visit. On every item there is an Update button. By clicking it the employee can edit the visit properties in a pop-up model. There is also a Delete button. By clicking it the employee can delete a visit from the database. A pop-up validation is provided to make shure that the employee won't delete a vehicle by accident. There are plenty of filter options that can be used: by vehicle id, date from and date to. The employee can customize the page by managing the paging options - choosing the page index, size of the page and the property that he wants to sort the page by and the order. 

- https://localhost:5001/Admin/Customer : Clicking the Users button on the navigation bar displays a paginated list with all the users registered in the database. On every row there is information about the user (id, first name, last name, email and phone number). There is also an Update button by which the employee can update the properties of the user model. There is a Delete button - by clicking it, an employee can delete a user from the database. A pop-up validation is provided to make shure that the employee won't delete a user by accident. There are plenty of filter options that can be used: first name, last name, email, phone number, vehicle manufacturer, vehicle model, vehicle type, visit from date and visit to date. The employee can customize the page by managing the paging options - choosing the page index, size of the page and the property that he wants to sort the page by and the order. 

- https://localhost:5001/Identity/Account/ChangePassword : Clicking the Change Password button allows users to change their password. The user should enter his old password and two matches of a new password in order to change it. A redirection to an informational page occurs if the operation is successful. A proper validation is implemented. 

- https://localhost:5001/Identity/Account/Register : By clicking the Register button, the employee can create an account for a customer of the garage. A new page opens where the employee can enter the customer's information (first name, last name, email and phone number). An automatic email is send to the customer with auto-generated password. The employee is redirected to the home page. 

- https://localhost:5001/Identity/Account/Logout : Clicking the Logout button ends the session for a user and logs him out of his account.

### 4. Used APIs:

- https://free.currconv.com : Used for converting currency 
- Google Maps API - for the map on the About Us page




