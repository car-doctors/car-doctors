﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace CarDoctors.Data.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Manufacturers",
                columns: table => new
                {
                    ManufacturerID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Manufacturers", x => x.ManufacturerID);
                });

            migrationBuilder.CreateTable(
                name: "ResetPasses",
                columns: table => new
                {
                    ResetID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    Time = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Guid = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResetPasses", x => x.ResetID);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    RoleID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.RoleID);
                });

            migrationBuilder.CreateTable(
                name: "Services",
                columns: table => new
                {
                    ServiceID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ServiceName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Price = table.Column<double>(type: "float", nullable: false),
                    Currency = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Services", x => x.ServiceID);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Username = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VehicleTypes",
                columns: table => new
                {
                    VehicleTypeID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false),
                    Picture = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleTypes", x => x.VehicleTypeID);
                });

            migrationBuilder.CreateTable(
                name: "VisitStatuses",
                columns: table => new
                {
                    StatusID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VisitStatuses", x => x.StatusID);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    RoleID = table.Column<int>(type: "int", nullable: false),
                    UserID = table.Column<int>(type: "int", nullable: false),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => new { x.UserID, x.RoleID });
                    table.ForeignKey(
                        name: "FK_UserRoles_Roles_RoleID",
                        column: x => x.RoleID,
                        principalTable: "Roles",
                        principalColumn: "RoleID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserRoles_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VehicleModels",
                columns: table => new
                {
                    VehicleModelID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ManufacturerID = table.Column<int>(type: "int", nullable: false),
                    Model = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    VehicleTypeID = table.Column<int>(type: "int", nullable: false),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleModels", x => x.VehicleModelID);
                    table.ForeignKey(
                        name: "FK_VehicleModels_Manufacturers_ManufacturerID",
                        column: x => x.ManufacturerID,
                        principalTable: "Manufacturers",
                        principalColumn: "ManufacturerID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleModels_VehicleTypes_VehicleTypeID",
                        column: x => x.VehicleTypeID,
                        principalTable: "VehicleTypes",
                        principalColumn: "VehicleTypeID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Vehicles",
                columns: table => new
                {
                    VehicleID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    VIN = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    VehicleModelID = table.Column<int>(type: "int", nullable: false),
                    OwnerID = table.Column<int>(type: "int", nullable: false),
                    RegistrationPlate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicles", x => x.VehicleID);
                    table.ForeignKey(
                        name: "FK_Vehicles_Users_OwnerID",
                        column: x => x.OwnerID,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Vehicles_VehicleModels_VehicleModelID",
                        column: x => x.VehicleModelID,
                        principalTable: "VehicleModels",
                        principalColumn: "VehicleModelID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "VehicleServices",
                columns: table => new
                {
                    VehicleID = table.Column<int>(type: "int", nullable: false),
                    ServiceID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleServices", x => new { x.VehicleID, x.ServiceID });
                    table.ForeignKey(
                        name: "FK_VehicleServices_Services_ServiceID",
                        column: x => x.ServiceID,
                        principalTable: "Services",
                        principalColumn: "ServiceID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleServices_Vehicles_VehicleID",
                        column: x => x.VehicleID,
                        principalTable: "Vehicles",
                        principalColumn: "VehicleID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Visits",
                columns: table => new
                {
                    VisitID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    VehicleID = table.Column<int>(type: "int", nullable: false),
                    VisitDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PickUpDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    VisitStatusID = table.Column<int>(type: "int", nullable: false),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Visits", x => x.VisitID);
                    table.ForeignKey(
                        name: "FK_Visits_Vehicles_VehicleID",
                        column: x => x.VehicleID,
                        principalTable: "Vehicles",
                        principalColumn: "VehicleID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Visits_VisitStatuses_VisitStatusID",
                        column: x => x.VisitStatusID,
                        principalTable: "VisitStatuses",
                        principalColumn: "StatusID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VisitServices",
                columns: table => new
                {
                    VisitID = table.Column<int>(type: "int", nullable: false),
                    ServiceID = table.Column<int>(type: "int", nullable: false),
                    isDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VisitServices", x => new { x.VisitID, x.ServiceID });
                    table.ForeignKey(
                        name: "FK_VisitServices_Services_ServiceID",
                        column: x => x.ServiceID,
                        principalTable: "Services",
                        principalColumn: "ServiceID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VisitServices_Visits_VisitID",
                        column: x => x.VisitID,
                        principalTable: "Visits",
                        principalColumn: "VisitID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Manufacturers",
                columns: new[] { "ManufacturerID", "Name", "isDeleted" },
                values: new object[,]
                {
                    { 1, "BMW", false },
                    { 2, "Peugeot", false }
                });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "RoleID", "Name", "isDeleted" },
                values: new object[,]
                {
                    { 1, "Admin", false },
                    { 2, "Customer", false },
                    { 3, "Employee", false }
                });

            migrationBuilder.InsertData(
                table: "Services",
                columns: new[] { "ServiceID", "Currency", "Price", "ServiceName", "isDeleted" },
                values: new object[,]
                {
                    { 1, "BGN", 100.0, "Oil change", false },
                    { 2, "BGN", 35.0, "Oil filter replacement (diesel engines)", false },
                    { 3, "BGN", 30.0, "Air filter replacement", false },
                    { 4, "BGN", 25.0, "Wipers replacement", false },
                    { 5, "BGN", 175.0, "Car battery replacement", false },
                    { 6, "BGN", 175.0, "Car battery replacement", false },
                    { 7, "BGN", 37.5, "New spark plugs (petrol engines)", false }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Email", "FirstName", "LastName", "PasswordHash", "Phone", "Username", "isDeleted" },
                values: new object[,]
                {
                    { 2, "gd@gmail.com", "Georgi", "Dobrilov", "1rCXsVvLFWVkEy4ijv0EHhd6RVjIvhOVinpoRD0RdLaglgpzYm0yI3Ht7df2tAbw", "0878173167", "gd@gmail.com", false },
                    { 1, "admin@admin.com", "Admin", "Admin", "kyLnu0B+rQkvsI7wm4KiQ3tPBJaEHFxcdOZ8IxmzUUBXpAXyXEVWc+GZN2CrT221", "0888888888", "admin@admin.com", false }
                });

            migrationBuilder.InsertData(
                table: "VehicleTypes",
                columns: new[] { "VehicleTypeID", "Name", "Picture", "isDeleted" },
                values: new object[,]
                {
                    { 1, "Car", "/assets/Vehicles/Car.jpg", false },
                    { 2, "Suv", "/assets/Vehicles/Suv.jpg", false },
                    { 3, "Truck", "/assets/Vehicles/Truck.jpg", false },
                    { 4, "Motorcycle", "/assets/Vehicles/Motorcycle.jpg", false },
                    { 5, "Big Truck", "/assets/Vehicles/Bigtruck.jpg", false },
                    { 6, "Campervan", "/assets/Vehicles/Campervan.jpg", false },
                    { 7, "Cargo", "/assets/Vehicles/Cargo.jpg", false },
                    { 8, "Van", "/assets/Vehicles/Van.jpg", false }
                });

            migrationBuilder.InsertData(
                table: "VisitStatuses",
                columns: new[] { "StatusID", "Name", "isDeleted" },
                values: new object[,]
                {
                    { 1, "Not started", false },
                    { 2, "In progress", false },
                    { 3, "Ready for pickup", false }
                });

            migrationBuilder.InsertData(
                table: "UserRoles",
                columns: new[] { "RoleID", "UserID", "isDeleted" },
                values: new object[,]
                {
                    { 1, 1, false },
                    { 2, 2, false }
                });

            migrationBuilder.InsertData(
                table: "VehicleModels",
                columns: new[] { "VehicleModelID", "ManufacturerID", "Model", "VehicleTypeID", "isDeleted" },
                values: new object[,]
                {
                    { 1, 1, "330", 1, false },
                    { 2, 2, "207", 1, false }
                });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "VehicleID", "OwnerID", "RegistrationPlate", "VIN", "VehicleModelID", "isDeleted" },
                values: new object[] { 1, 2, "BP7777BP", "WBSBL93435PN07730", 1, false });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "VehicleID", "OwnerID", "RegistrationPlate", "VIN", "VehicleModelID", "isDeleted" },
                values: new object[] { 2, 2, "BP8888BP", "5XYKTDA14BG124123", 2, false });

            migrationBuilder.InsertData(
                table: "Visits",
                columns: new[] { "VisitID", "PickUpDate", "VehicleID", "VisitDate", "VisitStatusID", "isDeleted" },
                values: new object[] { 1, new DateTime(2021, 6, 14, 10, 28, 0, 221, DateTimeKind.Local).AddTicks(4569), 1, new DateTime(2021, 6, 9, 10, 28, 0, 219, DateTimeKind.Local).AddTicks(3260), 1, false });

            migrationBuilder.InsertData(
                table: "Visits",
                columns: new[] { "VisitID", "PickUpDate", "VehicleID", "VisitDate", "VisitStatusID", "isDeleted" },
                values: new object[] { 2, new DateTime(2021, 6, 16, 10, 28, 0, 221, DateTimeKind.Local).AddTicks(5636), 2, new DateTime(2021, 6, 9, 10, 28, 0, 221, DateTimeKind.Local).AddTicks(5620), 2, false });

            migrationBuilder.InsertData(
                table: "VisitServices",
                columns: new[] { "ServiceID", "VisitID", "isDeleted" },
                values: new object[] { 1, 1, false });

            migrationBuilder.InsertData(
                table: "VisitServices",
                columns: new[] { "ServiceID", "VisitID", "isDeleted" },
                values: new object[] { 2, 2, false });

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_RoleID",
                table: "UserRoles",
                column: "RoleID");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleModels_ManufacturerID",
                table: "VehicleModels",
                column: "ManufacturerID");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleModels_VehicleTypeID",
                table: "VehicleModels",
                column: "VehicleTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_OwnerID",
                table: "Vehicles",
                column: "OwnerID");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_VehicleModelID",
                table: "Vehicles",
                column: "VehicleModelID");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleServices_ServiceID",
                table: "VehicleServices",
                column: "ServiceID");

            migrationBuilder.CreateIndex(
                name: "IX_Visits_VehicleID",
                table: "Visits",
                column: "VehicleID");

            migrationBuilder.CreateIndex(
                name: "IX_Visits_VisitStatusID",
                table: "Visits",
                column: "VisitStatusID");

            migrationBuilder.CreateIndex(
                name: "IX_VisitServices_ServiceID",
                table: "VisitServices",
                column: "ServiceID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ResetPasses");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "VehicleServices");

            migrationBuilder.DropTable(
                name: "VisitServices");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Services");

            migrationBuilder.DropTable(
                name: "Visits");

            migrationBuilder.DropTable(
                name: "Vehicles");

            migrationBuilder.DropTable(
                name: "VisitStatuses");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "VehicleModels");

            migrationBuilder.DropTable(
                name: "Manufacturers");

            migrationBuilder.DropTable(
                name: "VehicleTypes");
        }
    }
}
