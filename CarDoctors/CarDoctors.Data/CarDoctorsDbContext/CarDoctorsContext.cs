﻿using CarDoctors.Data.Configurations;
using CarDoctors.Data.Helpers;
using CarDoctors.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CarDoctors.Data.CarDoctorsDbContext
{
    public class CarDoctorsContext : DbContext
    {
        public CarDoctorsContext(DbContextOptions<CarDoctorsContext> options)
           : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<UserRole> UserRoles { get; set; }

        public DbSet<Manufacturer> Manufacturers { get; set; }

        public DbSet<Service> Services { get; set; }

        public DbSet<Vehicle> Vehicles { get; set; }

        public DbSet<VehicleModel> VehicleModels { get; set; }

        public DbSet<VehicleService> VehicleServices { get; set; }

        public DbSet<VehicleType> VehicleTypes { get; set; }

        public DbSet<Visit> Visits { get; set; }

        public DbSet<VisitService> VisitServices { get; set; }

        public DbSet<VisitStatus> VisitStatuses { get; set; }

        public DbSet<ResetPass> ResetPasses { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfig());

            modelBuilder.Entity<UserRole>().HasKey(cs => new { cs.UserID, cs.RoleID });

            modelBuilder.Entity<UserRole>().HasOne(c => c.User)
                .WithMany(o => o.UserRoles)
                .HasForeignKey(c => c.UserID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<UserRole>().HasOne(c => c.Role)
                .WithMany(o => o.UserRoles)
                .HasForeignKey(c => c.RoleID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<VehicleModel>().HasOne(c => c.Manufacturer)
                .WithMany(c => c.VehicleModels)
                .HasForeignKey(c => c.ManufacturerID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Vehicle>().HasOne(c => c.VehicleModel)
                .WithMany(c => c.Vehicles)
                .HasForeignKey(c => c.VehicleModelID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Vehicle>().HasOne(c => c.Owner)
                .WithMany(o => o.Vehicles)
                .HasForeignKey(c => c.OwnerID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Visit>().HasOne(c => c.Vehicle)
                .WithMany(o => o.Visits)
                .HasForeignKey(c => c.VehicleID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<VehicleService>().HasKey(cs => new { cs.VehicleID, cs.ServiceID });

            modelBuilder.Entity<VehicleService>().HasOne(c => c.Vehicle)
                .WithMany(o => o.VehicleServices)
                .HasForeignKey(c => c.VehicleID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<VehicleService>().HasOne(c => c.Service)
                .WithMany(o => o.VehicleServices)
                .HasForeignKey(c => c.ServiceID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<VisitService>().HasKey(vs => new { vs.VisitID, vs.ServiceID });

            modelBuilder.Entity<VisitService>().HasOne(v => v.Visit)
                .WithMany(o => o.VisitServices)
                .HasForeignKey(c => c.VisitID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<VisitService>().HasOne(v => v.Service)
                .WithMany(o => o.VisitServices)
                .HasForeignKey(c => c.ServiceID)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Manufacturer>().Property<bool>("isDeleted");
            modelBuilder.Entity<Manufacturer>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Entity<Service>().Property<bool>("isDeleted");
            modelBuilder.Entity<Service>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Entity<User>().Property<bool>("isDeleted");
            modelBuilder.Entity<User>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Entity<UserRole>().Property<bool>("isDeleted");
            modelBuilder.Entity<UserRole>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Entity<Role>().Property<bool>("isDeleted");
            modelBuilder.Entity<Role>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Entity<Vehicle>().Property<bool>("isDeleted");
            modelBuilder.Entity<Vehicle>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Entity<VehicleModel>().Property<bool>("isDeleted");
            modelBuilder.Entity<VehicleModel>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Entity<VehicleType>().Property<bool>("isDeleted");
            modelBuilder.Entity<VehicleType>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Entity<Visit>().Property<bool>("isDeleted");
            modelBuilder.Entity<Visit>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Entity<ResetPass>().Property<bool>("isDeleted");
            modelBuilder.Entity<ResetPass>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            this.Seed(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        /// <summary>
        /// Overriding SaveChanges so that soft delete is implemented
        /// </summary>
        /// <returns></returns>
        public override int SaveChanges()
        {
            UpdateSoftDeleteStatuses();

            return base.SaveChanges();
        }

        /// <summary>
        /// Overriding SaveChangesAsync so that soft delete is implemented
        /// </summary>
        /// <param name="acceptAllChangesOnSuccess"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        /// <summary>
        /// Updates the "isDeleted" property so that it is set to false, if the object is created, or true, if the object is deleted
        /// </summary>
        private void UpdateSoftDeleteStatuses()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.CurrentValues["isDeleted"] = false;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        entry.CurrentValues["isDeleted"] = true;
                        break;
                }
            }
        }

        /// <summary>
        /// Seed data to the database
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected virtual void Seed(ModelBuilder modelBuilder)
        {
            // Roles
            modelBuilder.Entity<Role>().HasData(
                new Role() { RoleID = 1, Name = "Admin" },
                new Role() { RoleID = 2, Name = "Customer" },
                new Role() { RoleID = 3, Name = "Employee" }
                );

            // Password hasher
            var adminPass = PasswordHasher.HashPassword("Admin123");

            // Admin
            var adminUser = new User();
            adminUser.Id = 1;
            adminUser.FirstName = "Admin";
            adminUser.LastName = "Admin";
            adminUser.Username = "admin@admin.com";
            adminUser.PasswordHash = adminPass;
            adminUser.Phone = "0888888888";
            adminUser.Email = "admin@admin.com";
            modelBuilder.Entity<User>().HasData(adminUser);

            // Link Role & User (for Admin)
            var adminUserRole = new UserRole();
            adminUserRole.RoleID = 1;
            adminUserRole.UserID = adminUser.Id;
            modelBuilder.Entity<UserRole>().HasData(adminUserRole);

            var customerPass = PasswordHasher.HashPassword("Georgi00");

            // Customer
            var customerUser = new User();
            customerUser.Id = 2;
            customerUser.FirstName = "Georgi";
            customerUser.LastName = "Dobrilov";
            customerUser.Username = "gd@gmail.com";
            customerUser.PasswordHash = customerPass;
            customerUser.Phone = "0878173167";
            customerUser.Email = "gd@gmail.com";
            modelBuilder.Entity<User>().HasData(customerUser);

            // Link Role & User (for Admin)
            var cutomerUserRole = new UserRole();
            cutomerUserRole.RoleID = 2;
            cutomerUserRole.UserID = customerUser.Id;
            modelBuilder.Entity<UserRole>().HasData(cutomerUserRole);

            //Statuses
            var statuses = new List<VisitStatus>
            {
                new VisitStatus
                {
                    StatusID = 1,
                    Name = "Not started"
                },
                new VisitStatus
                {
                    StatusID = 2,
                    Name = "In progress"
                },
                new VisitStatus
                {
                    StatusID = 3,
                    Name = "Ready for pickup"
                }
            };

            modelBuilder.Entity<VisitStatus>().HasData(statuses);

            //Vehicle Types
            var vehicleTypes = new List<VehicleType>
            {
                new VehicleType
                {
                    VehicleTypeID = 1,
                    Name = "Car",
                    Picture = "/assets/Vehicles/Car.jpg"
                },
                new VehicleType
                {
                    VehicleTypeID = 2,
                    Name = "Suv",
                    Picture = "/assets/Vehicles/Suv.jpg"
                },
                new VehicleType
                {
                    VehicleTypeID = 3,
                    Name = "Truck",
                    Picture = "/assets/Vehicles/Truck.jpg"
                },
                new VehicleType
                {
                    VehicleTypeID = 4,
                    Name = "Motorcycle",
                    Picture = "/assets/Vehicles/Motorcycle.jpg"
                },
                new VehicleType
                {
                    VehicleTypeID = 5,
                    Name = "Big Truck",
                    Picture = "/assets/Vehicles/Bigtruck.jpg"
                },
                new VehicleType
                {
                    VehicleTypeID = 6,
                    Name = "Campervan",
                    Picture = "/assets/Vehicles/Campervan.jpg"
                },
                new VehicleType
                {
                    VehicleTypeID = 7,
                    Name = "Cargo",
                    Picture = "/assets/Vehicles/Cargo.jpg"
                },
                new VehicleType
                {
                    VehicleTypeID = 8,
                    Name = "Van",
                    Picture = "/assets/Vehicles/Van.jpg"
                }
            };

            modelBuilder.Entity<VehicleType>().HasData(vehicleTypes);

            //Vehicles
            var vehicles = new List<Vehicle>
            {
                new Vehicle()
                {
                    VehicleID = 1,
                    VIN = "WBSBL93435PN07730",
                    VehicleModelID = 1,
                    OwnerID =2,
                    RegistrationPlate = "BP7777BP"
                },
                new Vehicle()
                {
                    VehicleID = 2,
                    VIN = "5XYKTDA14BG124123",
                    VehicleModelID = 2,
                    OwnerID =2,
                    RegistrationPlate = "BP8888BP"
                }
            };

            modelBuilder.Entity<Vehicle>().HasData(vehicles);

            //VehicleModels
            var vehicleModels = new List<VehicleModel>
            {
                new VehicleModel()
                {
                    VehicleModelID = 1,
                    ManufacturerID = 1,
                    Model = "330",
                    VehicleTypeID = 1
                },
                new VehicleModel()
                {
                    VehicleModelID = 2,
                    ManufacturerID = 2,
                    Model = "207",
                    VehicleTypeID = 1
                }
            };

            modelBuilder.Entity<VehicleModel>().HasData(vehicleModels);

            //Manufacturers
            var manufacturers = new List<Manufacturer>
            {
                new Manufacturer()
                {
                    ManufacturerID = 1,
                    Name = "BMW"
                },
                new Manufacturer()
                {
                    ManufacturerID = 2,
                    Name = "Peugeot"
                }
            };

            modelBuilder.Entity<Manufacturer>().HasData(manufacturers);

            // Services
            var services = new List<Service>
            {
                new Service()
                {
                    ServiceID = 1,
                    ServiceName = "Oil change",
                    Price = 100.00,
                    Currency = "BGN",
                },
                new Service()
                {
                    ServiceID = 2,
                    ServiceName = "Oil filter replacement (diesel engines)",
                    Price = 35.00,
                    Currency = "BGN"
                },
                new Service()
                {
                    ServiceID = 3,
                    ServiceName = "Air filter replacement",
                    Price = 30.00,
                    Currency = "BGN"
                },
                new Service()
                {
                    ServiceID = 4,
                    ServiceName = "Wipers replacement",
                    Price = 25.00,
                    Currency = "BGN"
                },
                new Service()
                {
                    ServiceID = 5,
                    ServiceName = "Car battery replacement",
                    Price = 175.00,
                    Currency = "BGN"
                },
                new Service()
                {
                    ServiceID = 6,
                    ServiceName = "Car battery replacement",
                    Price = 175.00,
                    Currency = "BGN"
                },
                new Service()
                {
                    ServiceID = 7,
                    ServiceName = "New spark plugs (petrol engines)",
                    Price = 37.50,
                    Currency = "BGN"
                }
            };
            modelBuilder.Entity<Service>().HasData(services);

            // Visits
            var visits = new List<Visit>
            {
                new Visit()
                {
                    VisitID =1,
                    VehicleID = 1,
                    VisitDate = DateTime.Now,
                    PickUpDate = DateTime.Now.AddDays(5),
                    VisitStatusID = 1,
                },
                new Visit()
                {
                    VisitID =2,
                    VehicleID = 2,
                    VisitDate = DateTime.Now,
                    PickUpDate = DateTime.Now.AddDays(7),
                    VisitStatusID = 2,
                },

            };
            modelBuilder.Entity<Visit>().HasData(visits);

            // VisitServices
            var visitServices = new List<VisitService>
            {
                new VisitService()
                {
                    VisitID =1,
                    ServiceID = 1,
                },
                 new VisitService()
                {
                    VisitID =2,
                    ServiceID = 2,
                }
            };
            modelBuilder.Entity<VisitService>().HasData(visitServices);
        }
    }
}
