﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Data.Models
{
    public class VisitStatus
    {
        [Key, Required]
        public int StatusID { get; set; }

        [Required]
        public string Name { get; set; }

        public List<Visit> Visits { get; set; }

        public bool isDeleted { get; set; }
    }
}
