﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace CarDoctors.Data.Models
{
    public class Service
    {
        [Key]
        public int ServiceID { get; set; }

        public string ServiceName { get; set; }

        [Required, Range(0, int.MaxValue, ErrorMessage = "Price must be between {1} and {2}.")]
        public double Price { get; set; }

        public string Currency { get; set; }

        public bool isDeleted { get; set; }

        public List<VehicleService> VehicleServices { get; set; }

        public List<VisitService> VisitServices { get; set; }
    }
}
