﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace CarDoctors.Data.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public List<UserRole> UserRoles { get; set; }


        [JsonIgnore]
        public string PasswordHash { get; set; }

        public bool isDeleted { get; set; }

        public List<Vehicle> Vehicles { get; set; }
    }
}
