﻿namespace CarDoctors.Data.Models
{
    public class VisitService
    {
        public int VisitID { get; set; }

        public Visit Visit { get; set; }

        public int ServiceID { get; set; }

        public Service Service { get; set; }

        public bool isDeleted { get; set; }
    }
}