﻿namespace CarDoctors.Data.Models.Mail
{
    public class MailRequest
    {
        public string ToEmail { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public string FileName { get; set; }
    }
}
