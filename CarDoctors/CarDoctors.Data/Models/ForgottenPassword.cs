﻿namespace CarDoctors.Data.Models
{
    public class ForgottenPassword
    {
        public string Email { get; set; }
    }
}
