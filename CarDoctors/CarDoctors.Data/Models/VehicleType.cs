﻿using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Data.Models
{
    public class VehicleType
    {
        [Key, Required]
        public int VehicleTypeID { get; set; }
        [Required]
        public string Name { get; set; }
        public bool isDeleted { get; set; }
        public string Picture { get; set; }
    }
}
