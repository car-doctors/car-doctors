﻿namespace CarDoctors.Data.Models
{
    public class UserRole
    {
        public int RoleID { get; set; }

        public Role Role { get; set; }

        public int UserID { get; set; }

        public User User { get; set; }

        public bool isDeleted { get; set; }
    }
}
