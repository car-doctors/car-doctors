﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Data.Models
{
    public class ResetPass
    {
        [Key]
        public int ResetID { get; set; }

        public int UserId { get; set; }

        public DateTime Time { get; set; }

        public Guid Guid { get; set; }

        public bool isDeleted { get; set; }
    }
}
