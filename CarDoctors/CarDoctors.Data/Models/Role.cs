﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Data.Models
{
    public class Role
    {
        [Key]
        public int RoleID { get; set; }

        public string Name { get; set; }

        public List<UserRole> UserRoles { get; set; }

        public bool isDeleted { get; set; }
    }
}
