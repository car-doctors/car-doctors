﻿namespace CarDoctors.Data.Models
{
    public class VehicleService
    {
        public int VehicleID { get; set; }

        public Vehicle Vehicle { get; set; }

        public int ServiceID { get; set; }

        public Service Service { get; set; }
    }
}
