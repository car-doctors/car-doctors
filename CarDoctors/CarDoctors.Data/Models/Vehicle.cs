﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Data.Models
{
    public class Vehicle
    {
        [Key]
        public int VehicleID { get; set; }

        [RegularExpression("[A-HJ-NPR-Z0-9]{13}[0-9]{4}", ErrorMessage = "Invalid Vehicle Identification Number Format.")]
        public string VIN { get; set; }

        public int VehicleModelID { get; set; }

        public VehicleModel VehicleModel { get; set; }

        public int OwnerID { get; set; }

        public User Owner { get; set; }

        [RegularExpression("(?:(?:E|A|B|BT|BH|BP|EB|TX|K|KH|OB|M|PA|PK|EH|PB|PP|P|CC|CH|CM|CO|C|CB|CA|CT|T|X|H|Y)[0-9]{4}(?:E|A|B|H|P|X|K|O|C|T|Y){1,2})|(?:E|A|B|BT|BH|BP|EB|TX|K|KH|OB|M|PA|PK|EH|PB|PP|P|CC|CH|CM|CO|C|CB|CA|CT|T|X|H|Y)[0-9]{5,6})|(?:E|A|B|BT|BH|BP|EB|TX|K|KH|OB|M|PA|PK|EH|PB|PP|P|CC|CH|CM|CO|C|CB|CA|CT|T|X|H|Y)(?:E|A|B|H|P|X|K|O|C|T|Y){5,6})", ErrorMessage = "Invalid Registration Plate Number")]
        public string RegistrationPlate { get; set; }

        public bool isDeleted { get; set; }

        public List<VehicleService> VehicleServices { get; set; }

        public List<Visit> Visits { get; set; }
    }
}
