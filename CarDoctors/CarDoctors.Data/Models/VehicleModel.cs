﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Data.Models
{
    public class VehicleModel
    {
        [Key, Required]
        public int VehicleModelID { get; set; }

        public List<Vehicle> Vehicles { get; set; }

        public int ManufacturerID { get; set; }

        public Manufacturer Manufacturer { get; set; }

        public string Model { get; set; }

        [Required]
        public int VehicleTypeID { get; set; }

        public VehicleType VehicleType { get; set; }

        public bool isDeleted { get; set; }
    }
}
