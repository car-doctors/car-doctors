﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Data.Models
{
    public class Manufacturer
    {
        [Key]
        public int ManufacturerID { get; set; }

        public string Name { get; set; }

        public bool isDeleted { get; set; }

        public List<VehicleModel> VehicleModels { get; set; }
    }
}
