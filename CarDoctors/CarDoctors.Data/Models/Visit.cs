﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Data.Models
{
    public class Visit
    {
        [Key, Required]
        public int VisitID { get; set; }

        public int VehicleID { get; set; }

        public Vehicle Vehicle { get; set; }

        public List<VisitService> VisitServices { get; set; }

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime VisitDate { get; set; }

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime PickUpDate { get; set; }

        [Required]
        public int VisitStatusID { get; set; }

        public VisitStatus VisitStatus { get; set; }

        public bool isDeleted { get; set; }
    }
}
