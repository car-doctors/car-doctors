﻿using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Data.Models;
using CarDoctors.Services.Models.FilterModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;


namespace CarDoctors.Tests
{
    public class Utils
    {
        public static DbContextOptions<CarDoctorsContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<CarDoctorsContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }
        public static List<VisitStatus> GetVisitStatuses()
        {
            return new List<VisitStatus>()
            {
                new VisitStatus
                {
                    StatusID = 1,
                    Name = "Not started"
                },
                new VisitStatus
                {
                    StatusID = 2,
                    Name = "In progress"
                },
                new VisitStatus
                {
                    StatusID = 3,
                    Name = "Ready for pickup"
                }
            };
        }

        public static List<VehicleType> GetVehicleTypes()
        {
            return new List<VehicleType>()
            {
                 new VehicleType
                {
                    VehicleTypeID = 1,
                    Name = "Car",
                },
                new VehicleType
                {
                    VehicleTypeID = 2,
                    Name = "Bus",
                },
                new VehicleType
                {
                    VehicleTypeID = 3,
                    Name = "Truck",
                },
                new VehicleType
                {
                    VehicleTypeID = 4,
                    Name = "Motorcycle",
                }
            };
        }

        public static List<Vehicle> GetVehicles()
        {
            return new List<Vehicle>()
            {
                 new Vehicle()
                {
                    VehicleID = 1,
                    VIN = "WBSBL93435PN07730",
                    VehicleModelID = 1,
                    OwnerID =1,
                    RegistrationPlate = "BP7777BP"
                },
                new Vehicle()
                {
                    VehicleID = 2,
                    VIN = "5XYKTDA14BG124123",
                    VehicleModelID = 2,
                    OwnerID =1,
                    RegistrationPlate = "BP8888BP"
                }
            };
        }

        public static List<VehicleModel> GetVehicleModels()
        {
            return new List<VehicleModel>()
            {
                new VehicleModel()
                {
                    VehicleModelID = 1,
                    ManufacturerID = 1,
                    Model = "330",
                    VehicleTypeID = 1
                },
                new VehicleModel()
                {
                    VehicleModelID = 2,
                    ManufacturerID = 2,
                    Model = "207",
                    VehicleTypeID = 1
                }
            };
        }

        public static List<Manufacturer> GetManufacturers()
        {
            return new List<Manufacturer>()
            {
                 new Manufacturer()
                {
                    ManufacturerID = 1,
                    Name = "BMW"
                },
                new Manufacturer()
                {
                    ManufacturerID = 2,
                    Name = "Peugeot"
                }
            };
        }

        public static List<Service> GetServices()
        {
            return new List<Service>()
            {
                new Service()
                {
                    ServiceID = 1,
                    ServiceName = "Oil change",
                    Price = 100.00,
                    Currency = "BGN",
                },
                new Service()
                {
                    ServiceID = 2,
                    ServiceName = "Air filter change",
                    Price = 50.00,
                    Currency = "BGN"
                }
            };
        }

        public static List<Visit> GetVisits()
        {
            return new List<Visit>()
            {
               new Visit()
                {
                    VisitID = 1,
                    VehicleID = 1,
                    VisitDate = DateTime.Now,
                    PickUpDate = DateTime.Now.AddDays(5),
                    VisitStatusID = 1,
                },
                new Visit()
                {
                    VisitID = 2,
                    VehicleID = 2,
                    VisitDate = DateTime.Now.AddDays(-10),
                    PickUpDate = DateTime.Now.AddDays(7),
                    VisitStatusID = 2,
                },
            };
        }

        public static List<VisitService> GetVisitServices()
        {
            return new List<VisitService>()
            {
                 new VisitService()
                {
                    VisitID = 1,
                    ServiceID = 1,
                },
                 new VisitService()
                {
                    VisitID = 1,
                    ServiceID = 2,
                }
            };
        }

        public static List<FilterServices> GetFilterParcel()
        {
            return new List<FilterServices>()
            {
                new FilterServices
                {
                    Name = "Oil change",
                    PriceFrom = 1,
                    PriceTo = 200,
                    PriceIs = 100
                }
            };
        }

        public static List<User> GetUsers()
        {
            return new List<User>()
            {
                new User
                {
                    Id = 1,
                    FirstName = "Georgi",
                    LastName = "Dobrilov",
                    Email = "g.d@abv.bg",
                    Username = "g.d@abv.bg",
                    Phone = "0878173167",
                    PasswordHash = "qE2vIsTjxppOreWdGMllHqKYTkxNZCjam9xZ7hdMwNReFCumP+doVkYQFnqKA08j"
                 }
              };
        }

        public static List<UserRole> GetUserRoles()
        {
            return new List<UserRole>()
            {
                new UserRole
                {
                    UserID = 1,
                    RoleID = 2
                }
            };
        }

        public static List<Role> GetRoles()
        {
            return new List<Role>()
            {
                new Role
                {
                    RoleID = 2,
                    Name = "Cutomer"
                }
            };
        }
    }
}
