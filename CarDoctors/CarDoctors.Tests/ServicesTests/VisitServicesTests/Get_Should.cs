﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Data.Models;
using CarDoctors.MVC.Infrastructure;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarDoctors.Tests.ServicesTests.VisitServicesTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public async Task ReturnCorrectVisit()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectVisit));

            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var visitStatuses = Utils.GetVisitStatuses();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var services = Utils.GetServices();

            var visits = new List<Visit>()
            {
                new Visit()
                {
                    VisitID =1,
                    VehicleID = 1,
                    VisitDate = DateTime.Now,
                    PickUpDate = DateTime.Now.AddDays(5),
                    VisitStatusID = 1
                }
            };

            var visitServices = new List<VisitService>()
            {
                new VisitService()
                {
                    VisitID = 1,
                    Visit = visits[0],
                    ServiceID = 1,
                    Service = services[0]
                }
            };

            visits[0].VisitServices = visitServices;

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.Services.AddRange(services);
                arrangeContext.Visits.AddRange(visits);
                arrangeContext.VisitServices.AddRange(visitServices);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                var sut = new VisitServices(actContext, mapper);
                //Act
                var result = await sut.Get(1);
                //Assert
                Assert.AreEqual(visits[0].Vehicle.VIN, result.VIN);
                Assert.AreEqual($"{visits[0].Vehicle.VehicleModel.Manufacturer.Name} {visits[0].Vehicle.VehicleModel.Model}", result.ModelMake);
                Assert.AreEqual(visits[0].Vehicle.VehicleModel.VehicleType.Name, result.VehicleType);
                Assert.AreEqual($"{visits[0].Vehicle.Owner.FirstName} {visits[0].Vehicle.Owner.LastName}", result.Owner);
                Assert.AreEqual(visits[0].Vehicle.RegistrationPlate, result.RegistrationPlate);
                Assert.AreEqual(visits[0].VisitDate, result.VisitDate);
                Assert.AreEqual(visits[0].PickUpDate, result.PickUpDate);
                Assert.AreEqual(visits[0].VisitStatus.Name, result.VisitStatus);

                actContext.Database.EnsureDeleted();
            }
        }


        [TestMethod]
        public async Task ReturnCorrectVisitWhenGettingAsCustomer()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectVisitWhenGettingAsCustomer));

            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var visitStatuses = Utils.GetVisitStatuses();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var services = Utils.GetServices();

            var visits = new List<Visit>()
            {
                new Visit()
                {
                    VisitID =1,
                    VehicleID = 1,
                    VisitDate = DateTime.Now,
                    PickUpDate = DateTime.Now.AddDays(5),
                    VisitStatusID = 1
                }
            };

            var visitServices = new List<VisitService>()
            {
                new VisitService()
                {
                    VisitID = 1,
                    Visit = visits[0],
                    ServiceID = 1,
                    Service = services[0]
                }
            };

            visits[0].VisitServices = visitServices;

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.Services.AddRange(services);
                arrangeContext.Visits.AddRange(visits);
                arrangeContext.VisitServices.AddRange(visitServices);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                var sut = new VisitServices(actContext, mapper);
                //Act
                var result = await sut.Get(1, owners[0].Id);
                //Assert
                Assert.AreEqual(visits[0].Vehicle.VIN, result.VIN);
                Assert.AreEqual($"{visits[0].Vehicle.VehicleModel.Manufacturer.Name} {visits[0].Vehicle.VehicleModel.Model}", result.ModelMake);
                Assert.AreEqual(visits[0].Vehicle.VehicleModel.VehicleType.Name, result.VehicleType);
                Assert.AreEqual($"{visits[0].Vehicle.Owner.FirstName} {visits[0].Vehicle.Owner.LastName}", result.Owner);
                Assert.AreEqual(visits[0].Vehicle.RegistrationPlate, result.RegistrationPlate);
                Assert.AreEqual(visits[0].VisitDate, result.VisitDate);
                Assert.AreEqual(visits[0].PickUpDate, result.PickUpDate);
                Assert.AreEqual(visits[0].VisitStatus.Name, result.VisitStatus);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ReturnCorrectCollectionOfVisits()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectCollectionOfVisits));

            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var visitStatuses = Utils.GetVisitStatuses();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var services = Utils.GetServices();

            var visits = new List<Visit>()
            {
                new Visit()
                {
                    VisitID =1,
                    VehicleID = 1,
                    VisitDate = DateTime.Now,
                    PickUpDate = DateTime.Now.AddDays(5),
                    VisitStatusID = 1
                }
            };

            var visitServices = new List<VisitService>()
            {
                new VisitService()
                {
                    VisitID = 1,
                    Visit = visits[0],
                    ServiceID = 1,
                    Service = services[0]
                }
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.Services.AddRange(services);
                arrangeContext.Visits.AddRange(visits);
                arrangeContext.VisitServices.AddRange(visitServices);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                var sut = new VisitServices(actContext, mapper);
                //Act
                var result = sut.GetAll();

                //Assert
                Assert.AreEqual(visits[0].Vehicle.VIN, result.First().VIN);
                Assert.AreEqual($"{visits[0].Vehicle.VehicleModel.Manufacturer.Name} {visits[0].Vehicle.VehicleModel.Model}", result.First().ModelMake);
                Assert.AreEqual(visits[0].Vehicle.VehicleModel.VehicleType.Name, result.First().VehicleType);
                Assert.AreEqual($"{visits[0].Vehicle.Owner.FirstName} {visits[0].Vehicle.Owner.LastName}", result.First().Owner);
                Assert.AreEqual(visits[0].Vehicle.RegistrationPlate, result.First().RegistrationPlate);
                Assert.AreEqual(visits[0].VisitDate, result.First().VisitDate);
                Assert.AreEqual(visits[0].PickUpDate, result.First().PickUpDate);
                Assert.AreEqual(visits[0].VisitStatus.Name, result.First().VisitStatus);

                actContext.Database.EnsureDeleted();
            }
        }


        [TestMethod]
        public async Task ReturnCorrectVisitsWithGetPaginateAsync()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectVisitsWithGetPaginateAsync));

            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var visitStatuses = Utils.GetVisitStatuses();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var services = Utils.GetServices();

            var visits = new List<Visit>()
            {
                new Visit()
                {
                    VisitID =1,
                    VehicleID = 1,
                    VisitDate = DateTime.Now,
                    PickUpDate = DateTime.Now.AddDays(5),
                    VisitStatusID = 1
                }
            };

            var visitServices = new List<VisitService>()
            {
                new VisitService()
                {
                    VisitID = 1,
                    Visit = visits[0],
                    ServiceID = 1,
                    Service = services[0]
                }
            };

            visits[0].VisitServices = visitServices;

            var filterVisits = new FilterVisits()
            {
                VehicleID = visits[0].VehicleID
            };

            var optionsModel = new PagingOptionsModelVisit()
            {
                Page = 1,
                PageSize = 1,
                SortBy = null,
                SortInDescendingOrder = false
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.Services.AddRange(services);
                arrangeContext.Visits.AddRange(visits);
                arrangeContext.VisitServices.AddRange(visitServices);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var myProfile = new MappingProfile();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                IMapper mapper = new Mapper(configuration);

                var sut = new VisitServices(actContext, mapper);
                //Act
                var result = await sut.GetPaginatedAsync(optionsModel, filterVisits);
                //Assert
                Assert.AreEqual(visits[0].Vehicle.VIN, result.Elements.First().VIN);
                Assert.AreEqual($"{visits[0].Vehicle.VehicleModel.Manufacturer.Name} {visits[0].Vehicle.VehicleModel.Model}", result.Elements.First().ModelMake);
                Assert.AreEqual(visits[0].Vehicle.VehicleModel.VehicleType.Name, result.Elements.First().VehicleType);
                Assert.AreEqual($"{visits[0].Vehicle.Owner.FirstName} {visits[0].Vehicle.Owner.LastName}", result.Elements.First().Owner);
                Assert.AreEqual(visits[0].Vehicle.RegistrationPlate, result.Elements.First().RegistrationPlate);
                Assert.AreEqual(visits[0].VisitDate, result.Elements.First().VisitDate);
                Assert.AreEqual(visits[0].PickUpDate, result.Elements.First().PickUpDate);
                Assert.AreEqual(visits[0].VisitStatus.Name, result.Elements.First().VisitStatus);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ReturnCorrectWhenConvertingCurrency()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenConvertingCurrency));

            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var visitStatuses = Utils.GetVisitStatuses();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var services = Utils.GetServices();

            var visits = new List<Visit>()
            {
                new Visit()
                {
                    VisitID =1,
                    VehicleID = 1,
                    VisitDate = DateTime.Now,
                    PickUpDate = DateTime.Now.AddDays(5),
                    VisitStatusID = 1
                }
            };

            var visitServices = new List<VisitService>()
            {
                new VisitService()
                {
                    VisitID = 1,
                    Visit = visits[0],
                    ServiceID = 1,
                    Service = services[0]
                }
            };

            visits[0].VisitServices = visitServices;

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.Services.AddRange(services);
                arrangeContext.Visits.AddRange(visits);
                arrangeContext.VisitServices.AddRange(visitServices);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                var sut = new VisitServices(actContext, mapper);
                //Act
                var result = await sut.ConvertCurrency(services[0].Currency, "EUR");
                //Assert
                Assert.AreEqual(services[0].Currency, "BGN");

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ReturnCorrectVisitsWithGetPaginatedForCustomerAsync()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectVisitsWithGetPaginatedForCustomerAsync));

            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var visitStatuses = Utils.GetVisitStatuses();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var services = Utils.GetServices();

            var visits = new List<Visit>()
            {
                new Visit()
                {
                    VisitID =1,
                    VehicleID = 1,
                    VisitDate = DateTime.Now,
                    PickUpDate = DateTime.Now.AddDays(5),
                    VisitStatusID = 1
                }
            };

            var visitServices = new List<VisitService>()
            {
                new VisitService()
                {
                    VisitID = 1,
                    Visit = visits[0],
                    ServiceID = 1,
                    Service = services[0]
                }
            };

            visits[0].VisitServices = visitServices;

            var filterVisits = new FilterVisits()
            {
                VehicleID = visits[0].VehicleID,
                Email = owners[0].Email
            };

            var optionsModel = new PagingOptionsModelVisit()
            {
                Page = 1,
                PageSize = 1,
                SortBy = null,
                SortInDescendingOrder = false
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.Services.AddRange(services);
                arrangeContext.Visits.AddRange(visits);
                arrangeContext.VisitServices.AddRange(visitServices);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var myProfile = new MappingProfile();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                IMapper mapper = new Mapper(configuration);

                var sut = new VisitServices(actContext, mapper);
                //Act
                var result = await sut.GetPaginatedForCustomerAsync(optionsModel, filterVisits);
                //Assert
                Assert.AreEqual(visits[0].Vehicle.VIN, result.Elements.First().VIN);
                Assert.AreEqual($"{visits[0].Vehicle.VehicleModel.Manufacturer.Name} {visits[0].Vehicle.VehicleModel.Model}", result.Elements.First().ModelMake);
                Assert.AreEqual(visits[0].Vehicle.VehicleModel.VehicleType.Name, result.Elements.First().VehicleType);
                Assert.AreEqual($"{visits[0].Vehicle.Owner.FirstName} {visits[0].Vehicle.Owner.LastName}", result.Elements.First().Owner);
                Assert.AreEqual(visits[0].Vehicle.RegistrationPlate, result.Elements.First().RegistrationPlate);
                Assert.AreEqual(visits[0].VisitDate, result.Elements.First().VisitDate);
                Assert.AreEqual(visits[0].PickUpDate, result.Elements.First().PickUpDate);
                Assert.AreEqual(visits[0].VisitStatus.Name, result.Elements.First().VisitStatus);

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
