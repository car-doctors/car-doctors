﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace CarDoctors.Tests.ServicesTests.VisitServicesTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void CorrectlyDeleteVisit_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(CorrectlyDeleteVisit_When_ParamsAreValid));


            var visits = Utils.GetVisits();


            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Visits.AddRange(visits);

                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new CarDoctorsContext(options))
            {
                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                var sut = new VisitServices(actContext, mapper);

                sut.Delete(1);
            }
            //Assert
            using (var assertContext = new CarDoctorsContext(options))
            {
                var actual = assertContext.Visits.FirstOrDefault(x => x.VisitID == 1);
                Assert.IsNull(actual);

                assertContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowsWhenInvalidVisit()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowsWhenInvalidVisit));

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                var sut = new VisitServices(actContext, mapper);

                Assert.ThrowsException<ObjectNotFoundException>(() => sut.Delete(1));

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
