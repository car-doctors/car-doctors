﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CarDoctors.Tests.ServicesTests.VisitServicesTests
{
    [TestClass]
    public class GetCustomerServices_Should
    {
        [TestMethod]
        public void ReturnCorrectWhenGettingCustomerServicesByFilteringByVehicleId()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenGettingCustomerServicesByFilteringByVehicleId));

            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var visitStatuses = Utils.GetVisitStatuses();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var services = Utils.GetServices();
            var visits = Utils.GetVisits();
            var visitServices = Utils.GetVisitServices();

            var filter = new FilterVisits
            {
                VehicleID = 1
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.Services.AddRange(services);
                arrangeContext.Visits.AddRange(visits);
                arrangeContext.VisitServices.AddRange(visitServices);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                var sut = new VisitServices(actContext, mapper);
                //Act
                var result = sut.GetCustomerServices(1, filter);
                //Assert
                // Assert.AreEqual(visits[0].Vehicle.VIN, result.First().VIN);
                // Assert.AreEqual($"{visits[0].Vehicle.VehicleModel.Manufacturer.Name} {visits[0].Vehicle.VehicleModel.Model}", result.First().ModelMake);
                // Assert.AreEqual(visits[0].Vehicle.VehicleModel.VehicleType.Name, result.First().VehicleType);
                // Assert.AreEqual($"{visits[0].Vehicle.Owner.FirstName} {visits[0].Vehicle.Owner.LastName}", result.First().Owner);
                // Assert.AreEqual(visits[0].Vehicle.RegistrationPlate, result.First().RegistrationPlate);
                // Assert.AreEqual(visits[0].VisitDate, result.First().VisitDate);
                // Assert.AreEqual(visits[0].PickUpDate, result.First().PickUpDate);
                // Assert.AreEqual(visits[0].VisitStatus.Name, result.First().VisitStatus);

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
