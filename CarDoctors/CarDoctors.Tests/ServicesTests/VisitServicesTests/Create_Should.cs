﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace CarDoctors.Tests.ServicesTests.VisitServicesTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public async Task ReturnCorrectEntityWhenCreatingVisit()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectEntityWhenCreatingVisit));

            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var visitStatuses = Utils.GetVisitStatuses();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var services = Utils.GetServices();
            var visitServices = Utils.GetVisitServices();

            var visit = new VisitInputModel
            {
                VehicleID = 1,
                VisitDate = DateTime.Now,
                PickupDate = DateTime.Now.AddDays(10),
                VisitStatusID = 2,
                ServiceID = "1"
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.Services.AddRange(services);
                // arrangeContext.VisitServices.AddRange(visitServices);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockMapper = new Mock<IMapper>();


                IMapper mapper = mockMapper.Object;

                var sut = new VisitServices(actContext, mapper);

                //Act
                var result = await sut.Create(visit);

                //Assert
                Assert.AreEqual(visit.VehicleID, result.VehicleID);
                Assert.AreEqual(visit.VisitDate, result.VisitDate);
                Assert.AreEqual(visit.PickupDate, result.PickUpDate);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ThrowWhenCreatingDuplicateVisit()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowWhenCreatingDuplicateVisit));

            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var visitStatuses = Utils.GetVisitStatuses();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var services = Utils.GetServices();
            var visitServices = Utils.GetVisitServices();
            var visits = Utils.GetVisits();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.Services.AddRange(services);
                arrangeContext.Visits.AddRange(visits);
                //arrangeContext.VisitServices.AddRange(visitServices);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockMapper = new Mock<IMapper>();


                IMapper mapper = mockMapper.Object;

                var sut = new VisitServices(actContext, mapper);

                //Act && Assert

                var visitInputModel = new VisitInputModel
                {
                    VehicleID = visits[0].VehicleID,
                    VisitDate = visits[0].VisitDate,
                    PickupDate = visits[0].PickUpDate,
                    VisitStatusID = visits[0].VisitStatusID,
                    ServiceID = "1"
                };

                await Assert.ThrowsExceptionAsync<ObjectDuplicateException>(() => sut.Create(visitInputModel));

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ReturnCorrectWhenCreatingWhenVisitIsDeleted()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenCreatingWhenVisitIsDeleted));

            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var visitStatuses = Utils.GetVisitStatuses();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var services = Utils.GetServices();
            var visitServices = Utils.GetVisitServices();
            var visits = Utils.GetVisits();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.Services.AddRange(services);
                arrangeContext.Visits.AddRange(visits);
                //arrangeContext.VisitServices.AddRange(visitServices);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockMapper = new Mock<IMapper>();


                IMapper mapper = mockMapper.Object;

                var sut = new VisitServices(actContext, mapper);

                var visitInputModel = new VisitInputModel
                {
                    VehicleID = visits[0].VehicleID,
                    VisitDate = visits[0].VisitDate,
                    PickupDate = visits[0].PickUpDate,
                    VisitStatusID = visits[0].VisitStatusID,
                    ServiceID = "1"
                };

                sut.Delete(1);
                //Act 

                var result = await sut.Create(visitInputModel);

                //Assert
                Assert.AreEqual(visits[0].VehicleID, result.VehicleID);
                Assert.AreEqual(visits[0].VisitDate, result.VisitDate);
                Assert.AreEqual(visits[0].PickUpDate, result.PickUpDate);

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
