﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Mappers;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;

namespace CarDoctors.Tests.ServicesTests.VisitServicesTests
{
    [TestClass]
    public class Filter_Should
    {
        [TestMethod]
        public void ReturnCorrectWhenGettingCustomerServicesByFilteringByVehicleId()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenGettingCustomerServicesByFilteringByVehicleId));

            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var visitStatuses = Utils.GetVisitStatuses();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var services = Utils.GetServices();
            var visits = Utils.GetVisits();
            var visitServices = Utils.GetVisitServices();

            var filter = new FilterVisits
            {
                VehicleID = 1
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.Services.AddRange(services);
                arrangeContext.Visits.AddRange(visits);
                arrangeContext.VisitServices.AddRange(visitServices);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                var sut = new VisitServices(actContext, mapper);
                //Act
                var visitsDTO = sut.GetAll().ToList();

                var result = sut.Filter(visitsDTO, filter);
                //Assert
                Assert.AreEqual(visitsDTO[0].VIN, result.First().VIN);
                Assert.AreEqual(visitsDTO[0].ModelMake, result.First().ModelMake);
                Assert.AreEqual(visitsDTO[0].VehicleType, result.First().VehicleType);
                Assert.AreEqual(visitsDTO[0].Owner, result.First().Owner);
                Assert.AreEqual(visitsDTO[0].RegistrationPlate, result.First().RegistrationPlate);
                Assert.AreEqual(visitsDTO[0].VisitDate, result.First().VisitDate);
                Assert.AreEqual(visitsDTO[0].PickUpDate, result.First().PickUpDate);
                Assert.AreEqual(visitsDTO[0].VisitStatus, result.First().VisitStatus);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ReturnCorrectWhenGettingCustomerServicesByFilteringByDateFrom()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenGettingCustomerServicesByFilteringByDateFrom));

            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var visitStatuses = Utils.GetVisitStatuses();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var services = Utils.GetServices();
            var visits = Utils.GetVisits();
            var visitServices = Utils.GetVisitServices();

            var filter = new FilterVisits
            {
                DateFrom = DateTime.Now.AddDays(-10)
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.Services.AddRange(services);
                arrangeContext.Visits.AddRange(visits);
                arrangeContext.VisitServices.AddRange(visitServices);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                var sut = new VisitServices(actContext, mapper);
                //Act
                var visitsDTO = sut.GetAll().ToList();

                var result = sut.Filter(visitsDTO, filter);
                //Assert             
                Assert.AreEqual(visitsDTO[0].VIN, result.First().VIN);
                Assert.AreEqual(visitsDTO[0].ModelMake, result.First().ModelMake);
                Assert.AreEqual(visitsDTO[0].VehicleType, result.First().VehicleType);
                Assert.AreEqual(visitsDTO[0].Owner, result.First().Owner);
                Assert.AreEqual(visitsDTO[0].RegistrationPlate, result.First().RegistrationPlate);
                Assert.AreEqual(visitsDTO[0].VisitDate, result.First().VisitDate);
                Assert.AreEqual(visitsDTO[0].PickUpDate, result.First().PickUpDate);
                Assert.AreEqual(visitsDTO[0].VisitStatus, result.First().VisitStatus);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ReturnCorrectWhenGettingCustomerServicesByFilteringByDateTo()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenGettingCustomerServicesByFilteringByDateTo));

            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var visitStatuses = Utils.GetVisitStatuses();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var services = Utils.GetServices();
            var visits = Utils.GetVisits();
            var visitServices = Utils.GetVisitServices();

            var filter = new FilterVisits
            {
                DateTo = DateTime.Now
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.Services.AddRange(services);
                arrangeContext.Visits.AddRange(visits);
                arrangeContext.VisitServices.AddRange(visitServices);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                var sut = new VisitServices(actContext, mapper);
                //Act
                var visitsDTO = sut.GetAll().ToList();

                var result = sut.Filter(visitsDTO, filter);
                //Assert             
                Assert.AreEqual(visitsDTO[0].VIN, result.First().VIN);
                Assert.AreEqual(visitsDTO[0].ModelMake, result.First().ModelMake);
                Assert.AreEqual(visitsDTO[0].VehicleType, result.First().VehicleType);
                Assert.AreEqual(visitsDTO[0].Owner, result.First().Owner);
                Assert.AreEqual(visitsDTO[0].RegistrationPlate, result.First().RegistrationPlate);
                Assert.AreEqual(visitsDTO[0].VisitDate, result.First().VisitDate);
                Assert.AreEqual(visitsDTO[0].PickUpDate, result.First().PickUpDate);
                Assert.AreEqual(visitsDTO[0].VisitStatus, result.First().VisitStatus);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ReturnCorrectWhenFilteringCustomersByVehicleId()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenFilteringCustomersByVehicleId));

            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var visitStatuses = Utils.GetVisitStatuses();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var services = Utils.GetServices();
            var visits = Utils.GetVisits();
            var visitServices = Utils.GetVisitServices();

            var filter = new FilterVisits
            {
                VehicleID = 1,
                Email = owners[0].Email
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.Services.AddRange(services);
                arrangeContext.Visits.AddRange(visits);
                arrangeContext.VisitServices.AddRange(visitServices);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                var sut = new VisitServices(actContext, mapper);
                //Act

                var visitsDTO = actContext.Visits.MapVisitDTO();

                var result = sut.FilterForCustomer(visitsDTO, filter);

                var visitDTOs = visitsDTO.ToList();

                //Assert
                Assert.AreEqual(visitDTOs[0].VIN, result.First().VIN);
                Assert.AreEqual(visitDTOs[0].ModelMake, result.First().ModelMake);
                Assert.AreEqual(visitDTOs[0].VehicleType, result.First().VehicleType);
                Assert.AreEqual(visitDTOs[0].Owner, result.First().Owner);
                Assert.AreEqual(visitDTOs[0].RegistrationPlate, result.First().RegistrationPlate);
                Assert.AreEqual(visitDTOs[0].VisitDate, result.First().VisitDate);
                Assert.AreEqual(visitDTOs[0].PickUpDate, result.First().PickUpDate);
                Assert.AreEqual(visitDTOs[0].VisitStatus, result.First().VisitStatus);

                actContext.Database.EnsureDeleted();
            }
        }


        [TestMethod]
        public void ReturnCorrectWhenFilteringCustomersByDateFrom()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenFilteringCustomersByDateFrom));

            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var visitStatuses = Utils.GetVisitStatuses();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var services = Utils.GetServices();
            var visits = Utils.GetVisits();
            var visitServices = Utils.GetVisitServices();

            var filter = new FilterVisits
            {
                DateFrom = DateTime.Now.AddDays(-10),
                Email = owners[0].Email
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.Services.AddRange(services);
                arrangeContext.Visits.AddRange(visits);
                arrangeContext.VisitServices.AddRange(visitServices);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                var sut = new VisitServices(actContext, mapper);
                //Act

                var visitsDTO = actContext.Visits.MapVisitDTO();

                var result = sut.FilterForCustomer(visitsDTO, filter);

                var visitDTOs = visitsDTO.ToList();

                //Assert
                Assert.AreEqual(visitDTOs[0].VIN, result.First().VIN);
                Assert.AreEqual(visitDTOs[0].ModelMake, result.First().ModelMake);
                Assert.AreEqual(visitDTOs[0].VehicleType, result.First().VehicleType);
                Assert.AreEqual(visitDTOs[0].Owner, result.First().Owner);
                Assert.AreEqual(visitDTOs[0].RegistrationPlate, result.First().RegistrationPlate);
                Assert.AreEqual(visitDTOs[0].VisitDate, result.First().VisitDate);
                Assert.AreEqual(visitDTOs[0].PickUpDate, result.First().PickUpDate);
                Assert.AreEqual(visitDTOs[0].VisitStatus, result.First().VisitStatus);

                actContext.Database.EnsureDeleted();
            }
        }


        [TestMethod]
        public void ReturnCorrectWhenFilteringCustomersByDateTo()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenFilteringCustomersByDateTo));

            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var visitStatuses = Utils.GetVisitStatuses();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var services = Utils.GetServices();
            var visits = Utils.GetVisits();
            var visitServices = Utils.GetVisitServices();

            var filter = new FilterVisits
            {
                DateTo = DateTime.Now,
                Email = owners[0].Email
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.Services.AddRange(services);
                arrangeContext.Visits.AddRange(visits);
                arrangeContext.VisitServices.AddRange(visitServices);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                var sut = new VisitServices(actContext, mapper);
                //Act

                var visitsDTO = actContext.Visits.MapVisitDTO();

                var result = sut.FilterForCustomer(visitsDTO, filter);

                var visitDTOs = visitsDTO.ToList();

                //Assert
                Assert.AreEqual(visitDTOs[0].VIN, result.First().VIN);
                Assert.AreEqual(visitDTOs[0].ModelMake, result.First().ModelMake);
                Assert.AreEqual(visitDTOs[0].VehicleType, result.First().VehicleType);
                Assert.AreEqual(visitDTOs[0].Owner, result.First().Owner);
                Assert.AreEqual(visitDTOs[0].RegistrationPlate, result.First().RegistrationPlate);
                Assert.AreEqual(visitDTOs[0].VisitDate, result.First().VisitDate);
                Assert.AreEqual(visitDTOs[0].PickUpDate, result.First().PickUpDate);
                Assert.AreEqual(visitDTOs[0].VisitStatus, result.First().VisitStatus);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ReturnCorrectWhenGettingCustomerServicesByFilteringByDateFrom1()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenGettingCustomerServicesByFilteringByDateFrom1));

            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var visitStatuses = Utils.GetVisitStatuses();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var services = Utils.GetServices();
            var visits = Utils.GetVisits();
            var visitServices = Utils.GetVisitServices();

            var filter = new FilterVisits
            {
                Email = owners[0].Email,
                DateFrom = DateTime.Now.AddDays(-10),
                VehicleID = vehicles[0].VehicleID,
                DateTo = DateTime.Now.AddDays(10)
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.Services.AddRange(services);
                arrangeContext.Visits.AddRange(visits);
                arrangeContext.VisitServices.AddRange(visitServices);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                var sut = new VisitServices(actContext, mapper);
                //Act
                var visitsDTO = actContext.Visits;

                var result = sut.Filter(visitsDTO, filter);

                var visitsDTOList = actContext.Visits.MapVisitDTO().ToList();

                //Assert             
                Assert.AreEqual(visitsDTOList[0].VisitDate, result.First().VisitDate);
                Assert.AreEqual(visitsDTOList[0].PickUpDate, result.First().PickUpDate);

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
