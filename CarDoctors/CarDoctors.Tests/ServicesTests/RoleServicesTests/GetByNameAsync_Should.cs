﻿using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace CarDoctors.Tests.ServicesTests.RoleServicesTests
{
    [TestClass]
    public class GetByNameAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectRoleByName()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectRoleByName));

            var roles = Utils.GetRoles();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Roles.AddRange(roles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var sut = new RoleServices(actContext);
                //Act
                var result = await sut.GetByNameAsync(roles[0].Name);
                //Assert
                Assert.AreEqual(roles[0].Name, result.Name);

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
