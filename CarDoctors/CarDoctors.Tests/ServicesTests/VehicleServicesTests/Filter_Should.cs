﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Services;
using CarDoctors.Services.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace CarDoctors.Tests.ServicesTests.VehicleServicesTests
{
    [TestClass]
    public class Filter_Should
    {

        [TestMethod]
        public void ReturnCorrectWhenFilterVehiclesByOnwerId()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenFilterVehiclesByOnwerId));

            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicleModels = Utils.GetVehicleModels();
            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var vehicles = Utils.GetVehicles();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Vehicles.AddRange(vehicles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mock = new Mock<IVehicleModelServices>();

                var mockMapper = new Mock<IMapper>();

                IVehicleModelServices services = mock.Object;

                IMapper mapper = mockMapper.Object;

                var sut = new VehicleServices(actContext, services, mapper);
                //Act

                var result = sut.Filter(actContext.Vehicles, 1);
                //Assert
                Assert.AreEqual(vehicles[0].VehicleID, result.First().VehicleID);
                Assert.AreEqual(vehicles[0].VIN, result.First().VIN);
                Assert.AreEqual($"{vehicles[0].RegistrationPlate}", result.First().RegistrationPlate);

                actContext.Database.EnsureDeleted();
            }
        }
        [TestMethod]
        public void ReturnCorrectWhenFilterVehicles()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenFilterVehicles));

            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicleModels = Utils.GetVehicleModels();
            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var vehicles = Utils.GetVehicles();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Vehicles.AddRange(vehicles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mock = new Mock<IVehicleModelServices>();

                var mockMapper = new Mock<IMapper>();

                IVehicleModelServices services = mock.Object;

                IMapper mapper = mockMapper.Object;

                var sut = new VehicleServices(actContext, services, mapper);
                //Act

                var result = sut.Filter(actContext.Vehicles, 0);
                //Assert
                Assert.AreEqual(vehicles[0].VehicleID, result.First().VehicleID);
                Assert.AreEqual(vehicles[0].VIN, result.First().VIN);
                Assert.AreEqual($"{vehicles[0].RegistrationPlate}", result.First().RegistrationPlate);

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
