﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Services;
using CarDoctors.Services.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace CarDoctors.Tests.ServicesTests.VehicleServicesTests
{
    [TestClass]
    public class Update_Should
    {
        [TestMethod]
        public async Task ReturnCorrectVehicleWhenUpdating()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectVehicleWhenUpdating));

            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicleModels = Utils.GetVehicleModels();
            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var vehicles = Utils.GetVehicles();

            var vehicleInputModel = new VehicleInputModel
            {
                VIN = "5XYKTDA14BG127777",
                Manufacturer = "BMW",
                Model = "330",
                OwnerFirstName = owners[0].FirstName,
                OwnerLastName = owners[0].LastName,
                RegistrationPlate = "BP8888BP",
                Type = "Car"
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Vehicles.AddRange(vehicles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mock = new Mock<IVehicleModelServices>();

                var mockMapper = new Mock<IMapper>();

                IVehicleModelServices services = mock.Object;

                IMapper mapper = mockMapper.Object;

                var sut = new VehicleServices(actContext, services, mapper);

                //Act
                var result = await sut.Update(vehicleInputModel, 1);

                //Assert
                Assert.AreEqual(vehicleInputModel.VIN, result.VIN);
                Assert.AreEqual(vehicleInputModel.Model, result.Model);
                Assert.AreEqual(vehicleInputModel.Manufacturer, result.Manufacturer);
                Assert.AreEqual(vehicleInputModel.RegistrationPlate, result.RegistrationPlate);
                Assert.AreEqual(vehicleInputModel.Type, result.VehicleType);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ThrowWhenVehicleNotFound()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowWhenVehicleNotFound));

            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicleModels = Utils.GetVehicleModels();
            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var vehicles = Utils.GetVehicles();

            var vehicleInputModel = new VehicleInputModel
            {
                VIN = "5XYKTDA14BG127777",
                Manufacturer = "BMW",
                Model = "330",
                OwnerFirstName = owners[0].FirstName,
                OwnerLastName = owners[0].LastName,
                RegistrationPlate = "BP8888BP",
                Type = "Car"
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Vehicles.AddRange(vehicles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mock = new Mock<IVehicleModelServices>();

                var mockMapper = new Mock<IMapper>();

                IVehicleModelServices services = mock.Object;

                IMapper mapper = mockMapper.Object;

                var sut = new VehicleServices(actContext, services, mapper);
                //Act && Assert

                await Assert.ThrowsExceptionAsync<ObjectNullException>(() => sut.Update(vehicleInputModel, 0));

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ThrowWhenVehicleOwnerIsNullWhenUpdating()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowWhenVehicleOwnerIsNullWhenUpdating));

            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicleModels = Utils.GetVehicleModels();
            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var vehicles = Utils.GetVehicles();

            var vehicleInputModel = new VehicleInputModel
            {
                VIN = "5XYKTDA14BG124123",
                Manufacturer = "BMW",
                Model = "330",
                OwnerFirstName = null,
                OwnerLastName = null,
                RegistrationPlate = "BP8888BP",
                Type = "Car"
            };
            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Vehicles.AddRange(vehicles);


                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mock = new Mock<IVehicleModelServices>();

                var mockMapper = new Mock<IMapper>();

                IVehicleModelServices services = mock.Object;

                IMapper mapper = mockMapper.Object;

                var sut = new VehicleServices(actContext, services, mapper);

                //Act && Assert

                await Assert.ThrowsExceptionAsync<ObjectNullException>(() => sut.Update(vehicleInputModel, 1));

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
