﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Services;
using CarDoctors.Services.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace CarDoctors.Tests.ServicesTests.VehicleServicesTests
{
    [TestClass]
    public class Delete_Should
    {

        [TestMethod]
        public void CorrectlyDeleteVehicle_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(CorrectlyDeleteVehicle_When_ParamsAreValid));


            var vehicles = Utils.GetVehicles();


            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Vehicles.AddRange(vehicles);

                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new CarDoctorsContext(options))
            {
                var mock = new Mock<IVehicleModelServices>();

                var mockMapper = new Mock<IMapper>();

                IVehicleModelServices services = mock.Object;

                IMapper mapper = mockMapper.Object;

                var sut = new VehicleServices(actContext, services, mapper);

                sut.Delete(1);
            }
            //Assert
            using (var assertContext = new CarDoctorsContext(options))
            {
                var actual = assertContext.Vehicles.FirstOrDefault(x => x.VehicleID == 1);
                Assert.IsNull(actual);

                assertContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowsWhenInvalidVehicle()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowsWhenInvalidVehicle));

            using (var actContext = new CarDoctorsContext(options))
            {
                var mock = new Mock<IVehicleModelServices>();

                var mockMapper = new Mock<IMapper>();

                IVehicleModelServices services = mock.Object;

                IMapper mapper = mockMapper.Object;

                var sut = new VehicleServices(actContext, services, mapper);

                Assert.ThrowsException<ObjectNotFoundException>(() => sut.Delete(1));

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
