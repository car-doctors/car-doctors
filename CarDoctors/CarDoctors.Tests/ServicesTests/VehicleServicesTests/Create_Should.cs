﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Services;
using CarDoctors.Services.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace CarDoctors.Tests.ServicesTests.VehicleServicesTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public async Task ReturnCorrectEntity()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectEntity));

            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicleModels = Utils.GetVehicleModels();
            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();

            var vehicle = new VehicleInputModel
            {
                VIN = "5XYKTDA14BG124123",
                Manufacturer = "BMW",
                Model = "330",
                OwnerFirstName = owners[0].FirstName,
                OwnerLastName = owners[0].LastName,
                RegistrationPlate = "BP8888BP",
                Type = "Car"
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.VehicleModels.AddRange(vehicleModels);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {

                var mock = new Mock<IVehicleModelServices>();

                var mockMapper = new Mock<IMapper>();

                IVehicleModelServices services = mock.Object;

                IMapper mapper = mockMapper.Object;

                var sut = new VehicleServices(actContext, services, mapper);

                //Act
                var result = await sut.Create(vehicle);

                //Assert
                Assert.AreEqual(vehicle.VIN, result.VIN);
                Assert.AreEqual(vehicle.Model, result.Model);
                Assert.AreEqual(vehicle.Manufacturer, result.Manufacturer);
                Assert.AreEqual(vehicle.RegistrationPlate, result.RegistrationPlate);
                Assert.AreEqual(vehicle.Type, result.VehicleType);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ThrowWhenCreatingDuplicateVehicle()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowWhenCreatingDuplicateVehicle));

            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicleModels = Utils.GetVehicleModels();
            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var vehicles = Utils.GetVehicles();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Vehicles.AddRange(vehicles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mock = new Mock<IVehicleModelServices>();

                var mockMapper = new Mock<IMapper>();

                IVehicleModelServices services = mock.Object;

                IMapper mapper = mockMapper.Object;

                var sut = new VehicleServices(actContext, services, mapper);

                //Act && Assert
                var vehicleInputModel = new VehicleInputModel
                {
                    VIN = vehicles[0].VIN,
                    Manufacturer = vehicles[0].VehicleModel.Manufacturer.Name,
                    Model = vehicles[0].VehicleModel.Model,
                    OwnerFirstName = vehicles[0].Owner.FirstName,
                    OwnerLastName = vehicles[0].Owner.LastName,
                    RegistrationPlate = vehicles[0].RegistrationPlate,
                    Type = vehicles[0].VehicleModel.VehicleType.Name
                };

                await Assert.ThrowsExceptionAsync<ObjectDuplicateException>(() => sut.Create(vehicleInputModel));

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ThrowWhenVehicleOwnerIsNullWhenCreating()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowWhenVehicleOwnerIsNullWhenCreating));

            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicleModels = Utils.GetVehicleModels();
            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();

            var vehicleInputModel = new VehicleInputModel
            {
                VIN = "5XYKTDA14BG124123",
                Manufacturer = "BMW",
                Model = "330",
                OwnerFirstName = null,
                OwnerLastName = null,
                RegistrationPlate = "BP8888BP",
                Type = "Car"
            };
            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.VehicleModels.AddRange(vehicleModels);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mock = new Mock<IVehicleModelServices>();

                var mockMapper = new Mock<IMapper>();

                IVehicleModelServices services = mock.Object;

                IMapper mapper = mockMapper.Object;

                var sut = new VehicleServices(actContext, services, mapper);

                //Act && Assert

                await Assert.ThrowsExceptionAsync<ObjectNullException>(() => sut.Create(vehicleInputModel));

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ReturnCorrectWhenCreatingWhenVehicleIsDeleted()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenCreatingWhenVehicleIsDeleted));

            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicleModels = Utils.GetVehicleModels();
            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var vehicles = Utils.GetVehicles();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Vehicles.AddRange(vehicles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mock = new Mock<IVehicleModelServices>();

                var mockMapper = new Mock<IMapper>();

                IVehicleModelServices services = mock.Object;

                IMapper mapper = mockMapper.Object;

                var sut = new VehicleServices(actContext, services, mapper);

                var vehicleInputModel = new VehicleInputModel
                {
                    VIN = vehicles[0].VIN,
                    Manufacturer = vehicles[0].VehicleModel.Manufacturer.Name,
                    Model = vehicles[0].VehicleModel.Model,
                    OwnerFirstName = vehicles[0].Owner.FirstName,
                    OwnerLastName = vehicles[0].Owner.LastName,
                    RegistrationPlate = vehicles[0].RegistrationPlate,
                    Type = vehicles[0].VehicleModel.VehicleType.Name
                };

                sut.Delete(1);
                //Act 

                var result = await sut.Create(vehicleInputModel);
                //Assert
                Assert.AreEqual(vehicles[0].VIN, result.VIN);
                Assert.AreEqual(vehicles[0].VehicleModel.Model, result.Model);
                Assert.AreEqual(vehicles[0].VehicleModel.Manufacturer.Name, result.Manufacturer);
                Assert.AreEqual(vehicles[0].RegistrationPlate, result.RegistrationPlate);
                Assert.AreEqual(vehicles[0].VehicleModel.VehicleType.Name, result.VehicleType);

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
