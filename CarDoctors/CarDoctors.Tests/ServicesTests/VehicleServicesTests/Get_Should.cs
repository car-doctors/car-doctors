﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.MVC.Infrastructure;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.Services.Services;
using CarDoctors.Services.Services.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace CarDoctors.Tests.ServicesTests.VehicleServicesTests
{
    [TestClass]
    public class Get_Should
    {

        [TestMethod]
        public async Task ReturnCorrectVehicle()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectVehicle));

            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicleModels = Utils.GetVehicleModels();
            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var vehicles = Utils.GetVehicles();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Vehicles.AddRange(vehicles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mock = new Mock<IVehicleModelServices>();

                var mockMapper = new Mock<IMapper>();

                IVehicleModelServices services = mock.Object;

                IMapper mapper = mockMapper.Object;

                var sut = new VehicleServices(actContext, services, mapper);
                //Act
                var result = await sut.Get(1);
                //Assert
                Assert.AreEqual(vehicles[0].VehicleID, result.ID);
                Assert.AreEqual(vehicles[0].VIN, result.VIN);
                Assert.AreEqual(vehicles[0].VehicleModel.Model, result.Model);
                Assert.AreEqual(vehicles[0].VehicleModel.Manufacturer.Name, result.Manufacturer);
                Assert.AreEqual(vehicles[0].VehicleModel.VehicleType.Name, result.VehicleType);
                Assert.AreEqual($"{vehicles[0].Owner.FirstName} {vehicles[0].Owner.LastName}", result.Owner);
                Assert.AreEqual($"{vehicles[0].RegistrationPlate}", result.RegistrationPlate);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ReturnCorrectVehicleWithCustomerRequest()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectVehicleWithCustomerRequest));

            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicleModels = Utils.GetVehicleModels();
            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var vehicles = Utils.GetVehicles();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Vehicles.AddRange(vehicles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mock = new Mock<IVehicleModelServices>();

                var mockMapper = new Mock<IMapper>();

                IVehicleModelServices services = mock.Object;

                IMapper mapper = mockMapper.Object;

                var sut = new VehicleServices(actContext, services, mapper);
                //Act
                var result = await sut.Get(1, owners[0].Id);
                //Assert
                Assert.AreEqual(vehicles[0].VehicleID, result.ID);
                Assert.AreEqual(vehicles[0].VIN, result.VIN);
                Assert.AreEqual(vehicles[0].VehicleModel.Model, result.Model);
                Assert.AreEqual(vehicles[0].VehicleModel.Manufacturer.Name, result.Manufacturer);
                Assert.AreEqual(vehicles[0].VehicleModel.VehicleType.Name, result.VehicleType);
                Assert.AreEqual($"{vehicles[0].Owner.FirstName} {vehicles[0].Owner.LastName}", result.Owner);
                Assert.AreEqual($"{vehicles[0].RegistrationPlate}", result.RegistrationPlate);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ThrowWhenCustomerIsGettingButVehicleNotFound()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowWhenCustomerIsGettingButVehicleNotFound));

            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicleModels = Utils.GetVehicleModels();
            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var vehicles = Utils.GetVehicles();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Vehicles.AddRange(vehicles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mock = new Mock<IVehicleModelServices>();

                var mockMapper = new Mock<IMapper>();

                IVehicleModelServices services = mock.Object;

                IMapper mapper = mockMapper.Object;

                var sut = new VehicleServices(actContext, services, mapper);
                //Act
                //Assert
                await Assert.ThrowsExceptionAsync<ObjectNotFoundException>(() => sut.Get(0, owners[0].Id));

                actContext.Database.EnsureDeleted();
            }
        }


        [TestMethod]
        public void ReturnCorrectCollectionOfVehiclesWithOwnerID()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectCollectionOfVehiclesWithOwnerID));

            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicleModels = Utils.GetVehicleModels();
            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var vehicles = Utils.GetVehicles();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Vehicles.AddRange(vehicles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mock = new Mock<IVehicleModelServices>();

                var mockMapper = new Mock<IMapper>();

                IVehicleModelServices services = mock.Object;

                IMapper mapper = mockMapper.Object;

                var sut = new VehicleServices(actContext, services, mapper);
                //Act
                var result = sut.GetAll(1);

                //Assert
                Assert.AreEqual(vehicles[0].VehicleID, result.First().ID);
                Assert.AreEqual(vehicles[0].VIN, result.First().VIN);
                Assert.AreEqual(vehicles[0].VehicleModel.Model, result.First().Model);
                Assert.AreEqual(vehicles[0].VehicleModel.Manufacturer.Name, result.First().Manufacturer);
                Assert.AreEqual(vehicles[0].VehicleModel.VehicleType.Name, result.First().VehicleType);
                Assert.AreEqual($"{vehicles[0].Owner.FirstName} {vehicles[0].Owner.LastName}", result.First().Owner);
                Assert.AreEqual($"{vehicles[0].RegistrationPlate}", result.First().RegistrationPlate);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ReturnCorrectCollectionOfVehicles()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectCollectionOfVehicles));

            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicleModels = Utils.GetVehicleModels();
            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var vehicles = Utils.GetVehicles();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Vehicles.AddRange(vehicles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mock = new Mock<IVehicleModelServices>();

                var mockMapper = new Mock<IMapper>();

                IVehicleModelServices services = mock.Object;

                IMapper mapper = mockMapper.Object;

                var sut = new VehicleServices(actContext, services, mapper);
                //Act
                var result = sut.GetAll(null);

                //Assert
                Assert.AreEqual(vehicles[0].VehicleID, result.First().ID);
                Assert.AreEqual(vehicles[0].VIN, result.First().VIN);
                Assert.AreEqual(vehicles[0].VehicleModel.Model, result.First().Model);
                Assert.AreEqual(vehicles[0].VehicleModel.Manufacturer.Name, result.First().Manufacturer);
                Assert.AreEqual(vehicles[0].VehicleModel.VehicleType.Name, result.First().VehicleType);
                Assert.AreEqual($"{vehicles[0].Owner.FirstName} {vehicles[0].Owner.LastName}", result.First().Owner);
                Assert.AreEqual($"{vehicles[0].RegistrationPlate}", result.First().RegistrationPlate);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ReturnCorrectCollectionOfVehiclesWithGetAllAsync()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectCollectionOfVehicles));

            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicleModels = Utils.GetVehicleModels();
            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var vehicles = Utils.GetVehicles();

            var paginatedModel = new PagingOptionsModelVehicle
            {
                Page = 1,
                PageSize = 2
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Vehicles.AddRange(vehicles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mock = new Mock<IVehicleModelServices>();

                var mockMapper = new Mock<IMapper>();

                IVehicleModelServices services = mock.Object;

                IMapper mapper = mockMapper.Object;

                var sut = new VehicleServices(actContext, services, mapper);
                //Act
                var result = await sut.GetAllAsync(paginatedModel);

                //Assert
                Assert.AreEqual(vehicles[0].VehicleID, result.First().ID);
                Assert.AreEqual(vehicles[0].VIN, result.First().VIN);
                Assert.AreEqual(vehicles[0].VehicleModel.Model, result.First().Model);
                Assert.AreEqual(vehicles[0].VehicleModel.Manufacturer.Name, result.First().Manufacturer);
                Assert.AreEqual(vehicles[0].VehicleModel.VehicleType.Name, result.First().VehicleType);
                Assert.AreEqual($"{vehicles[0].Owner.FirstName} {vehicles[0].Owner.LastName}", result.First().Owner);
                Assert.AreEqual($"{vehicles[0].RegistrationPlate}", result.First().RegistrationPlate);

                actContext.Database.EnsureDeleted();
            }
        }


        [TestMethod]
        public async Task ReturnCorrectServicesWithGetPaginateAsync()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectCollectionOfVehicles));

            //Arrange 
            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicleModels = Utils.GetVehicleModels();
            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var vehicles = Utils.GetVehicles();

            var filterVehicles = new PagingOptionsModelVehicle
            {
                Page = 1,
                PageSize = 1,
                SortBy = null,
                SortInDescendingOrder = false
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Vehicles.AddRange(vehicles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var myProfile = new MappingProfile();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                IMapper mapper = new Mapper(configuration);

                var mock = new Mock<IVehicleModelServices>();

                var mockMapper = new Mock<IMapper>();

                IVehicleModelServices services = mock.Object;


                var sut = new VehicleServices(actContext, services, mapper);


                //Act
                var result = await sut.GetPaginatedAsync(filterVehicles, vehicles[0].VehicleID);
                //Assert
                Assert.AreEqual(vehicles[1].VehicleID, result.Elements.First().ID);
                Assert.AreEqual(vehicles[1].VIN, result.Elements.First().VIN);
                Assert.AreEqual(vehicles[1].VehicleModel.Model, result.Elements.First().Model);
                Assert.AreEqual(vehicles[1].VehicleModel.Manufacturer.Name, result.Elements.First().Manufacturer);
                Assert.AreEqual(vehicles[1].VehicleModel.VehicleType.Name, result.Elements.First().VehicleType);
                Assert.AreEqual($"{vehicles[1].Owner.FirstName} {vehicles[0].Owner.LastName}", result.Elements.First().Owner);
                Assert.AreEqual($"{vehicles[1].RegistrationPlate}", result.Elements.First().RegistrationPlate);

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
