﻿using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;

namespace CarDoctors.Tests.ServicesTests.VehicleModelServicesTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public void ReturnCorrectCollectionOfVehicleModels()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectCollectionOfVehicleModels));

            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicleModels = Utils.GetVehicleModels();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.VehicleModels.AddRange(vehicleModels);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var sut = new VehicleModelServices(actContext);
                //Act
                var result = sut.GetAll();

                //Assert
                Assert.AreEqual(vehicleModels[0].Model, result.First().Model);
                Assert.AreEqual(vehicleModels[0].Manufacturer.Name, result.First().Manufacturer);
                Assert.AreEqual(vehicleModels[0].VehicleType.Name, result.First().VehicleType);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ReturnCorrectVehicleModel()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectVehicleModel));

            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicleModels = Utils.GetVehicleModels();
            var owners = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();
            var visitStatuses = Utils.GetVisitStatuses();
            var vehicles = Utils.GetVehicles();
            var services = Utils.GetServices();
            var visitServices = Utils.GetVisitServices();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Users.AddRange(owners);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.Services.AddRange(services);
                arrangeContext.VisitServices.AddRange(visitServices);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var sut = new VehicleModelServices(actContext);
                //Act
                var result = await sut.Get("330");
                //Assert
                Assert.AreEqual(vehicleModels[0].Model, result.Model);
                Assert.AreEqual(vehicleModels[0].Manufacturer.Name, result.Manufacturer);
                Assert.AreEqual(vehicleModels[0].VehicleType.Name, result.VehicleType);

                actContext.Database.EnsureDeleted();
            }
        }


        [TestMethod]
        public async Task ThrowsExceptionWhenVehicleModelNotFound()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowsExceptionWhenVehicleModelNotFound));

            using (var actContext = new CarDoctorsContext(options))
            {
                var sut = new VehicleModelServices(actContext);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ObjectNotFoundException>(() => sut.Get("test"));

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
