﻿using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace CarDoctors.Tests.ServicesTests.VehicleModelServicesTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public async Task ReturnCorrectEntity()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectEntity));

            string model = "Enzo";
            string manufacturer = "Ferrari";

            var vehicleTypes = Utils.GetVehicleTypes();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var sut = new VehicleModelServices(actContext);
                //Act
                var result = await sut.Create(manufacturer, model, vehicleTypes[0].Name);

                //Assert
                Assert.AreEqual(model, result.Model);
                Assert.AreEqual(manufacturer, result.Manufacturer.Name);
                Assert.AreEqual(vehicleTypes[0].Name, result.VehicleType.Name);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ThrowWhenCreatingDuplicateVehicleModel()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowWhenCreatingDuplicateVehicleModel));

            string manufacturer = "Ferrari";

            var vehicleModels = Utils.GetVehicleModels();
            var vehicleTypes = Utils.GetVehicleTypes();


            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var sut = new VehicleModelServices(actContext);
                //Act && Assert              

                await Assert.ThrowsExceptionAsync<ObjectDuplicateException>(() => sut.Create(manufacturer, vehicleModels[0].Model, vehicleTypes[0].Name));

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ThrowWhenCreatingWhenVehicleTypeIsNull()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowWhenCreatingWhenVehicleTypeIsNull));

            string model = "Enzo";
            string manufacturer = "Ferrari";

            var vehicleModels = Utils.GetVehicleModels();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.VehicleModels.AddRange(vehicleModels);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var sut = new VehicleModelServices(actContext);
                //Act && Assert              

                await Assert.ThrowsExceptionAsync<ObjectNullException>(() => sut.Create(manufacturer, model, null));

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
