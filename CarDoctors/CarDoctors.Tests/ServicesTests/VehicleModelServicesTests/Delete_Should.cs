﻿using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace CarDoctors.Tests.ServicesTests.VehicleModelServicesTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void CorrectlyDeleteVehicleModel_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(CorrectlyDeleteVehicleModel_When_ParamsAreValid));

            var vehicleModels = Utils.GetVehicleModels();


            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.VehicleModels.AddRange(vehicleModels);

                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new CarDoctorsContext(options))
            {
                var sut = new VehicleModelServices(actContext);

                sut.Delete(1);
            }
            //Assert
            using (var assertContext = new CarDoctorsContext(options))
            {
                var actual = assertContext.VehicleModels.FirstOrDefault(x => x.VehicleModelID == 1);
                Assert.IsNull(actual);

                assertContext.Database.EnsureDeleted();
            }
        }


        [TestMethod]
        public void ThrowsWhenInvalidVehicleModel()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowsWhenInvalidVehicleModel));

            using (var actContext = new CarDoctorsContext(options))
            {
                var sut = new VehicleModelServices(actContext);

                Assert.ThrowsException<ObjectNotFoundException>(() => sut.Delete(1));

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
