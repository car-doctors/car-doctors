﻿using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Helpers;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Services;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace CarDoctors.Tests.ServicesTests.UserServicesTests
{
    [TestClass]
    public class FilterUsers_Should
    {
        [TestMethod]
        public void ReturnCorrectWhenFIlteringUserByFirstName()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenFIlteringUserByFirstName));

            var users = Utils.GetUsers();
            var userRoles = Utils.GetUserRoles();
            var roles = Utils.GetRoles();

            var filter = new FilterUser
            {
                FirstName = users[0].FirstName
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                var sut = new UserService(appSettings, actContext);
                //Act
                var result = sut.FilterUsers(filter);
                //Assert
                Assert.AreEqual(users[0].FirstName, result.First().FirstName);
                Assert.AreEqual(users[0].LastName, result.First().LastName);
                Assert.AreEqual(users[0].Email, result.First().Email);
                Assert.AreEqual(users[0].Phone, result.First().Phone);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ReturnCorrectWhenFIlteringUserByLastName()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenFIlteringUserByLastName));

            var users = Utils.GetUsers();
            var userRoles = Utils.GetUserRoles();
            var roles = Utils.GetRoles();

            var filter = new FilterUser
            {
                LastName = users[0].LastName
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                var sut = new UserService(appSettings, actContext);
                //Act
                var result = sut.FilterUsers(filter);
                //Assert
                Assert.AreEqual(users[0].FirstName, result.First().FirstName);
                Assert.AreEqual(users[0].LastName, result.First().LastName);
                Assert.AreEqual(users[0].Email, result.First().Email);
                Assert.AreEqual(users[0].Phone, result.First().Phone);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ReturnCorrectWhenFIlteringUserByEmail()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenFIlteringUserByEmail));

            var users = Utils.GetUsers();
            var userRoles = Utils.GetUserRoles();
            var roles = Utils.GetRoles();

            var filter = new FilterUser
            {
                Email = users[0].Email
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                var sut = new UserService(appSettings, actContext);
                //Act
                var result = sut.FilterUsers(filter);
                //Assert
                Assert.AreEqual(users[0].FirstName, result.First().FirstName);
                Assert.AreEqual(users[0].LastName, result.First().LastName);
                Assert.AreEqual(users[0].Email, result.First().Email);
                Assert.AreEqual(users[0].Phone, result.First().Phone);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ReturnCorrectWhenFIlteringUserByPhoneNumber()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenFIlteringUserByPhoneNumber));

            var users = Utils.GetUsers();
            var userRoles = Utils.GetUserRoles();
            var roles = Utils.GetRoles();

            var filter = new FilterUser
            {
                Phone = users[0].Phone
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                var sut = new UserService(appSettings, actContext);
                //Act
                var result = sut.FilterUsers(filter);
                //Assert
                Assert.AreEqual(users[0].FirstName, result.First().FirstName);
                Assert.AreEqual(users[0].LastName, result.First().LastName);
                Assert.AreEqual(users[0].Email, result.First().Email);
                Assert.AreEqual(users[0].Phone, result.First().Phone);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ReturnCorrectWhenFIlteringUserByVehicleModel()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenFIlteringUserByVehicleModel));

            var users = Utils.GetUsers();
            var userRoles = Utils.GetUserRoles();
            var roles = Utils.GetRoles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();


            var filter = new FilterUser
            {
                VehicleModel = vehicleModels[0].Model
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                var sut = new UserService(appSettings, actContext);
                //Act
                var result = sut.FilterUsers(filter);
                //Assert
                Assert.AreEqual(users[0].FirstName, result.First().FirstName);
                Assert.AreEqual(users[0].LastName, result.First().LastName);
                Assert.AreEqual(users[0].Email, result.First().Email);
                Assert.AreEqual(users[0].Phone, result.First().Phone);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ReturnCorrectWhenFIlteringUserByManufacturer()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenFIlteringUserByManufacturer));

            var users = Utils.GetUsers();
            var userRoles = Utils.GetUserRoles();
            var roles = Utils.GetRoles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();


            var filter = new FilterUser
            {
                VehicleManufacturer = manufacturers[0].Name
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                var sut = new UserService(appSettings, actContext);
                //Act
                var result = sut.FilterUsers(filter);
                //Assert
                Assert.AreEqual(users[0].FirstName, result.First().FirstName);
                Assert.AreEqual(users[0].LastName, result.First().LastName);
                Assert.AreEqual(users[0].Email, result.First().Email);
                Assert.AreEqual(users[0].Phone, result.First().Phone);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ReturnCorrectWhenFIlteringUserByVehicleType()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenFIlteringUserByVehicleType));

            var users = Utils.GetUsers();
            var userRoles = Utils.GetUserRoles();
            var roles = Utils.GetRoles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var visits = Utils.GetVisits();
            var visitServices = Utils.GetVisitServices();
            var visitStatuses = Utils.GetVisitStatuses();

            var filter = new FilterUser
            {
                VehicleType = vehicleTypes[0].Name
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.Visits.AddRange(visits);
                arrangeContext.VisitServices.AddRange(visitServices);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                var sut = new UserService(appSettings, actContext);
                //Act
                var result = sut.FilterUsers(filter);
                //Assert
                Assert.AreEqual(users[0].FirstName, result.First().FirstName);
                Assert.AreEqual(users[0].LastName, result.First().LastName);
                Assert.AreEqual(users[0].Email, result.First().Email);
                Assert.AreEqual(users[0].Phone, result.First().Phone);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ReturnCorrectWhenFIlteringUserByVisitFrom()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenFIlteringUserByVisitFrom));

            var users = Utils.GetUsers();
            var userRoles = Utils.GetUserRoles();
            var roles = Utils.GetRoles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var visits = Utils.GetVisits();
            var visitServices = Utils.GetVisitServices();
            var visitStatuses = Utils.GetVisitStatuses();

            var filter = new FilterUser
            {
                VisitFrom = visits[0].VisitDate.AddDays(-5)
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.Visits.AddRange(visits);
                arrangeContext.VisitServices.AddRange(visitServices);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                var sut = new UserService(appSettings, actContext);
                //Act
                var result = sut.FilterUsers(filter);
                //Assert
                Assert.AreEqual(users[0].FirstName, result.First().FirstName);
                Assert.AreEqual(users[0].LastName, result.First().LastName);
                Assert.AreEqual(users[0].Email, result.First().Email);
                Assert.AreEqual(users[0].Phone, result.First().Phone);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ReturnCorrectWhenFIlteringUserByVisitTo()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenFIlteringUserByVisitTo));

            var users = Utils.GetUsers();
            var userRoles = Utils.GetUserRoles();
            var roles = Utils.GetRoles();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var visits = Utils.GetVisits();
            var visitServices = Utils.GetVisitServices();
            var visitStatuses = Utils.GetVisitStatuses();

            var filter = new FilterUser
            {
                VisitTo = visits[0].VisitDate.AddDays(-5)
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);
                arrangeContext.Visits.AddRange(visits);
                arrangeContext.VisitServices.AddRange(visitServices);
                arrangeContext.VisitStatuses.AddRange(visitStatuses);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                var sut = new UserService(appSettings, actContext);
                //Act
                var result = sut.FilterUsers(filter);
                //Assert
                Assert.AreEqual(users[0].FirstName, result.First().FirstName);
                Assert.AreEqual(users[0].LastName, result.First().LastName);
                Assert.AreEqual(users[0].Email, result.First().Email);
                Assert.AreEqual(users[0].Phone, result.First().Phone);

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
