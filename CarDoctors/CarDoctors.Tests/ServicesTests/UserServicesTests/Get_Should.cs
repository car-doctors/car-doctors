﻿using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Helpers;
using CarDoctors.Services.Services;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace CarDoctors.Tests.ServicesTests.UserServicesTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhenGettingUserByEmailAsync()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenGettingUserByEmailAsync));

            var users = Utils.GetUsers();
            var userRoles = Utils.GetUserRoles();
            var roles = Utils.GetRoles();


            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                var sut = new UserService(appSettings, actContext);
                //Act
                var result = await sut.GetUserByEmailAsync(users[0].Email);
                //Assert
                Assert.AreEqual(users[0].FirstName, result.FirstName);
                Assert.AreEqual(users[0].LastName, result.LastName);
                Assert.AreEqual(users[0].Email, result.Email);
                Assert.AreEqual(users[0].Phone, result.Phone);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ThrowsExceptionWhenUserNotFoundWhenGettingUserByEmailAsync()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowsExceptionWhenUserNotFoundWhenGettingUserByEmailAsync));

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                var sut = new UserService(appSettings, actContext);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ObjectNotFoundException>(() => sut.GetUserByEmailAsync(null));

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ReturnCorrectWhenGettingUserByEmail()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenGettingUserByEmail));

            var users = Utils.GetUsers();
            var userRoles = Utils.GetUserRoles();
            var roles = Utils.GetRoles();


            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                var sut = new UserService(appSettings, actContext);
                //Act
                var result = sut.GetUserByEmail(users[0].Email);
                //Assert
                Assert.AreEqual(users[0].FirstName, result.FirstName);
                Assert.AreEqual(users[0].LastName, result.LastName);
                Assert.AreEqual(users[0].Email, result.Email);
                Assert.AreEqual(users[0].Phone, result.Phone);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowsExceptionWhenUserNotFoundWhenGettingUserByEmail()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowsExceptionWhenUserNotFoundWhenGettingUserByEmail));

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                var sut = new UserService(appSettings, actContext);

                //Act & Assert
                Assert.ThrowsException<ObjectNotFoundException>(() => sut.GetUserByEmail("test"));

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ReturnCorrectWhenGettingUserById()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenGettingUserById));

            var users = Utils.GetUsers();
            var userRoles = Utils.GetUserRoles();
            var roles = Utils.GetRoles();


            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                var sut = new UserService(appSettings, actContext);
                //Act
                var result = sut.GetById(users[0].Id);
                //Assert
                Assert.AreEqual(users[0].FirstName, result.FirstName);
                Assert.AreEqual(users[0].LastName, result.LastName);
                Assert.AreEqual(users[0].Email, result.Email);
                Assert.AreEqual(users[0].Phone, result.Phone);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ReturnCorrectWhenGettingAllUsers()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenGettingAllUsers));

            var users = Utils.GetUsers();
            var userRoles = Utils.GetUserRoles();
            var roles = Utils.GetRoles();


            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.Roles.AddRange(roles);
                arrangeContext.UserRoles.AddRange(userRoles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                var sut = new UserService(appSettings, actContext);
                //Act
                var result = sut.GetAll();
                //Assert
                Assert.AreEqual(users[0].FirstName, result.First().FirstName);
                Assert.AreEqual(users[0].LastName, result.First().LastName);
                Assert.AreEqual(users[0].Email, result.First().Email);
                Assert.AreEqual(users[0].Phone, result.First().Phone);

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
