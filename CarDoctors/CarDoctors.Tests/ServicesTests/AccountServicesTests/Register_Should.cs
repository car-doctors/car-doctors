﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Data.Models;
using CarDoctors.Services.Helpers;
using CarDoctors.Services.Services;
using CarDoctors.Services.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace CarDoctors.Tests.ServicesTests.AccountServicesTests
{
    [TestClass]
    public class Register_Should
    {
        [TestMethod]
        public async Task ReturnRegisteredUser()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnRegisteredUser));


            var roles = Utils.GetRoles();

            var user = new User
            {
                FirstName = "Antoan",
                LastName = "Vassilev",
                Email = "a.v@abv.bg",
                Username = "a.v@abv.bg",
                Phone = "0889677576",
            };

            var userRole = new UserRole()
            {
                RoleID = 2,
                UserID = 2
            };

            string password = "Antoan00";

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Roles.AddRange(roles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                //IUserService userService, IOptions<AppSettings> appSettings, IRoleServices roleServices
                var mockUser = new Mock<IUserService>();

                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                var mockRoles = new Mock<IRoleServices>();

                var role = await actContext.Roles.FirstOrDefaultAsync(u => u.RoleID == 2);

                mockRoles.Setup(p => p.GetByNameAsync("Customer")).ReturnsAsync(role);

                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                IUserService userService = mockUser.Object;

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                IRoleServices roleServices = mockRoles.Object;

                var sut = new AccountServices(actContext, userService, appSettings, roleServices, mapper);
                //Act
                var result = await sut.Register(user, password);
                //Assert
                Assert.AreEqual(user.Email, result.Email);
                Assert.AreEqual(user.FirstName, result.FirstName);
                Assert.AreEqual(user.LastName, result.LastName);
                Assert.AreEqual(user.Phone, result.Phone);

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
