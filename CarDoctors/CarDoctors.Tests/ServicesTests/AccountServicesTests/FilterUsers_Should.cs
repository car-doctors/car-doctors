﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.MVC.Infrastructure;
using CarDoctors.Services.Helpers;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Services;
using CarDoctors.Services.Services.Contracts;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace CarDoctors.Tests.ServicesTests.AccountServicesTests
{
    [TestClass]
    public class FilterUsers_Should
    {
        [TestMethod]
        public void ReturnCorrectWhenFilteringUsers()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenFilteringUsers));

            var users = Utils.GetUsers();
            var vehicleModels = Utils.GetVehicleModels();
            var manufacturers = Utils.GetManufacturers();
            var vehicleTypes = Utils.GetVehicleTypes();
            var vehicles = Utils.GetVehicles();
            var visits = Utils.GetVisits();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.VehicleModels.AddRange(vehicleModels);
                arrangeContext.Manufacturers.AddRange(manufacturers);
                arrangeContext.VehicleTypes.AddRange(vehicleTypes);
                arrangeContext.Vehicles.AddRange(vehicles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockUser = new Mock<IUserService>();

                mockUser.Setup(p => p.GetUserByEmailAsync(users[0].Email)).ReturnsAsync(users[0]);

                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                var mockRoles = new Mock<IRoleServices>();

                var myProfile = new MappingProfile();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                IMapper mapper = new Mapper(configuration);

                IUserService userService = mockUser.Object;

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                IRoleServices roleServices = mockRoles.Object;

                var filterServices = new FilterUser()
                {
                    FirstName = users[0].FirstName,
                    LastName = users[0].LastName,
                    Email = users[0].Email,
                    Phone = users[0].Phone,
                    VehicleModel = vehicleModels[0].Model,
                    VehicleManufacturer = vehicleModels[0].Manufacturer.Name,
                    VehicleType = vehicleModels[0].VehicleType.Name,
                    //VisitFrom = visits[0].VisitDate,
                    //VisitTo=visits[0].PickUpDate
                };

                var sut = new AccountServices(actContext, userService, appSettings, roleServices, mapper);

                //Act
                var result = sut.FilterUsers(filterServices);
                //Assert
                Assert.AreEqual(users[0].Email, result.First().Email);
                Assert.AreEqual(users[0].FirstName, result.First().FirstName);
                Assert.AreEqual(users[0].LastName, result.First().LastName);
                Assert.AreEqual(users[0].Phone, result.First().Phone);

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
