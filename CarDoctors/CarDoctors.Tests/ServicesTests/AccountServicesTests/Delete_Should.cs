﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Helpers;
using CarDoctors.Services.Services;
using CarDoctors.Services.Services.Contracts;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace CarDoctors.Tests.ServicesTests.AccountServicesTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public async Task CorrectlyDeleteUser_When_ParamsAreValid()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(CorrectlyDeleteUser_When_ParamsAreValid));

            var users = Utils.GetUsers();
            var roles = Utils.GetRoles();
            var userRoles = Utils.GetUserRoles();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);
                arrangeContext.UserRoles.AddRange(userRoles);
                arrangeContext.Roles.AddRange(roles);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                //IUserService userService, IOptions<AppSettings> appSettings, IRoleServices roleServices
                var mockUser = new Mock<IUserService>();

                mockUser.Setup(p => p.GetUserByEmailAsync(users[0].Email)).ReturnsAsync(users[0]);

                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                var mockRoles = new Mock<IRoleServices>();

                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                IUserService userService = mockUser.Object;

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                IRoleServices roleServices = mockRoles.Object;

                var sut = new AccountServices(actContext, userService, appSettings, roleServices, mapper);
                //Act
                await sut.DeleteAsync(1);
            }
            //Assert
            using (var assertContext = new CarDoctorsContext(options))
            {
                var actual = assertContext.Users.FirstOrDefault(x => x.Id == 1);
                Assert.IsNull(actual);
            }
        }
    }
}
