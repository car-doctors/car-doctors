﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Helpers;
using CarDoctors.Services.Services;
using CarDoctors.Services.Services.Contracts;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace CarDoctors.Tests.ServicesTests.AccountServicesTests
{
    [TestClass]
    public class ChangePassword_Should
    {
        //"qE2vIsTjxppOreWdGMllHqKYTkxNZCjam9xZ7hdMwNReFCumP+doVkYQFnqKA08j"

        [TestMethod]
        public async Task ReturnCorrectWhenChangingPassword()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenChangingPassword));

            var users = Utils.GetUsers();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                //IUserService userService, IOptions<AppSettings> appSettings, IRoleServices roleServices
                var mockUser = new Mock<IUserService>();

                mockUser.Setup(p => p.GetUserByEmailAsync(users[0].Email)).ReturnsAsync(users[0]);

                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                var mockRoles = new Mock<IRoleServices>();

                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                IUserService userService = mockUser.Object;

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                IRoleServices roleServices = mockRoles.Object;

                string oldPassword = "Georgi00";

                string newPassword = "NewPassword";

                var sut = new AccountServices(actContext, userService, appSettings, roleServices, mapper);
                //Act
                var result = await sut.ChangePassword(users[0].Email, oldPassword, newPassword);
                //Assert
                Assert.AreEqual(users[0].Email, result.Email);
                Assert.AreEqual(users[0].FirstName, result.FirstName);
                Assert.AreEqual(users[0].LastName, result.LastName);
                Assert.AreEqual(users[0].Phone, result.Phone);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ThrowWhenInvalidPasswordChangeRequest()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowWhenInvalidPasswordChangeRequest));

            var users = Utils.GetUsers();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockUser = new Mock<IUserService>();

                mockUser.Setup(p => p.GetUserByEmailAsync(users[0].Email)).ReturnsAsync(users[0]);

                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                var mockRoles = new Mock<IRoleServices>();

                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                IUserService userService = mockUser.Object;

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                IRoleServices roleServices = mockRoles.Object;

                string oldPassword = "FakePassword";

                string newPassword = "NewPassword";

                var sut = new AccountServices(actContext, userService, appSettings, roleServices, mapper);
                //Act
                //Assert
                await Assert.ThrowsExceptionAsync<InvalidPasswordResetException>(() => sut.ChangePassword(users[0].Email, oldPassword, newPassword));

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
