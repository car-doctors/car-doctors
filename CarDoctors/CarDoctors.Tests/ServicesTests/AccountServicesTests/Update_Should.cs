﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.MVC.Infrastructure;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Helpers;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Services;
using CarDoctors.Services.Services.Contracts;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace CarDoctors.Tests.ServicesTests.AccountServicesTests
{
    [TestClass]
    public class Update_Should
    {

        [TestMethod]
        public async Task ReturnCorrectServiceWhenUpdatingService()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectServiceWhenUpdatingService));

            var users = Utils.GetUsers();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockUser = new Mock<IUserService>();

                mockUser.Setup(p => p.GetUserByEmailAsync(users[0].Email)).ReturnsAsync(users[0]);

                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                var mockRoles = new Mock<IRoleServices>();

                var myProfile = new MappingProfile();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                IMapper mapper = new Mapper(configuration);

                IUserService userService = mockUser.Object;

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                IRoleServices roleServices = mockRoles.Object;

                var userInputModel = new UserViewInputModel
                {
                    Email = users[0].Email,
                    FirstName = users[0].FirstName,
                    LastName = users[0].LastName,
                    Phone = users[0].Phone
                };

                var sut = new AccountServices(actContext, userService, appSettings, roleServices, mapper);

                //Act
                var result = await sut.UpdateAsync(userInputModel, users[0].Email);
                //Assert
                Assert.AreEqual(users[0].Email, result.Email);
                Assert.AreEqual(users[0].FirstName, result.FirstName);
                Assert.AreEqual(users[0].LastName, result.LastName);
                Assert.AreEqual(users[0].Phone, result.Phone);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ThrowWhenServiceNotFound()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectServiceWhenUpdatingService));

            var users = Utils.GetUsers();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockUser = new Mock<IUserService>();

                mockUser.Setup(p => p.GetUserByEmailAsync(users[0].Email)).ReturnsAsync(users[0]);

                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                var mockRoles = new Mock<IRoleServices>();

                var myProfile = new MappingProfile();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                IMapper mapper = new Mapper(configuration);

                IUserService userService = mockUser.Object;

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                IRoleServices roleServices = mockRoles.Object;

                var userInputModel = new UserViewInputModel
                {
                    Email = users[0].Email,
                    FirstName = users[0].FirstName,
                    LastName = users[0].LastName,
                    Phone = users[0].Phone
                };

                var sut = new AccountServices(actContext, userService, appSettings, roleServices, mapper);
                //Act && Assert

                await Assert.ThrowsExceptionAsync<ObjectNotFoundException>(() => sut.UpdateAsync(userInputModel, null));

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
