﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Helpers;
using CarDoctors.Services.Models.AccountModels;
using CarDoctors.Services.Services;
using CarDoctors.Services.Services.Contracts;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace CarDoctors.Tests.ServicesTests.AccountServicesTests
{
    [TestClass]
    public class Login_Should
    {
        [TestMethod]
        public async Task ReturnCorrectWhenLogingIn()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenLogingIn));

            var users = Utils.GetUsers();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                //IUserService userService, IOptions<AppSettings> appSettings, IRoleServices roleServices
                var mockUser = new Mock<IUserService>();

                mockUser.Setup(p => p.GetUserByEmailAsync(users[0].Email)).ReturnsAsync(users[0]);

                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                var mockRoles = new Mock<IRoleServices>();

                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                IUserService userService = mockUser.Object;

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                IRoleServices roleServices = mockRoles.Object;

                string password = "Georgi00";

                var sut = new AccountServices(actContext, userService, appSettings, roleServices, mapper);
                //Act
                var result = await sut.Login(users[0].Email, password);
                //Assert
                Assert.AreEqual(users[0].Email, result.Email);
                Assert.AreEqual(users[0].FirstName, result.FirstName);
                Assert.AreEqual(users[0].LastName, result.LastName);
                Assert.AreEqual(users[0].Phone, result.Phone);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ThrowWhenIncorrectPassword()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowWhenIncorrectPassword));

            var users = Utils.GetUsers();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                //IUserService userService, IOptions<AppSettings> appSettings, IRoleServices roleServices
                var mockUser = new Mock<IUserService>();

                mockUser.Setup(p => p.GetUserByEmailAsync(users[0].Email)).ReturnsAsync(users[0]);

                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                var mockRoles = new Mock<IRoleServices>();

                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                IUserService userService = mockUser.Object;

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                IRoleServices roleServices = mockRoles.Object;

                string password = "WrongPassword";

                var sut = new AccountServices(actContext, userService, appSettings, roleServices, mapper);
                //Act & Assert 
                await Assert.ThrowsExceptionAsync<InvaludUsernameOrPasswordException>(() => sut.Login(users[0].Email, password));

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ReturnCorrectWhenUsingLogInModel()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenUsingLogInModel));

            var users = Utils.GetUsers();

            var loginModel = new LoginModel
            {
                Email = users[0].Email,
                Password = "Georgi00"
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                //IUserService userService, IOptions<AppSettings> appSettings, IRoleServices roleServices
                var mockUser = new Mock<IUserService>();

                mockUser.Setup(p => p.GetUserByEmailAsync(users[0].Email)).ReturnsAsync(users[0]);

                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                var mockRoles = new Mock<IRoleServices>();

                var mockMapper = new Mock<IMapper>();

                IMapper mapper = mockMapper.Object;

                IUserService userService = mockUser.Object;

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                IRoleServices roleServices = mockRoles.Object;

                var sut = new AccountServices(actContext, userService, appSettings, roleServices, mapper);
                //Act
                var result = await sut.Login(loginModel.Email, loginModel.Password);
                //Assert
                Assert.AreEqual(users[0].Email, result.Email);
                Assert.AreEqual(users[0].FirstName, result.FirstName);
                Assert.AreEqual(users[0].LastName, result.LastName);
                Assert.AreEqual(users[0].Phone, result.Phone);

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
