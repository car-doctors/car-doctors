﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.MVC.Infrastructure;
using CarDoctors.Services.Helpers;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.Services.Services;
using CarDoctors.Services.Services.Contracts;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace CarDoctors.Tests.ServicesTests.AccountServicesTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public async Task ReturnCorrectUsersWithGetPaginateAsync()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectUsersWithGetPaginateAsync));

            var users = Utils.GetUsers();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Users.AddRange(users);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var mockUser = new Mock<IUserService>();

                mockUser.Setup(p => p.GetUserByEmailAsync(users[0].Email)).ReturnsAsync(users[0]);

                var mockAppSettings = new Mock<IOptions<AppSettings>>();

                var mockRoles = new Mock<IRoleServices>();

                var myProfile = new MappingProfile();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                IMapper mapper = new Mapper(configuration);

                IUserService userService = mockUser.Object;

                IOptions<AppSettings> appSettings = mockAppSettings.Object;

                IRoleServices roleServices = mockRoles.Object;

                var filterServices = new FilterUser()
                {
                    Email = users[0].Email
                };

                var optionsModel = new PagingOptionsModelUser()
                {
                    Page = 1,
                    PageSize = 1,
                    SortBy = null,
                    SortInDescendingOrder = false
                };

                var sut = new AccountServices(actContext, userService, appSettings, roleServices, mapper);

                //Act
                var result = await sut.GetPaginatedAsync(optionsModel, filterServices);
                //Assert
                Assert.AreEqual(users[0].Email, result.Elements.First().Email);
                Assert.AreEqual(users[0].FirstName, result.Elements.First().FirstName);
                Assert.AreEqual(users[0].LastName, result.Elements.First().LastName);
                Assert.AreEqual(users[0].Phone, result.Elements.First().Phone);

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
