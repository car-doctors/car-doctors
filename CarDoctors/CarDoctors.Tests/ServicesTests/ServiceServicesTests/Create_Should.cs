﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace CarDoctors.Tests.ServicesTests.ServiceServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public async Task ReturnCorrectEntity()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectEntity));

            var service = new ServiceInputModel
            {
                Name = "Test",
                Currency = "BGN",
                Price = 100
            };

            using (var actContext = new CarDoctorsContext(options))
            {
                Mock<IMapper> mock = new Mock<IMapper>();

                var mapper = mock.Object;

                var sut = new ServiceServices(actContext, mapper);
                //Act
                var result = await sut.Create(service);

                //Assert
                Assert.AreEqual(service.Name, result.ServiceName);
                Assert.AreEqual(service.Currency, result.Currency);
                Assert.AreEqual(service.Price, result.Price);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ThrowWhenCreatingDuplicateService()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowWhenCreatingDuplicateService));

            var services = Utils.GetServices();


            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Services.AddRange(services);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                Mock<IMapper> mock = new Mock<IMapper>();

                var mapper = mock.Object;

                var sut = new ServiceServices(actContext, mapper);
                //Act && Assert
                var serviceInputModel = new ServiceInputModel
                {
                    Name = services[0].ServiceName,
                    Currency = services[0].Currency,
                    Price = services[0].Price
                };

                await Assert.ThrowsExceptionAsync<ObjectDuplicateException>(() => sut.Create(serviceInputModel));

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ReturnCorrectWhenCreatingWhenServiceIsDeleted()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectWhenCreatingWhenServiceIsDeleted));

            var services = Utils.GetServices();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Services.AddRange(services);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                Mock<IMapper> mock = new Mock<IMapper>();

                var mapper = mock.Object;

                var sut = new ServiceServices(actContext, mapper);

                var serviceInputModel = new ServiceInputModel
                {
                    Name = services[0].ServiceName,
                    Currency = services[0].Currency,
                    Price = services[0].Price
                };

                sut.Delete(1);
                //Act 

                var result = await sut.Create(serviceInputModel);
                //Assert
                Assert.AreEqual(services[0].ServiceName, result.ServiceName);
                Assert.AreEqual(services[0].Currency, result.Currency);
                Assert.AreEqual(services[0].Price, result.Price);

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
