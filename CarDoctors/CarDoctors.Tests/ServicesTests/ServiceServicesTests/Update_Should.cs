﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace CarDoctors.Tests.ServicesTests.ServiceServiceTests
{
    [TestClass]
    public class Update_Should
    {
        [TestMethod]
        public async Task ReturnCorrectServiceWhenUpdatingService()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectServiceWhenUpdatingService));

            var services = Utils.GetServices();

            var service = new ServiceInputModel
            {
                Name = "Test",
                Currency = "BGN",
                Price = 100
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Services.AddRange(services);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                Mock<IMapper> mock = new Mock<IMapper>();

                var mapper = mock.Object;

                var sut = new ServiceServices(actContext, mapper);
                //Act
                var result = await sut.Update(service, 1);

                //Assert
                Assert.AreEqual(service.Name, result.ServiceName);
                Assert.AreEqual(service.Currency, result.Currency);
                Assert.AreEqual(service.Price, result.Price);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ThrowWhenServiceNotFound()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowWhenServiceNotFound));

            var services = Utils.GetServices();

            var service = new ServiceInputModel
            {
                Name = "Test",
                Currency = "BGN",
                Price = 100
            };

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Services.AddRange(services);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                Mock<IMapper> mock = new Mock<IMapper>();

                var mapper = mock.Object;

                var sut = new ServiceServices(actContext, mapper);
                //Act && Assert

                await Assert.ThrowsExceptionAsync<ObjectNotFoundException>(() => sut.Update(service, 0));

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
