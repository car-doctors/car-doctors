﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace CarDoctors.Tests.ServicesTests.ServiceServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void CorrectlyDeleteServices_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(CorrectlyDeleteServices_When_ParamsAreValid));


            var services = Utils.GetServices();


            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Services.AddRange(services);

                arrangeContext.SaveChanges();
            }
            //Act
            using (var actContext = new CarDoctorsContext(options))
            {
                Mock<IMapper> mock = new Mock<IMapper>();

                var mapper = mock.Object;

                var sut = new ServiceServices(actContext, mapper);

                sut.Delete(1);
            }
            //Assert
            using (var assertContext = new CarDoctorsContext(options))
            {
                var actual = assertContext.Services.FirstOrDefault(x => x.ServiceID == 1);
                Assert.IsNull(actual);

                assertContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowsWhenInvalidService()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowsWhenInvalidService));

            using (var actContext = new CarDoctorsContext(options))
            {
                Mock<IMapper> mock = new Mock<IMapper>();

                var mapper = mock.Object;

                var sut = new ServiceServices(actContext, mapper);

                Assert.ThrowsException<ObjectNotFoundException>(() => sut.Delete(1));

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
