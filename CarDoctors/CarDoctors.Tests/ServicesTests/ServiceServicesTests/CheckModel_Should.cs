﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace CarDoctors.Tests.ServicesTests.ServiceServiceTests
{
    [TestClass]
    public class CheckModel_Should
    {
        [TestMethod]
        public async Task ThrowsWhenNullName()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowsWhenNullName));

            var service = new ServiceInputModel();

            using (var actContext = new CarDoctorsContext(options))
            {
                Mock<IMapper> mock = new Mock<IMapper>();

                var mapper = mock.Object;

                var sut = new ServiceServices(actContext, mapper);

                await Assert.ThrowsExceptionAsync<ObjectNullException>(() => sut.Create(service));

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ThrowsWhenPriceIsNegative()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowsWhenNullName));

            var service = new ServiceInputModel
            {
                Name = "Test",
                Currency = "BGN",
                Price = -1
            };

            using (var actContext = new CarDoctorsContext(options))
            {
                Mock<IMapper> mock = new Mock<IMapper>();

                var mapper = mock.Object;

                var sut = new ServiceServices(actContext, mapper);

                await Assert.ThrowsExceptionAsync<NegativePriceException>(() => sut.Create(service));

                actContext.Database.EnsureDeleted();
            }
        }
    }
}
