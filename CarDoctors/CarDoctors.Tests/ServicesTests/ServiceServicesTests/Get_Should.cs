﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.MVC.Infrastructure;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace CarDoctors.Tests.ServicesTests.ServiceServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public async Task ReturnCorrectService()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectService));

            var services = Utils.GetServices();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Services.AddRange(services);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                Mock<IMapper> mock = new Mock<IMapper>();

                var mapper = mock.Object;

                var sut = new ServiceServices(actContext, mapper);
                //Act
                var result = await sut.Get(1);
                //Assert
                Assert.AreEqual(services[0].ServiceName, result.ServiceName);
                Assert.AreEqual(services[0].Currency, result.Currency);
                Assert.AreEqual(services[0].Price, result.Price);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task Throw_When_ServiceNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_ServiceNotFound));

            using (var context = new CarDoctorsContext(options))
            {
                Mock<IMapper> mock = new Mock<IMapper>();

                var mapper = mock.Object;

                var sut = new ServiceServices(context, mapper);
                //Act & Assert
                await Assert.ThrowsExceptionAsync<ObjectNotFoundException>(() => sut.Get(1));

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ReturnCorrectCollectionOfServices()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectCollectionOfServices));

            var services = Utils.GetServices();
            var filter = Utils.GetFilterParcel();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Services.AddRange(services);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                Mock<IMapper> mock = new Mock<IMapper>();

                var mapper = mock.Object;

                var sut = new ServiceServices(actContext, mapper);
                //Act
                var result = await sut.GetAll(filter[0], "BGN");

                //Assert
                Assert.AreEqual(services[0].ServiceName, result.First().ServiceName);
                Assert.AreEqual(services[0].Currency, result.First().Currency);
                Assert.AreEqual(services[0].Price, result.First().Price);

                actContext.Database.EnsureDeleted();
            }
        }


        [TestMethod]
        public async Task ReturnCorrectServicesWithGetPaginateAsync()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectServicesWithGetPaginateAsync));

            var services = Utils.GetServices();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Services.AddRange(services);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var myProfile = new MappingProfile();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                IMapper mapper = new Mapper(configuration);

                var filterServices = new FilterServices()
                {
                    Name = services[0].ServiceName,
                    Currency = "EUR"
                };

                var optionsModel = new PagingOptionsModelService()
                {
                    Page = 1,
                    PageSize = 1,
                    SortBy = null,
                    SortInDescendingOrder = false
                };

                var sut = new ServiceServices(actContext, mapper);

                //Act
                var result = await sut.GetPaginatedAsync(filterServices, optionsModel);
                //Assert
                Assert.AreEqual(services[0].ServiceName, result.Elements.First().ServiceName);
                //Assert.AreEqual(services[0].Currency, result.Elements.First().Currency);
                //Assert.AreEqual(services[0].Price, result.Elements.First().Price);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ThrowWhenReturnCannotConvertCurrency()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ThrowWhenReturnCannotConvertCurrency));

            var services = Utils.GetServices();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Services.AddRange(services);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var myProfile = new MappingProfile();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                IMapper mapper = new Mapper(configuration);

                var sut = new ServiceServices(actContext, mapper);

                //Act
                //Assert
                await Assert.ThrowsExceptionAsync<InvalidCurrencyException>(() => sut.ConvertCurrency(services[0].Currency, "TEST"));

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ReturnCorrectServicesWhenFilteringByPriceFrom()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectServicesWhenFilteringByPriceFrom));

            var services = Utils.GetServices();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Services.AddRange(services);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var myProfile = new MappingProfile();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                IMapper mapper = new Mapper(configuration);

                var filterServices = new FilterServices()
                {
                    PriceFrom = 1
                };

                var optionsModel = new PagingOptionsModelService()
                {
                    Page = 1,
                    PageSize = 1,
                    SortBy = "Price",
                    SortInDescendingOrder = false
                };

                var sut = new ServiceServices(actContext, mapper);

                //Act
                var result = await sut.GetPaginatedAsync(filterServices, optionsModel);
                //Assert
                Assert.AreEqual(services[1].ServiceName, result.Elements.First().ServiceName);
                Assert.AreEqual(services[1].Currency, result.Elements.First().Currency);
                Assert.AreEqual(services[1].Price, result.Elements.First().Price);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ReturnCorrectServicesWhenFilteringByPriceIs()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectServicesWhenFilteringByPriceIs));

            var services = Utils.GetServices();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Services.AddRange(services);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var myProfile = new MappingProfile();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                IMapper mapper = new Mapper(configuration);

                var filterServices = new FilterServices()
                {
                    PriceIs = 50
                };

                var optionsModel = new PagingOptionsModelService()
                {
                    Page = 1,
                    PageSize = 1,
                    SortBy = "Price",
                    SortInDescendingOrder = false
                };

                var sut = new ServiceServices(actContext, mapper);

                //Act
                var result = await sut.GetPaginatedAsync(filterServices, optionsModel);
                //Assert
                Assert.AreEqual(services[1].ServiceName, result.Elements.First().ServiceName);
                Assert.AreEqual(services[1].Currency, result.Elements.First().Currency);
                Assert.AreEqual(services[1].Price, result.Elements.First().Price);

                actContext.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public async Task ReturnCorrectServicesWhenFilteringByPriceTo()
        {
            //Arrange 
            var options = Utils.GetOptions(nameof(ReturnCorrectServicesWhenFilteringByPriceTo));

            var services = Utils.GetServices();

            using (var arrangeContext = new CarDoctorsContext(options))
            {
                arrangeContext.Services.AddRange(services);

                arrangeContext.SaveChanges();
            }

            using (var actContext = new CarDoctorsContext(options))
            {
                var myProfile = new MappingProfile();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                IMapper mapper = new Mapper(configuration);

                var filterServices = new FilterServices()
                {
                    PriceTo = 1000
                };

                var optionsModel = new PagingOptionsModelService()
                {
                    Page = 1,
                    PageSize = 1,
                    SortBy = "Price",
                    SortInDescendingOrder = false
                };

                var sut = new ServiceServices(actContext, mapper);

                //Act
                var result = await sut.GetPaginatedAsync(filterServices, optionsModel);
                //Assert
                Assert.AreEqual(services[1].ServiceName, result.Elements.First().ServiceName);
                Assert.AreEqual(services[1].Currency, result.Elements.First().Currency);
                Assert.AreEqual(services[1].Price, result.Elements.First().Price);

                actContext.Database.EnsureDeleted();
            }
        }
    }
}