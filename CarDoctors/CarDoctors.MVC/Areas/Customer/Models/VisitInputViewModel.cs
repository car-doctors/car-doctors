﻿namespace CarDoctors.MVC.Areas.Customer.Models
{
    public class VisitInputViewModel
    {
        public int VisitID { get; set; }
        public string Visit { get; set; }
    }
}
