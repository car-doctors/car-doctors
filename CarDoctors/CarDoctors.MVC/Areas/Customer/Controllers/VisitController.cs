﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Data.Models.Mail;
using CarDoctors.MVC.Areas.Customer.Models;
using CarDoctors.MVC.Models;
using CarDoctors.Services.Mappers;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.Services.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core.Exceptions;
using System.Threading.Tasks;

namespace CarDoctors.MVC.Areas.Customer.Controllers
{
    [Area("Customer")]
    public class VisitController : Controller
    {
        private readonly IVisitServices visitServices;
        private readonly CarDoctorsContext carDoctorsContext;
        private readonly IMailService mailService;
        private readonly IMapper mapper;

        public VisitController(IVisitServices visitServices, CarDoctorsContext carDoctorsContext, IMailService mailService, IMapper mapper)
        {
            this.visitServices = visitServices;
            this.carDoctorsContext = carDoctorsContext;
            this.mailService = mailService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> IndexAsync(PaginatedListGetModel<VisitViewModel> inputModel, string Email, int pageNumber = 1, int PageSize = 6)
        {
            try
            {
                var vehicles = this.carDoctorsContext.Vehicles.Select(x => new VehicleViewDataModel()
                {
                    VehicleID = x.VehicleID,
                    Vehicle = $"{x.VehicleModel.Manufacturer.Name} {x.VehicleModel.Model} {x.RegistrationPlate}"
                });

                ViewData["VehicleID"] = new SelectList(vehicles, "VehicleID", "Vehicle");

                var filter = new FilterVisits()
                {
                    VehicleID = inputModel.VehicleID,
                    DateFrom = inputModel.DateFrom,
                    DateTo = inputModel.DateTo
                };

                filter.Email = Email;

                var booleans = new List<SelectListItem>()
                {
                    new SelectListItem()
                    {
                        Text = "True",
                        Value = "true"
                    },
                    new SelectListItem()
                    {
                        Text = "False",
                        Value = "False"
                    }
                };

                ViewData["Sort"] = booleans;

                var optionsModel = new PagingOptionsModelVisit()
                {
                    Page = pageNumber,
                    PageSize = PageSize,
                    SortBy = inputModel.SortBy,
                    SortInDescendingOrder = inputModel.SortInDescendingOrder
                };

                var visits = await this.visitServices.GetPaginatedForCustomerAsync(optionsModel, filter);

                inputModel = mapper.Map<PaginatedListGetModel<VisitViewModel>>(visits);

                return View(inputModel);
            }
            catch (ParseException)
            {
                ModelState.AddModelError(string.Empty, "Invalid Sort By criteria");

                return View(inputModel);
            }
            catch (Exception e)
            {

                ModelState.AddModelError(string.Empty, e.Message);

                return View(inputModel);
            }
        }

        public async Task<IActionResult> Details(int id = 0)
        {
            if (id == 0)
            {
                return View();
            }

            var visit = await this.visitServices.Get(id);

            return View(visit);
        }

        public async Task<IActionResult> GeneratePDF(string email)
        {
            var user = await this.carDoctorsContext.Users.FirstOrDefaultAsync(u => u.Email == email);

            var visits = this.carDoctorsContext.Visits
                .MapVisitDTO().Where(v => v.OwnerID == user.Id)
              .Select(a => new VisitInputViewModel()
              {
                  VisitID = a.VisitID,
                  Visit = $"{a.VisitDate} {a.RegistrationPlate}"
              });

            ViewData["VisitID"] = new SelectList(visits, "VisitID", "Visit");

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> GeneratePDF(List<string> visitIds, string Email, string currency)
        {
            var fileName = await this.visitServices.GeneratePDFForCollection(string.Join((" "), visitIds), currency);

            var request = new MailRequest()
            {
                ToEmail = Email,
                Subject = "Report requested",
                Body = "This is an automaticly generated email with a pdf attachment of the requested report. Contact us for additional information.",
                FileName = fileName
            };

            await this.mailService.SendEmailAsync(request);

            var url = Url.Action("Index", "Visit", new { Area = "Customer", Email = Email });

            var visits = this.carDoctorsContext.Visits.MapVisitDTO();

            var json = Json(new { isValid = true, html = Helper.Helper.RenderRazorViewToString(this, "_ViewAll", visits) });

            return json;
        }
    }
}
