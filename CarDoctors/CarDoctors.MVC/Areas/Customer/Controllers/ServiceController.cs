﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.MVC.Models;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.Services.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarDoctors.MVC.Areas.Customer.Controllers
{
    [Area("Customer")]
    public class ServiceController : Controller
    {
        private readonly IServiceServices serviceServices;
        private readonly IMapper mapper;
        private readonly CarDoctorsContext carDoctorsContest;

        public ServiceController(CarDoctorsContext carDoctorsContest, IServiceServices serviceServices, IMapper mapper)
        {
            this.serviceServices = serviceServices;
            this.mapper = mapper;
            this.carDoctorsContest = carDoctorsContest;
        }

        [AllowAnonymous]
        public async Task<IActionResult> IndexAsync(PaginatedListGetModel<ServiceViewModel> inputModel, int pageNumber = 1, int PageSize = 6)
        {
            try
            {
                var filter = new FilterServices()
                {
                    Name = inputModel.Name,
                    PriceIs = inputModel.PriceIs,
                    PriceFrom = inputModel.PriceFrom,
                    PriceTo = inputModel.PriceTo,
                    Currency = inputModel.Currency
                };

                var optionsModel = new PagingOptionsModelService()
                {
                    Page = pageNumber,
                    PageSize = PageSize,
                    SortBy = inputModel.SortBy,
                    SortInDescendingOrder = inputModel.SortInDescendingOrder
                };

                var booleans = new List<SelectListItem>()
                {
                    new SelectListItem()
                    {
                        Text = "True",
                        Value = "true"
                    },
                    new SelectListItem()
                    {
                        Text = "False",
                        Value = "False"
                    }
                };

                ViewData["Sort"] = booleans;

                var service = await this.serviceServices.GetPaginatedAsync(filter, optionsModel);

                inputModel = mapper.Map<PaginatedListGetModel<ServiceViewModel>>(service);

                return View(inputModel);
            }
            catch (InvalidCurrencyException e)
            {
                ModelState.AddModelError(string.Empty, e.Message);

                var services = await this.serviceServices.GetPaginatedAsync(new FilterServices(), new PagingOptionsModelService());

                var service = mapper.Map<PaginatedListGetModel<ServiceViewModel>>(services);

                return View(service);
            }
            catch (Exception e)
            {
                ModelState.AddModelError(string.Empty, e.Message);

                var services = await this.serviceServices.GetPaginatedAsync(new FilterServices(), new PagingOptionsModelService());

                var service = mapper.Map<PaginatedListGetModel<ServiceViewModel>>(services);

                return View(service);
            }
        }

        [AllowAnonymous]
        public async Task<IActionResult> ClearFilter()
        {
            return RedirectToAction("Index");
        }
    }
}
