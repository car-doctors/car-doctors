﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.MVC.Models;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.Services.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarDoctors.MVC.Areas.Customer.Controllers
{
    [Area("Customer")]
    public class VehicleController : Controller
    {
        private readonly IVehicleServices vehicleServices;
        private readonly IMapper mapper;
        private readonly CarDoctorsContext carDoctorsContext;

        public VehicleController(CarDoctorsContext carDoctorsContext, IVehicleServices vehicleServices, IMapper mapper)
        {
            this.vehicleServices = vehicleServices;
            this.mapper = mapper;
            this.carDoctorsContext = carDoctorsContext;
        }

        public async Task<IActionResult> Index(PaginatedListGetModel<VehicleViewModel> model, string Email, int pageNumber = 1, int PageSize = 6)
        {
            try
            {
                if (model.SortBy == "Price")
                {
                    model.SortBy = "VIN";
                }

                var optionsModel = new PagingOptionsModelVehicle()
                {
                    Page = pageNumber,
                    PageSize = PageSize,
                    SortBy = model.SortBy,
                    SortInDescendingOrder = model.SortInDescendingOrder
                };

                var owner = await this.carDoctorsContext.Users.FirstOrDefaultAsync(x => x.Username == Email);

                var booleans = new List<SelectListItem>()
                {
                    new SelectListItem()
                    {
                        Text = "True",
                        Value = "true"
                    },
                    new SelectListItem()
                    {
                        Text = "False",
                        Value = "False"
                    }
                };

                ViewData["Sort"] = booleans;

                var vehicles = await this.vehicleServices
                    .GetPaginatedAsync(optionsModel, owner.Id);

                model = mapper.Map<PaginatedListGetModel<VehicleViewModel>>(vehicles);

                return View(model);
            }
            catch (Exception e)
            {
                ModelState.AddModelError(string.Empty, e.Message);

                var owner = await this.carDoctorsContext.Users.FirstOrDefaultAsync(x => x.Username == Email);

                var vehicles = await this.vehicleServices
                   .GetPaginatedAsync(new PagingOptionsModelVehicle(), owner.Id);

                model = mapper.Map<PaginatedListGetModel<VehicleViewModel>>(vehicles);

                return View(model);
            }
        }

        public async Task<IActionResult> Details(int id = 0)
        {
            if (id == 0)
            {
                return View();
            }

            var visit = await this.vehicleServices.Get(id);

            return View(visit);
        }
    }
}
