using CarDoctors.Services.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace CarDoctors.MVC.Areas.Identity.Pages.Account
{
    public class ChangePasswordModel : PageModel
    {
        private readonly IAccountServices accountServices;

        [BindProperty]
        public InputModel Input { get; set; }
        public string ReturnUrl { get; set; }
        public ChangePasswordModel(IAccountServices accountServices)
        {
            this.accountServices = accountServices;
        }
        public class InputModel
        {
            [Required]
            [StringLength(50, MinimumLength = 8, ErrorMessage = "Old password must be between 8 and 20 characters long.")]
            [DataType(DataType.Password)]
            public string OldPassword { get; set; }

            [Required]
            [StringLength(20, MinimumLength = 8, ErrorMessage = "New password must be between 8 and 20 characters long.")]
            [DataType(DataType.Password)]
            public string NewPassword { get; set; }

            [DataType(DataType.Password)]
            [Compare("NewPassword", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }
        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync(string email, string returnUrl = null)
        {
            if (HttpContext.User.IsInRole("Admin") || HttpContext.User.IsInRole("Employee"))
            {
                returnUrl = Url.Content("~/Admin/Home/PasswordChangeSuccess");
            }
            else
            {
                returnUrl = Url.Content("~/Customer/Home/PasswordChangeSuccess");
            }

            if (ModelState.IsValid)
            {
                await this.accountServices.ChangePassword(email, Input.OldPassword, Input.NewPassword);

                return LocalRedirect(returnUrl);
            }

            return Page();
        }
    }
}
