using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CarDoctors.MVC.Areas.Identity.Pages
{
    public class LoginModel : PageModel
    {
        private readonly IAccountServices accountServices;
        private readonly CarDoctorsContext carDoctorsContext;

        public LoginModel(IAccountServices accountServices, CarDoctorsContext carDoctorsContext)
        {
            this.accountServices = accountServices;
            this.carDoctorsContext = carDoctorsContext;
        }

        [BindProperty]
        public InputModel Input { get; set; }
        public string ReturnUrl { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            public string Email { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [StringLength(50, MinimumLength = 8, ErrorMessage = "Password must be between 8 and 20 characters long.")]
            public string Password { get; set; }
        }
        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/Customer");

            if (ModelState.IsValid)
            {
                try
                {
                    var user = await this.accountServices.Login(Input.Email, Input.Password);

                    var role = this.carDoctorsContext.UserRoles.Where(x => x.UserID == user.UserID).Select(y => y.Role).FirstOrDefault();

                    if (role.Name == "Admin" || role.Name == "Employee")
                    {
                        returnUrl = Url.Content("~/Admin");
                    }

                    var token = this.accountServices.GenerateJWTToken(user);

                    HttpContext.Session.SetString("Authorization", token);
                    HttpContext.Session.SetString("Username", user.Email);
                    return LocalRedirect(returnUrl);
                }
                catch (InvaludUsernameOrPasswordException)
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");

                    return Page();
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(string.Empty, e.Message);

                    return Page();
                }

            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
