using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace CarDoctors.MVC.Areas.Identity.Pages.Account
{
    public class ResetPassRequest : PageModel
    {
        private readonly IAccountServices accountServices;
        private readonly CarDoctorsContext carDoctorsContext;

        public ResetPassRequest(IAccountServices accountServices, CarDoctorsContext carDoctorsContext)
        {
            this.accountServices = accountServices;
            this.carDoctorsContext = carDoctorsContext;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public class InputModel
        {
            [Required]
            [DataType(DataType.Password)]
            [StringLength(20, MinimumLength = 8, ErrorMessage = "Password must be between 8 and 20 characters long.")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync([FromRoute] string email, [FromRoute] string guid, string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~");
            try
            {
                if (ModelState.IsValid)
                {
                    await this.accountServices.ResetPass(guid, email, Input.Password);

                    return Redirect("~/Home");
                }

                return Page();
            }
            catch (InvalidPasswordResetException )
            {
                ModelState.AddModelError(string.Empty, "Invalid password reset link");

                return Page();
            }
            
        }
    }
}
