using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Data.Models.Mail;
using CarDoctors.Services.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Routing;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace CarDoctors.MVC.Areas.Identity.Pages.Account
{
    public class ReqestPasswordResetModel : PageModel
    {
        private readonly IAccountServices accountServices;
        private readonly CarDoctorsContext carDoctorsContext;
        private readonly IMailService mailService;
        private readonly LinkGenerator linkGenerator;

        public ReqestPasswordResetModel(IAccountServices accountServices, CarDoctorsContext carDoctorsContext, IMailService mailService, LinkGenerator linkGenerator)
        {
            this.accountServices = accountServices;
            this.carDoctorsContext = carDoctorsContext;
            this.mailService = mailService;
            this.linkGenerator = linkGenerator;
        }
        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            public string Email { get; set; }
        }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/Home/RequestPasswordResetSuccess");

            if (ModelState.IsValid)
            {
                try
                {
                    var guid = await this.accountServices.RequestPasswordReset(Input.Email);

                    var url = this.linkGenerator.GetUriByPage(HttpContext, "/Account/ResetPass", null, new { area = "Identity", guid = guid, email = Input.Email });

                    var message = new MailRequest()
                    {
                        ToEmail = Input.Email,
                        Subject = "Password Reset",
                        Body = $"Click here to reset your password: {url}"
                    };

                    await this.mailService.SendEmailAsync(message);

                    return Redirect(returnUrl);
                }
                catch (Exception)
                {
                    ModelState.AddModelError(string.Empty, "User is not registered!");
                    return Page();
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Invalid reset password attempt.");
                return Page();
            }
        }
    }
}
