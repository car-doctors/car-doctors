using CarDoctors.Data.Helpers;
using CarDoctors.Data.Models;
using CarDoctors.Data.Models.Mail;
using CarDoctors.Services.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace CarDoctors.MVC.Areas.Identity.Pages
{
    public class RegisterModel : PageModel
    {
        private readonly IAccountServices accountServices;
        private readonly IMailService mailService;

        public RegisterModel(IAccountServices accountServices, IMailService mailService)
        {
            this.accountServices = accountServices;
            this.mailService = mailService;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }


            [Required]
            [DataType(DataType.Text)]
            [StringLength(20, MinimumLength = 2, ErrorMessage = "First name must be between 8 and 20 characters long.")]
            [Display(Name = "First Name")]
            public string FirstName { get; set; }

            [Required]
            [DataType(DataType.Text)]
            [StringLength(20, MinimumLength = 2, ErrorMessage = "Last name must be between 8 and 20 characters long.")]
            [Display(Name = "Last Name")]
            public string LastName { get; set; }

            [Required]
            [DataType(DataType.PhoneNumber)]
            [StringLength(10, MinimumLength = 10, ErrorMessage = "The phone number should be 10 characters long.")]
            [Display(Name = "Phone number")]
            public string PhoneNumber { get; set; }
        }
        public void OnGet()
        {
        }
        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/Admin");
            if (ModelState.IsValid)
            {
                var guid = Guid.NewGuid();

                var password = PasswordHasher.HashPassword(guid.ToString());

                var user = new User { FirstName = Input.FirstName, LastName = Input.LastName, Phone = Input.PhoneNumber, Username = Input.Email, Email = Input.Email, PasswordHash = password };

                var createdUser = await this.accountServices.Register(user, guid.ToString());

                await this.mailService.SendEmailAsync(new MailRequest() { ToEmail = $"{user.Email}", Subject = "Account credentials", Body = $" Your username is: {user.Email} and your password is: {guid.ToString()}" });

                return LocalRedirect(returnUrl);
            }

            return Page();
        }
    }
}
