using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace CarDoctors.MVC.Areas.Identity.Pages
{
    public class LogoutModel : PageModel
    {
        public LogoutModel()
        {

        }
        public IActionResult OnGet(string returnUrl = null)
        {
            HttpContext.Session.Remove("Authorization");
            HttpContext.Session.Remove("Username");

            if (returnUrl != null)
            {
                return LocalRedirect(returnUrl);
            }
            else
            {
                return RedirectToPage();
            }
        }

        public IActionResult OnPost(string returnUrl = null)
        {
            HttpContext.Session.Remove("Authorization");
            HttpContext.Session.Remove("Username");

            if (returnUrl != null)
            {
                return LocalRedirect(returnUrl);
            }
            else
            {
                return RedirectToPage();
            }
        }
    }
}
