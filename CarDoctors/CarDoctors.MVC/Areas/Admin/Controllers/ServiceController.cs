﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.MVC.Models;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.Services.Services.Contracts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarDoctors.MVC.Helper;

namespace CarDoctors.MVC.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ServiceController : Controller
    {
        private readonly IServiceServices serviceServices;
        private readonly IMapper mapper;
        private readonly CarDoctorsContext carDoctorsContest;

        public ServiceController(CarDoctorsContext carDoctorsContest, IServiceServices serviceServices, IMapper mapper)
        {
            this.serviceServices = serviceServices;
            this.mapper = mapper;
            this.carDoctorsContest = carDoctorsContest;
        }

        [AllowAnonymous]
        public async Task<IActionResult> IndexAsync(PaginatedListGetModel<ServiceViewModel> inputModel, int pageNumber = 1, int PageSize = 6)
        {
            try
            {
                var filter = new FilterServices()
                {
                    Name = inputModel.Name,
                    PriceIs = inputModel.PriceIs,
                    PriceFrom = inputModel.PriceFrom,
                    PriceTo = inputModel.PriceTo,
                    Currency = inputModel.Currency
                };

                var optionsModel = new PagingOptionsModelService()
                {
                    Page = pageNumber,
                    PageSize = PageSize,
                    SortBy = inputModel.SortBy,
                    SortInDescendingOrder = inputModel.SortInDescendingOrder
                };

                var booleans = new List<SelectListItem>()
                {
                    new SelectListItem()
                    {
                        Text = "True",
                        Value = "true"
                    },
                    new SelectListItem()
                    {
                        Text = "False",
                        Value = "False"
                    }
                };

                ViewData["Sort"] = booleans;

                var services = await this.serviceServices.GetPaginatedAsync(filter, optionsModel);

                inputModel = mapper.Map<PaginatedListGetModel<ServiceViewModel>>(services);

                return View(inputModel);
            }
            catch (InvalidCurrencyException e)
            {
                ModelState.AddModelError(string.Empty, e.Message);

                var services = await this.serviceServices.GetPaginatedAsync(new FilterServices(), new PagingOptionsModelService());

                inputModel = mapper.Map<PaginatedListGetModel<ServiceViewModel>>(services);

                return View(inputModel);
            }
            catch (Exception e)
            {
                ModelState.AddModelError(string.Empty, e.Message);

                var services = await this.serviceServices.GetPaginatedAsync(new FilterServices(), new PagingOptionsModelService());

                inputModel = mapper.Map<PaginatedListGetModel<ServiceViewModel>>(services);

                return View(inputModel);
            }
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public async Task<IActionResult> CreateAsync(ServiceInputModel service)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await this.serviceServices.Create(service);

                    var services = await this.serviceServices.GetAll(new FilterServices(), "BGN");

                    var json = Json(new { isValid = true, html = Helper.Helper.RenderRazorViewToString(this, "_ViewAll", services) });

                    return json;
                }
                catch (ObjectDuplicateException e)
                {
                    ModelState.AddModelError(string.Empty, e.Message);

                    return Json(new { isValid = false, html = Helper.Helper.RenderRazorViewToString(this, "Create", service) });
                }
                catch (ObjectNullException e)
                {
                    ModelState.AddModelError(string.Empty, e.Message);

                    return Json(new { isValid = false, html = Helper.Helper.RenderRazorViewToString(this, "Create", service) });
                }
                catch (NegativePriceException e)
                {
                    ModelState.AddModelError(string.Empty, e.Message);

                    return Json(new { isValid = false, html = Helper.Helper.RenderRazorViewToString(this, "Create", service) });
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(string.Empty, e.Message);

                    return Json(new { isValid = false, html = Helper.Helper.RenderRazorViewToString(this, "Create", service) });
                }
            }

            return Json(new { isValid = false, html = Helper.Helper.RenderRazorViewToString(this, "Create", service) });
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public async Task<IActionResult> UpdateAsync(int id)
        {
            var service = await this.carDoctorsContest.Services.Where(s => s.ServiceID == id).FirstOrDefaultAsync();

            var inputModel = new ServiceInputModel()
            {
                ServiceID = service.ServiceID,
                Name = service.ServiceName,
                Price = service.Price,
                Currency = service.Currency
            };

            return View(inputModel);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public async Task<IActionResult> UpdateAsync(ServiceInputModel input, int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var service = await this.carDoctorsContest.Services.Where(s => s.ServiceID == id).FirstOrDefaultAsync();

                    await this.serviceServices.Update(input, service.ServiceID);

                    var services = await this.serviceServices.GetAll(new FilterServices(), "BGN");

                    var json = Json(new { isValid = true, html = Helper.Helper.RenderRazorViewToString(this, "_ViewAll", services) });

                    return json;
                }
                catch (ObjectNotFoundException e)
                {
                    ModelState.AddModelError(string.Empty, e.Message);

                    return Json(new { isValid = false, html = Helper.Helper.RenderRazorViewToString(this, "Update", input) });
                }
                catch (ObjectNullException e)
                {
                    ModelState.AddModelError(string.Empty, e.Message);

                    return Json(new { isValid = false, html = Helper.Helper.RenderRazorViewToString(this, "Update", input) });
                }
                catch (NegativePriceException e)
                {
                    ModelState.AddModelError(string.Empty, e.Message);

                    return Json(new { isValid = false, html = Helper.Helper.RenderRazorViewToString(this, "Update", input) });
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(string.Empty, e.Message);

                    return Json(new { isValid = false, html = Helper.Helper.RenderRazorViewToString(this, "Update", input) });
                }
            }

            return Json(new { isValid = false, html = Helper.Helper.RenderRazorViewToString(this, "Update", input) });
        }


        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public async Task<IActionResult> Delete(string name)
        {
            try
            {
                var service = await this.carDoctorsContest.Services.FirstOrDefaultAsync(s => s.ServiceName == name);

                this.serviceServices.Delete(service.ServiceID);

                var serviesToReturn = await this.serviceServices.GetAll(new FilterServices(), "BGN");

                return Json(new { html = Helper.Helper.RenderRazorViewToString(this, "_ViewAll", serviesToReturn) });
            }
            catch (ObjectNotFoundException e)
            {
                ModelState.AddModelError(string.Empty, e.Message);

                return Json(new { isValid = false, html = Helper.Helper.RenderRazorViewToString(this, "Delete", name) });
            }
            catch (Exception e)
            {
                ModelState.AddModelError(string.Empty, e.Message);

                return Json(new { isValid = false, html = Helper.Helper.RenderRazorViewToString(this, "Delete", name) });
            }
        }
    }
}
