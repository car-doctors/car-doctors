﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.MVC.Models;
using CarDoctors.Services.Mappers;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.Services.Services.Contracts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core.Exceptions;
using System.Threading.Tasks;

namespace CarDoctors.MVC.Areas.Admin.Controllers
{
    [Area("Admin")]

    public class VisitController : Controller
    {
        private readonly IVisitServices visitServices;
        private readonly CarDoctorsContext carDoctorsContext;
        private readonly IMapper mapper;

        public VisitController(IVisitServices visitServices, CarDoctorsContext carDoctorsContext, IMapper mapper)
        {
            this.visitServices = visitServices;
            this.carDoctorsContext = carDoctorsContext;
            this.mapper = mapper;
        }

        public async Task<IActionResult> IndexAsync(PaginatedListGetModel<VisitViewModel> inputModel, int pageNumber = 1, int PageSize = 6)
        {
            try
            {
                var vehicles = this.carDoctorsContext.Vehicles.Select(x => new VehicleViewDataModel()
                {
                    VehicleID = x.VehicleID,
                    Vehicle = $"{x.VehicleModel.Manufacturer.Name} {x.VehicleModel.Model} {x.RegistrationPlate}"
                });

                ViewData["VehicleID"] = new SelectList(vehicles, "VehicleID", "Vehicle");

                var booleans = new List<SelectListItem>()
                {
                    new SelectListItem()
                    {
                        Text = "True",
                        Value = "true"
                    },
                    new SelectListItem()
                    {
                        Text = "False",
                        Value = "False"
                    }
                };

                ViewData["Sort"] = booleans;

                var filter = new FilterVisits()
                {
                    VehicleID = inputModel.VehicleID,
                    DateFrom = inputModel.DateFrom,
                    DateTo = inputModel.DateTo
                };

                var optionsModel = new PagingOptionsModelVisit()
                {
                    Page = pageNumber,
                    PageSize = PageSize,
                    SortBy = inputModel.SortBy,
                    SortInDescendingOrder = inputModel.SortInDescendingOrder
                };

                var visits = await this.visitServices.GetPaginatedAsync(optionsModel, filter);

                inputModel = mapper.Map<PaginatedListGetModel<VisitViewModel>>(visits);

                return View(inputModel);
            }
            catch (ParseException)
            {
                ModelState.AddModelError(string.Empty, "Invalid Sort By criteria");

                return View(inputModel);
            }
            catch (Exception e)
            {

                ModelState.AddModelError(string.Empty, e.Message);

                return View(inputModel);
            }
        }

        public async Task<IActionResult> Details(int id = 0)
        {
            if (id == 0)
            {
                return View();
            }

            var visit = await this.visitServices.Get(id);

            return View(visit);
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public IActionResult Create()
        {
            var vehicles = this.carDoctorsContext.Vehicles.Select(x => new VehicleViewDataModel()
            {
                VehicleID = x.VehicleID,
                Vehicle = $"{x.VehicleModel.Manufacturer.Name} {x.VehicleModel.Model} {x.RegistrationPlate}"
            });

            ViewData["VehicleID"] = new SelectList(vehicles, "VehicleID", "Vehicle");

            ViewData["VisitStatusID"] = new SelectList(this.carDoctorsContext.VisitStatuses, "StatusID", "Name");

            ViewData["ServiceID"] = new SelectList(this.carDoctorsContext.Services, "ServiceID", "ServiceName");

            return View();
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public async Task<IActionResult> Create(VisitInputModel visitInputModel)
        {
            if (ModelState.IsValid)
            {
                visitInputModel.VisitDate = DateTime.Now;

                await this.visitServices.Create(visitInputModel);

                var optionsModel = new PagingOptionsModelVisit();

                var visits = await this.visitServices.GetPaginatedAsync(optionsModel, new FilterVisits());

                var json = Json(new { isValid = true, html = Helper.Helper.RenderRazorViewToString(this, "Index", visits) }); ;

                return json;
            }

            return Json(new { isValid = false, html = Helper.Helper.RenderRazorViewToString(this, "Create", visitInputModel) });
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public async Task<IActionResult> Update(int id)
        {
            var vehicles = this.carDoctorsContext.Vehicles.Select(x => new VehicleViewDataModel()
            {
                VehicleID = x.VehicleID,
                Vehicle = $"{x.VehicleModel.Manufacturer.Name} {x.VehicleModel.Model} {x.RegistrationPlate}"
            });

            var visit = await this.visitServices.Get(id);

            var serviceIds = visit.ServicesDone.Split(", ").ToList();

            var visitStatus = await this.carDoctorsContext.VisitStatuses.FirstOrDefaultAsync(x => x.Name == visit.VisitStatus);

            var services = string.Join(" ", this.carDoctorsContext.Services.Where(s => serviceIds.Contains(s.ServiceName)).Select(x => x.ServiceID).ToList());

            var visitInput = new VisitInputModel()
            {
                VehicleID = visit.VehicleID,
                VisitDate = visit.VisitDate,
                PickupDate = visit.PickUpDate,
                VisitStatusID = visitStatus.StatusID,
                ServiceID = services
            };

            ViewData["VehicleID"] = new SelectList(vehicles, "VehicleID", "Vehicle");

            ViewData["VisitStatusID"] = new SelectList(this.carDoctorsContext.VisitStatuses, "StatusID", "Name");

            var servicesViewData = this.carDoctorsContext.Services
                .MapServiceDTO()
              .Select(a => new ServiceViewDataModel()
              {
                  ServiceID = a.ServiceID,
                  Service = a.ServiceName
              });

            ViewData["ServiceID"] = new SelectList(servicesViewData, "ServiceID", "Service");

            return View(visitInput);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public async Task<IActionResult> Update([Bind("VehicleID,PickupDate,VisitStatusID,ServiceID")] VisitInputModel visitInputModel, int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await this.visitServices.Update(visitInputModel, id);

                    var visits = this.visitServices.GetAll();

                    var json = Json(new { isValid = true, html = Helper.Helper.RenderRazorViewToString(this, "_ViewAll", visits) });

                    return json;
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(null, e.Message);

                    return View();
                }
            }

            return Json(new { isValid = false, html = Helper.Helper.RenderRazorViewToString(this, "Create", visitInputModel) });
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public IActionResult Delete(int id)
        {
            this.visitServices.Delete(id);

            var visitsToReturn = this.visitServices.GetAll();

            return Json(new { html = Helper.Helper.RenderRazorViewToString(this, "_ViewAll", visitsToReturn) });
        }
    }
}
