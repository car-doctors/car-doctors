﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.MVC.Models;
using CarDoctors.Services.Mappers;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.Services.Services.Contracts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarDoctors.MVC.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class VehicleController : Controller
    {
        private readonly IVehicleServices vehicleServices;
        private readonly IMapper mapper;
        private readonly CarDoctorsContext carDoctorsContext;

        public VehicleController(CarDoctorsContext carDoctorsContext, IVehicleServices vehicleServices, IMapper mapper)
        {
            this.vehicleServices = vehicleServices;
            this.mapper = mapper;
            this.carDoctorsContext = carDoctorsContext;
        }

        public async Task<IActionResult> Index(PaginatedListGetModel<VehicleViewModel> model, int id, int pageNumber = 1, int PageSize = 6)
        {
            try
            {
                var optionsModel = new PagingOptionsModelVehicle()
                {
                    Page = pageNumber,
                    PageSize = PageSize,
                    SortBy = model.SortBy,
                    SortInDescendingOrder = model.SortInDescendingOrder
                };

                var booleans = new List<SelectListItem>()
                {
                    new SelectListItem()
                    {
                        Text = "True",
                        Value = "true"
                    },
                    new SelectListItem()
                    {
                        Text = "False",
                        Value = "False"
                    }
                };

                ViewData["Sort"] = booleans;

                var vehicles = await this.vehicleServices.GetPaginatedAsync(optionsModel, id);

                model = mapper.Map<PaginatedListGetModel<VehicleViewModel>>(vehicles);

                return View(model);
            }
            catch (Exception e)
            {

                ModelState.AddModelError(string.Empty, e.Message);

                var vehicles = await this.vehicleServices
                   .GetPaginatedAsync(new PagingOptionsModelVehicle(), id);

                model = mapper.Map<PaginatedListGetModel<VehicleViewModel>>(vehicles);

                return View(model);
            }
        }

        public async Task<IActionResult> Details(int id = 0)
        {
            if (id == 0)
            {
                return View();
            }

            var vehicle = await this.vehicleServices.Get(id);

            return View(vehicle);
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public IActionResult Create()
        {
            var types = this.carDoctorsContext.VehicleTypes
              .Select(a => new VehicleTypeViewDataModel()
              {
                  VehicleTypeID = a.VehicleTypeID,
                  Type = a.Name
              });

            ViewData["VehicleTypeID"] = new SelectList(types, "VehicleTypeID", "Type");

            var owner = this.carDoctorsContext.Users
              .Select(a => new VehicleOwnerView()
              {
                  OwnerID = a.Id,
                  OwnerName = $"{a.FirstName} {a.LastName}"
              });

            ViewData["OwnerID"] = new SelectList(owner, "OwnerID", "OwnerName");

            return View();
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public async Task<IActionResult> CreateAsync(VehicleViewInputModel input, int pageSize)
        {
            if (ModelState.IsValid)
            {
                var vehicleType = await this.carDoctorsContext.VehicleTypes.FirstOrDefaultAsync(v => v.VehicleTypeID == input.VehicleTypeID);

                var owner = await this.carDoctorsContext.Users.FirstOrDefaultAsync(v => v.Id == input.OwnerID);

                var vehicle = new VehicleInputModel()
                {
                    VIN = input.VIN,
                    Model = input.Model,
                    Type = vehicleType.Name,
                    Manufacturer = input.Manufacturer,
                    OwnerFirstName = owner.FirstName,
                    OwnerLastName = owner.LastName,
                    RegistrationPlate = input.RegistrationPlate
                };

                await this.vehicleServices.Create(vehicle);

                var vehicles = this.vehicleServices.GetAll(input.OwnerID);

                var json = Json(new { isValid = true, html = Helper.Helper.RenderRazorViewToString(this, "_ViewAll", vehicles) });

                return json;
            }
            else
            {
                var types = this.carDoctorsContext.VehicleTypes
              .Select(a => new VehicleTypeViewDataModel()
              {
                  VehicleTypeID = a.VehicleTypeID,
                  Type = a.Name
              });

                ViewData["VehicleTypeID"] = new SelectList(types, "VehicleTypeID", "Type");

                var owner = this.carDoctorsContext.Users
                  .Select(a => new VehicleOwnerView()
                  {
                      OwnerID = a.Id,
                      OwnerName = $"{a.FirstName} {a.LastName}"
                  });

                ViewData["OwnerID"] = new SelectList(owner, "OwnerID", "OwnerName");

                return Json(new { isValid = false, html = Helper.Helper.RenderRazorViewToString(this, "Create", input) });
            }
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public async Task<IActionResult> UpdateAsync(int id)
        {
            var types = this.carDoctorsContext.VehicleTypes
              .Select(a => new VehicleTypeViewDataModel()
              {
                  VehicleTypeID = a.VehicleTypeID,
                  Type = a.Name
              });

            ViewData["VehicleTypeID"] = new SelectList(types, "Type", "Type");

            var vehicle = await this.vehicleServices.Get(id);

            var inputModel = new VehicleInputModel()
            {
                Manufacturer = vehicle.Manufacturer,
                Model = vehicle.Model,
                Type = vehicle.VehicleType,
                OwnerFirstName = vehicle.Owner.Split().First(),
                OwnerLastName = vehicle.Owner.Split().Last(),
                VIN = vehicle.VIN,
                RegistrationPlate = vehicle.RegistrationPlate,
                VehicleID = id
            };

            return View(inputModel);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public async Task<IActionResult> UpdateAsync(VehicleInputModel input, int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await this.vehicleServices.Update(input, id);

                    var vehicles = this.carDoctorsContext.Vehicles.MapVehicleDTO();

                    var json = Json(new { isValid = true, html = Helper.Helper.RenderRazorViewToString(this, "_ViewAll", vehicles) });

                    return json;
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(string.Empty, e.Message);

                    var types = this.carDoctorsContext.VehicleTypes
                      .Select(a => new VehicleTypeViewDataModel()
                      {
                          VehicleTypeID = a.VehicleTypeID,
                          Type = a.Name
                      });

                    ViewData["VehicleTypeID"] = new SelectList(types, "Type", "Type");

                    return Json(new { isValid = false, html = Helper.Helper.RenderRazorViewToString(this, "Update", input) });
                }
            }
            else
            {
                var types = this.carDoctorsContext.VehicleTypes
                      .Select(a => new VehicleTypeViewDataModel()
                      {
                          VehicleTypeID = a.VehicleTypeID,
                          Type = a.Name
                      });

                return Json(new { isValid = false, html = Helper.Helper.RenderRazorViewToString(this, "Update", input) });
            }
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public IActionResult Delete(int id)
        {
            this.vehicleServices.Delete(id);

            var vehiclesToReturn = this.vehicleServices.GetAll(0);

            return Json(new { html = Helper.Helper.RenderRazorViewToString(this, "_ViewAll", vehiclesToReturn) });
        }
    }
}
