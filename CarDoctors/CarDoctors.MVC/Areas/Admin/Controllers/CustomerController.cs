﻿using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Data.Models;
using CarDoctors.Services.Mappers;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Models.OutputModels;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.MVC.Helper;
using CarDoctors.Services.Services.Contracts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarDoctors.MVC.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CustomerController : Controller
    {
        private readonly IAccountServices accountServices;
        private readonly CarDoctorsContext carDoctorsContext;
        private readonly IUserService userServices;

        public CustomerController(IAccountServices accountServices, CarDoctorsContext carDoctorsContext, IUserService userServices)
        {
            this.accountServices = accountServices;
            this.carDoctorsContext = carDoctorsContext;
            this.userServices = userServices;
        }

        public async Task<IActionResult> IndexAsync(PaginatedListGetModel<UserDTO> inputModel, int pageNumber = 1, int PageSize = 6)
        {
            if (inputModel.SortBy == "Price")
            {
                inputModel.SortBy = "FirstName";
            }

            var optionsModel = new PagingOptionsModelUser()
            {
                Page = pageNumber,
                PageSize = PageSize,
                SortBy = inputModel.SortBy,
                SortInDescendingOrder = inputModel.SortInDescendingOrder
            };

            var filter = new FilterUser()
            {
                FirstName = inputModel.FirstName,
                LastName = inputModel.LastName,
                Email = inputModel.Email,
                Phone = inputModel.Phone,
                VehicleManufacturer = inputModel.VehicleManufacturer,
                VehicleModel = inputModel.VehicleModel,
                VehicleType = inputModel.VehicleType,
                VisitFrom = inputModel.VisitFrom,
                VisitTo = inputModel.VisitTo
            };

            var booleans = new List<SelectListItem>()
                {
                    new SelectListItem()
                    {
                        Text = "True",
                        Value = "true"
                    },
                    new SelectListItem()
                    {
                        Text = "False",
                        Value = "False"
                    }
                };

            ViewData["Sort"] = booleans;

            inputModel = await this.accountServices.GetPaginatedAsync(optionsModel, filter);

            return View(inputModel);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public async Task<IActionResult> Delete(int id)
        {
            var user = await this.carDoctorsContext.Users.FirstOrDefaultAsync(s => s.Id == id);

            await this.accountServices.DeleteAsync(user.Id);

            var usersToReturn = this.carDoctorsContext.Users.MapUserDTO();

            return Json(new { html = Helper.Helper.RenderRazorViewToString(this, "_ViewAll", usersToReturn) });
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public async Task<IActionResult> UpdateAsync(string email)
        {
            var user = await this.userServices.GetUserByEmailAsync(email);

            var inputModel = new UserViewInputModel()
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Phone = user.Phone
            };

            return View(inputModel);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        public async Task<IActionResult> UpdateAsync(UserViewInputModel input, string email)
        {
            if (ModelState.IsValid)
            {
                await this.accountServices.UpdateAsync(input, email);

                var admin = this.carDoctorsContext.Roles.FirstOrDefault();

                var users = this.carDoctorsContext.Users.Where(u => u.UserRoles.Contains(new UserRole() { RoleID = 1, Role = admin, UserID = u.Id, User = u }) == false).MapUserDTO();

                var json = Json(new { isValid = true, html = Helper.Helper.RenderRazorViewToString(this, "_ViewAll", users) });

                return json;
            }

            return Json(new { isValid = false, html = Helper.Helper.RenderRazorViewToString(this, "Update", input) });
        }
    }
}
