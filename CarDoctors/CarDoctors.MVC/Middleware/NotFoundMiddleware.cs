﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace CarDoctors.MVC.Middleware
{
    public class NotFoundMiddleware
    {
        private readonly RequestDelegate next;

        public NotFoundMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {

            await this.next.Invoke(httpContext);

            var request = httpContext.Request;

            if (httpContext.Response.StatusCode == 404 && (httpContext.User.IsInRole("Admin") || httpContext.User.IsInRole("Employee")))
            {
                httpContext.Response.Redirect("/admin/home/notfound");
            }
            else if (httpContext.Response.StatusCode == 404 && httpContext.User.IsInRole("Customer"))
            {
                httpContext.Response.Redirect("/customer/home/notfound");
            }
            else if (httpContext.Response.StatusCode == 404)
            {
                httpContext.Response.Redirect("/home/notfound");
            }
        }
    }
}
