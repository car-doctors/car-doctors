﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.MVC.Models;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Models.OutputModels;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.Services.Services.Contracts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarDoctors.MVC.Controllers
{
    public class ServiceController : Controller
    {
        private readonly IServiceServices serviceServices;
        private readonly IMapper mapper;
        private readonly CarDoctorsContext carDoctorsContest;

        public ServiceController(CarDoctorsContext carDoctorsContest, IServiceServices serviceServices, IMapper mapper)
        {
            this.serviceServices = serviceServices;
            this.mapper = mapper;
            this.carDoctorsContest = carDoctorsContest;
        }

        [AllowAnonymous]
        public async Task<IActionResult> IndexAsync(/*ServiceInputModelView inputModel,*/ PaginatedListGetModel<ServiceViewModel> inputModel, int pageNumber = 1, int PageSize = 6)
        {
            try
            {
                var filter = new FilterServices()
                {
                    Name = inputModel.Name,
                    PriceIs = inputModel.PriceIs,
                    PriceFrom = inputModel.PriceFrom,
                    PriceTo = inputModel.PriceTo,
                    Currency = inputModel.Currency
                };

                var optionsModel = new PagingOptionsModelService()
                {
                    Page = pageNumber,
                    PageSize = PageSize,
                    SortBy = inputModel.SortBy,
                    SortInDescendingOrder = inputModel.SortInDescendingOrder
                };

                var booleans = new List<SelectListItem>()
                {
                    new SelectListItem()
                    {
                        Text = "True",
                        Value = "true"
                    },
                    new SelectListItem()
                    {
                        Text = "False",
                        Value = "False"
                    }
                };

                ViewData["Sort"] = booleans;

                var services = await this.serviceServices.GetPaginatedAsync(filter, optionsModel);

                inputModel = mapper.Map<PaginatedListGetModel<ServiceViewModel>>(services);

                return View(inputModel);
            }
            catch (InvalidCurrencyException e)
            {
                ModelState.AddModelError(string.Empty, e.Message);

                var services = await this.serviceServices.GetPaginatedAsync(new FilterServices(), new PagingOptionsModelService());

                inputModel = mapper.Map<PaginatedListGetModel<ServiceViewModel>>(services);

                return View(inputModel);
            }
            catch (Exception e)
            {
                ModelState.AddModelError(string.Empty, e.Message);

                var services = await this.serviceServices.GetPaginatedAsync(new FilterServices(), new PagingOptionsModelService());

                inputModel = mapper.Map<PaginatedListGetModel<ServiceViewModel>>(services);

                return View(inputModel);
            }
        }

        public IActionResult ClearFilter()
        {
            HttpContext.Session.Remove("SortBy");
            HttpContext.Session.Remove("Currency");
            HttpContext.Session.Remove("Name");
            HttpContext.Session.Remove("PriceFrom");
            HttpContext.Session.Remove("PriceTo");
            HttpContext.Session.Remove("PriceIs");

            return RedirectToAction("Index");
        }

        private void SetupFilter(PaginatedListGetModel<ServiceDTO> inputModel)
        {
            if (inputModel.SortBy == null)
            {
                inputModel.SortBy = this.HttpContext.Session.GetString("SortBy");
            }
            else
            {
                this.HttpContext.Session.SetString("SortBy", inputModel.SortBy);
            }

            if (inputModel.Currency == null)
            {
                inputModel.Currency = this.HttpContext.Session.GetString("Currency");
            }
            else
            {
                this.HttpContext.Session.SetString("Currency", inputModel.Currency);
            }

            if (inputModel.Name == null)
            {
                inputModel.Name = this.HttpContext.Session.GetString("Name");
            }
            else
            {
                this.HttpContext.Session.SetString("Name", inputModel.Name);
            }

            if (inputModel.PriceFrom.ToString() == null)
            {
                inputModel.PriceFrom = Double.Parse(this.HttpContext.Session.GetString("PriceFrom"));
            }
            else
            {
                this.HttpContext.Session.SetString("PriceFrom", inputModel.PriceFrom.ToString());
            }

            if (inputModel.PriceTo.ToString() == null)
            {
                inputModel.PriceTo = Double.Parse(this.HttpContext.Session.GetString("PriceTo"));
            }
            else
            {
                this.HttpContext.Session.SetString("PriceTo", inputModel.PriceTo.ToString());
            }

            if (inputModel.PriceIs.ToString() == null)
            {
                inputModel.PriceIs = Double.Parse(this.HttpContext.Session.GetString("PriceIs"));
            }
            else
            {
                this.HttpContext.Session.SetString("PriceIs", inputModel.PriceIs.ToString());
            }
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Customer")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Customer")]
        public async Task<IActionResult> CreateAsync(ServiceInputModel service)
        {
            if (ModelState.IsValid)
            {

                await this.serviceServices.Create(service);

                return RedirectToAction(nameof(Index));
            }

            return View(service);
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Customer")]
        public IActionResult Update()
        {
            var services = this.carDoctorsContest.Services.Select(a => new ServiceViewDataModel()
            {
                ServiceID = a.ServiceID,
                Service = $"{a.ServiceName} {a.Price} {a.Currency}"
            });

            ViewData["ServiceID"] = new SelectList(services, "ServiceID", "Service");

            return View();
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Customer")]
        public async Task<IActionResult> UpdateAsync(ServiceInputModelView input)
        {
            if (ModelState.IsValid)
            {
                var service = new ServiceInputModel()
                {
                    Name = input.Name,
                    Price = input.Price,
                    Currency = input.Currency
                };

                await this.serviceServices.Update(service, input.ServiceID);
                return RedirectToAction(nameof(Index));
            }

            return View(input);
        }
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Customer")]
        [HttpGet]
        public IActionResult Delete()
        {
            return View();
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Customer")]
        public IActionResult Delete(int id)
        {
            this.serviceServices.Delete(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
