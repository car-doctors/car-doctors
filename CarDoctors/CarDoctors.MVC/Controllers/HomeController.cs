﻿using CarDoctors.MVC.Models;
using CarDoctors.Services.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;

namespace CarDoctors.MVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IServiceServices serviceServices;

        public HomeController(ILogger<HomeController> logger, IServiceServices serviceServices)
        {
            _logger = logger;
            this.serviceServices = serviceServices;
        }

        public async System.Threading.Tasks.Task<IActionResult> IndexAsync()
        {
            //ViewData["Services"] = await this.serviceServices.GetAll(new FilterServices(), "BGN");
            return View();
        }

        public IActionResult AboutUs()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult NotFound()
        {
            return View();
        }

        public IActionResult RequestPasswordResetSuccess()
        {
            return View();
        }
    }
}
