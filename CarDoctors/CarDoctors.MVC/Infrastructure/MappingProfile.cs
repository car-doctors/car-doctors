﻿using AutoMapper;
using CarDoctors.Data.Models;
using CarDoctors.MVC.Models;
using CarDoctors.Services.Models.OutputModels;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.Services.Paging;

namespace CarDoctors.MVC.Infrastructure
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            //PagingOptions
            CreateMap<PagingOptionsModelVehicle, PagingOptions>()
                    .ForMember(p => p.PageNumber, opt => opt.MapFrom(m => m.Page))
                    .ForMember(target => target.SortByPropertyName, source => source.MapFrom(dto => dto.SortBy));
            CreateMap<PaginatedList<VehicleDTO>, PaginatedListGetModel<VehicleDTO>>()
                .ForMember(p => p.NumOfTotalPages, opt => opt.MapFrom(po => po.TotalPages));

            CreateMap<PagingOptionsModelService, PagingOptions>()
                    .ForMember(p => p.PageNumber, opt => opt.MapFrom(m => m.Page))
                    .ForMember(target => target.SortByPropertyName, source => source.MapFrom(dto => dto.SortBy));
            CreateMap<PaginatedList<ServiceDTO>, PaginatedListGetModel<ServiceDTO>>()
                .ForMember(p => p.NumOfTotalPages, opt => opt.MapFrom(po => po.TotalPages));

            CreateMap<PagingOptionsModelVisit, PagingOptions>()
                    .ForMember(p => p.PageNumber, opt => opt.MapFrom(m => m.Page))
                    .ForMember(target => target.SortByPropertyName, source => source.MapFrom(dto => dto.SortBy));
            CreateMap<PaginatedList<VisitDTO>, PaginatedListGetModel<VisitDTO>>()
                .ForMember(p => p.NumOfTotalPages, opt => opt.MapFrom(po => po.TotalPages));

            CreateMap<PagingOptionsModelUser, PagingOptions>()
                    .ForMember(p => p.PageNumber, opt => opt.MapFrom(m => m.Page))
                    .ForMember(target => target.SortByPropertyName, source => source.MapFrom(dto => dto.SortBy));
            CreateMap<PaginatedList<UserDTO>, PaginatedListGetModel<UserDTO>>()
                    .ForMember(p => p.NumOfTotalPages, opt => opt.MapFrom(po => po.TotalPages));

            CreateMap<PaginatedListGetModel<ServiceDTO>, PaginatedListGetModel<ServiceViewModel>>();

            CreateMap<ServiceDTO, ServiceViewModel>();

            CreateMap<PaginatedListGetModel<VisitDTO>, PaginatedListGetModel<VisitViewModel>>();

            CreateMap<VisitDTO, VisitViewModel>();

            CreateMap<PaginatedListGetModel<VehicleDTO>, PaginatedListGetModel<VehicleViewModel>>();

            CreateMap<VehicleDTO, VehicleViewModel>();

            CreateMap<Service, ServiceDTO>();

            CreateMap<User, UserDTO>();
        }
    }
}
