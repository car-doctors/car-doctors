﻿namespace CarDoctors.MVC.Models
{
    public class VehicleTypeViewDataModel
    {
        public int VehicleTypeID { get; set; }
        public string Type { get; set; }
    }
}
