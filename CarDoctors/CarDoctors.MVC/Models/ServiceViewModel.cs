﻿namespace CarDoctors.MVC.Models
{
    public class ServiceViewModel
    {
        public int ServiceID { get; set; }
        public string ServiceName { get; set; }
        public double Price { get; set; }
        public string Currency { get; set; }
    }
}
