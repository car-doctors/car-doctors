﻿namespace CarDoctors.MVC.Models
{
    public class ServiceViewDataModel
    {
        public int ServiceID { get; set; }
        public string Service { get; set; }
    }
}
