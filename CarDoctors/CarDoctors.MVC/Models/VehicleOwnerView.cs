﻿namespace CarDoctors.MVC.Models
{
    public class VehicleOwnerViewDataModel
    {
        public int OwnerID { get; set; }
        public string OwnerName { get; set; }
    }
}
