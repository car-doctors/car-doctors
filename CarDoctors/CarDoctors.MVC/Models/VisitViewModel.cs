﻿using System;

namespace CarDoctors.MVC.Models
{
    public class VisitViewModel
    {
        [System.Text.Json.Serialization.JsonIgnore]
        public int VisitID { get; set; }
        public string VIN { get; set; }
        public string ModelMake { get; set; }
        public string VehicleType { get; set; }
        [System.Text.Json.Serialization.JsonIgnore]
        public int VehicleID { get; set; }
        [System.Text.Json.Serialization.JsonIgnore]
        public int OwnerID { get; set; }
        public string Owner { get; set; }
        public string RegistrationPlate { get; set; }
        public string ServicesDone { get; set; }
        public double Price { get; set; }
        public string Currency { get; set; }
        public DateTime VisitDate { get; set; }
        public DateTime PickUpDate { get; set; }
        public string VisitStatus { get; set; }
    }
}
