﻿namespace CarDoctors.MVC.Models
{
    public class VehicleOwnerView
    {
        public int OwnerID { get; set; }
        public string OwnerName { get; set; }
    }
}
