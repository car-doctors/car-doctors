﻿using System.Text.Json.Serialization;

namespace CarDoctors.MVC.Models
{
    public class VehicleViewModel
    {
        [JsonIgnore]
        public int ID { get; set; }

        public string VIN { get; set; }

        public string Model { get; set; }

        public string Manufacturer { get; set; }

        public string VehicleType { get; set; }

        public string Owner { get; set; }

        public string RegistrationPlate { get; set; }

        public string Picture { get; set; }
    }
}
