﻿namespace CarDoctors.MVC.Models
{
    public class VehicleViewDataModel
    {
        public int VehicleID { get; set; }
        public string Vehicle { get; set; }
    }
}
