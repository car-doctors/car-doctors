﻿
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Models.OutputModels;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.Services.Services.Contracts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CarDoctors.MVC.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VisitController : ControllerBase
    {
        private readonly IVisitServices visitServices;
        private readonly CarDoctorsContext carDoctorsContext;

        public VisitController(IVisitServices visitServices, CarDoctorsContext carDoctorsContext)
        {
            this.visitServices = visitServices;
            this.carDoctorsContext = carDoctorsContext;
        }

        [HttpGet("")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Customer")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(VisitDTO))]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllAsync([FromQuery] PagingOptionsModelVisit options, [FromQuery] FilterVisits filter)
        {
            try
            {
                if (User.IsInRole("Admin"))
                {
                    var visits = await this.visitServices.GetPaginatedAsync(options, filter);

                    return Ok(visits);
                }
                else
                {
                    var http = HttpContext.User.Claims.ToList();

                    var email = http[0].Value;

                    var owner = this.carDoctorsContext.Users.FirstOrDefault(u => u.Email == email);

                    filter.Email = email;

                    var vehicles = await this.visitServices.GetPaginatedForCustomerAsync(options, filter);

                    return Ok(vehicles);
                }
            }
            catch (ValidationException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }

        [HttpGet("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Customer")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(VisitDTO))]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                if (User.IsInRole("Admin"))
                {
                    var visit = await this.visitServices.Get(id);

                    return Ok(visit);
                }
                else
                {
                    var http = HttpContext.User.Claims.ToList();

                    var email = http[0].Value;

                    var owner = this.carDoctorsContext.Users.FirstOrDefault(u => u.Email == email);

                    var visit = await this.visitServices.Get(id, owner.Id);

                    return Ok(visit);
                }
            }
            catch (ObjectNotFoundException о)
            {
                return NotFound(о.Message);
            }
            catch (ValidationException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }

        [HttpGet("visits")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Customer")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(VisitDTO))]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GenerateReportForMultipleVisits(string visits, string currency)
        {
            try
            {
                await this.visitServices.GeneratePDFForCollection(visits, currency);

                return Ok();
            }
            catch (InvalidCurrencyException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }

        [HttpPost("")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(VisitDTO))]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Post([FromBody] VisitInputModel visit)
        {
            try
            {
                var visitDTO = await this.visitServices.Create(visit);

                return Created("post", visitDTO);
            }
            catch (ValidationException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }

        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(VisitDTO))]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Put([FromBody] VisitInputModel visit, int id)
        {
            try
            {
                var visitDTO = await this.visitServices.Update(visit, id);

                return Created("post", visitDTO);
            }
            catch (ObjectNotFoundException о)
            {
                return NotFound(о.Message);
            }
            catch (ValidationException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }

        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Delete(int id)
        {
            try
            {
                this.visitServices.Delete(id);

                return NoContent();
            }
            catch (ObjectNotFoundException о)
            {
                return NotFound(о.Message);
            }
            catch (ValidationException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }
    }
}
