﻿
using CarDoctors.Data.Models;
using CarDoctors.Data.Models.Mail;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Models.AccountModels;
using CarDoctors.Services.Models.OutputModels;
using CarDoctors.Services.Services.Contracts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace CarDoctors.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IMailService mailService;
        private readonly IAccountServices accountServices;

        public AccountController(IMailService mailService, IAccountServices accountServices)
        {
            this.mailService = mailService;
            this.accountServices = accountServices;
        }

        [HttpPost("login")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UserDTO))]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var user = await accountServices.Login(model.Email, model.Password);

                var token = this.accountServices.GenerateJWTToken(user);

                return Ok(token);
            }
            catch (InvaludUsernameOrPasswordException о)
            {
                return NotFound(о.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }

        [HttpPost("requestPasswordReset")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Customer")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UserDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> RequestPasswordReset(string email)
        {
            try
            {
                var guid = this.accountServices.RequestPasswordReset(email);

                var url = this.Url.Link("Default", new { Controller = "Account", Action = "ResetPass", guid = guid, email = email });

                var message = new MailRequest()
                {
                    ToEmail = email,
                    Subject = "Password request",
                    Body = $"This is an email for resetting account password. Click here: {url} to reset your password.",
                };

                await this.mailService.SendEmailAsync(message);

                return Ok("Check your email.");
            }
            catch (InvalidPasswordResetRequestException о)
            {
                return BadRequest(о.Message);
            }
            catch (ObjectNotFoundException о)
            {
                return NotFound(о.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }

        [HttpPost("resetPass")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Customer")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UserDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ResetPass([FromQuery] string guid, [FromQuery] string email, [FromBody] string password)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                await this.accountServices.ResetPass(guid, email, password);

                return Ok("Password changed.");
            }
            catch (InvalidPasswordResetException о)
            {
                return BadRequest(о.Message);
            }
            catch (ObjectNotFoundException о)
            {
                return NotFound(о.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }


        [HttpPost("register")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UserDTO))]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            try
            {
                var user = new User()
                {
                    Username = model.Email,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Phone = model.Phone,
                    isDeleted = false
                };

                var password = Guid.NewGuid().ToString();

                var createdUser = await this.accountServices.Register(user, password);

                await this.mailService.SendEmailAsync(new MailRequest() { ToEmail = $"{model.Email}", Subject = "Account credentials", Body = $" Your username is: {user.Email} and your password is: {password}" });

                return Ok(createdUser);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }

        [HttpPost("resetPassword")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Customer")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UserDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ResetPassword([FromBody] ForgottenPassword account)
        {
            try
            {
                var newPass = await this.accountServices.ResetPassword(account.Email);

                await this.mailService.SendEmailAsync(new MailRequest() { ToEmail = $"{account.Email}", Subject = "Reset account password", Body = $"Your password has been changed. You can login with the following password: {newPass}" });

                return Ok("Check your email.");
            }
            catch (ObjectNotFoundException о)
            {
                return NotFound(о.Message);
            }
            catch (ValidationException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }

        [HttpPost("changePassword")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Customer")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UserDTO))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePassword account)
        {
            try
            {
                var user = await this.accountServices.ChangePassword(account.Email, account.OldPassword, account.NewPassword);

                return Ok(user);
            }
            catch (ObjectNotFoundException о)
            {
                return NotFound(о.Message);
            }
            catch (ValidationException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }

        [HttpDelete("delete/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await this.accountServices.DeleteAsync(id);

                return NoContent();
            }
            catch (ObjectNotFoundException о)
            {
                return NotFound(о.Message);
            }
            catch (ValidationException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }
    }
}
