﻿using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Models.OutputModels;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.Services.Services.Contracts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AuthorizeAttribute = Microsoft.AspNetCore.Authorization.AuthorizeAttribute;

namespace CarDoctors.MVC.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleController : Controller
    {
        private readonly IVehicleServices vehicleServices;
        private readonly CarDoctorsContext carDoctorsContext;

        public VehicleController(IVehicleServices vehicleServices, CarDoctorsContext carDoctorsContext)
        {
            this.vehicleServices = vehicleServices;
            this.carDoctorsContext = carDoctorsContext;
        }

        [HttpGet("id")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Customer")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(VehicleDTO))]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                if (User.IsInRole("Admin"))
                {
                    var vehicle = await this.vehicleServices.Get(id);

                    return Ok(vehicle);
                }
                else
                {
                    var http = HttpContext.User.Claims.ToList();

                    var email = http[0].Value;

                    var owner = this.carDoctorsContext.Users.FirstOrDefault(u => u.Email == email);

                    var vehicle = await this.vehicleServices.Get(id, owner.Id);

                    return Ok(vehicle);
                }
            }
            catch (ObjectNotFoundException о)
            {
                return NotFound(о.Message);
            }
            catch (ValidationException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }

        [HttpGet("")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin, Customer")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<VehicleDTO>))]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAsync([FromQuery] PagingOptionsModelVehicle optionsModel)
        {
            try
            {

                if (User.IsInRole("Admin"))
                {
                    var vehicles = await this.vehicleServices.GetPaginatedAsync(optionsModel, 0);

                    return Ok(vehicles);
                }
                else
                {
                    var http = HttpContext.User.Claims.ToList();

                    var email = http[0].Value;

                    var owner = this.carDoctorsContext.Users.FirstOrDefault(u => u.Email == email);

                    var vehicles = await this.vehicleServices.GetPaginatedAsync(optionsModel, owner.Id);

                    return Ok(vehicles);
                }
            }
            catch (ObjectNotFoundException о)
            {
                return NotFound(о.Message);
            }
            catch (ValidationException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }

        [HttpPost("")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(VehicleDTO))]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Create([FromBody] VehicleInputModel vehicle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model");
            }

            try
            {
                var vehicleCreated = await this.vehicleServices.Create(vehicle);

                return Ok(vehicleCreated);
            }
            catch (ValidationException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }

        [HttpPut("")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(VehicleDTO))]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Upadte([FromBody] VehicleInputModel vehicle, int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model");
            }

            try
            {
                var vehicleUpdated = await this.vehicleServices.Update(vehicle, id);

                return Ok(vehicleUpdated);
            }
            catch (ObjectNotFoundException о)
            {
                return NotFound(о.Message);
            }
            catch (ValidationException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }

        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Delete(int id)
        {
            try
            {
                this.vehicleServices.Delete(id);

                return NoContent();
            }
            catch (ObjectNotFoundException о)
            {
                return NotFound(о.Message);
            }
            catch (ValidationException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }
    }
}
