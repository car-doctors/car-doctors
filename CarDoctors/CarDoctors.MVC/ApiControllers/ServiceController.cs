﻿
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Models.OutputModels;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.Services.Services.Contracts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace CarDoctors.MVC.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        private readonly IServiceServices serviceServices;
        public ServiceController(IServiceServices serviceServices)
        {
            this.serviceServices = serviceServices;
        }

        [HttpGet("")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ServiceDTO))]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAll([FromQuery] ServicePaginatedModel inputModel)
        {
            try
            {
                var filter = new FilterServices() { Name = inputModel.Name, PriceIs = inputModel.PriceIs, PriceFrom = inputModel.PriceFrom, PriceTo = inputModel.PriceTo, Currency = inputModel.Currency };

                var optionsModel = new PagingOptionsModelService()
                {
                    Page = inputModel.Page,
                    PageSize = inputModel.PageSize,
                    SortBy = inputModel.SortBy,
                    SortInDescendingOrder = inputModel.SortInDescendingOrder
                };

                var services = await this.serviceServices.GetPaginatedAsync(filter, optionsModel);

                return Ok(services);
            }
            catch (ValidationException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ServiceDTO))]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var service = await this.serviceServices.Get(id);

                return Ok(service);
            }
            catch (ObjectNotFoundException о)
            {
                return NotFound(о.Message);
            }
            catch (ValidationException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }

        [HttpPost("")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(ServiceDTO))]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Post([FromBody] ServiceInputModel service)
        {
            try
            {
                var serviceDTO = await this.serviceServices.Create(service);

                return Created("post", serviceDTO);
            }
            catch (ValidationException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }

        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(ServiceDTO))]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Put([FromBody] ServiceInputModel service, int id)
        {
            try
            {
                var serviceDTO = await this.serviceServices.Update(service, id);

                return Created("post", serviceDTO);
            }
            catch (ObjectNotFoundException о)
            {
                return NotFound(о.Message);
            }
            catch (ValidationException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }

        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Delete(int id)
        {
            try
            {
                this.serviceServices.Delete(id);

                return NoContent();
            }
            catch (ObjectNotFoundException о)
            {
                return NotFound(о.Message);
            }
            catch (ValidationException e)
            {
                return this.BadRequest(e.Message);
            }
            catch (Exception)
            {
                return this.StatusCode(500, "Something went wrong");
            }
        }
    }
}
