﻿
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.OutputModels;
using CarDoctors.Services.Services.Contracts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;


namespace CarDoctors.MVC.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : Controller
    {
        private readonly IUserServices userServices;

        public CustomerController(IUserServices userServices)
        {
            this.userServices = userServices;
        }

        [HttpGet("Filtercustomers")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Admin")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UserDTO))]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult GetCustomerServices(string firstName, string lastName, string email, string phone, string vehicleModel, string vehicleManufacturer, string vehicleType, DateTime visitFrom, DateTime visitTo)
        {
            var filter = new FilterUser()
            {
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                Phone = phone,
                VehicleManufacturer = vehicleManufacturer,
                VehicleModel = vehicleModel,
                VehicleType = vehicleType,
                VisitFrom = visitFrom,
                VisitTo = visitTo
            };

            var users = this.userServices.FilterUsers(filter);

            return Ok(users);
        }
    }
}
