﻿namespace CarDoctors.Services.Resources
{
    public class Numbers
    {
        public const int MaxPageSize = 15;
        public const int DefaultPageSize = 6;
        public const int DefaultPage = 1;
    }
}
