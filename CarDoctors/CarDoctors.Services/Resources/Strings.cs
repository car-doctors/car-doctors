﻿namespace CarDoctors.Services.Resources
{
    public class Strings
    {
        public const string InvalidSortingProperty =
            "Invalid property for sorting:\nProperty {0} does not exist or can't be sorted.";

        public const string DefaultPagingOptionsVehicleSortByProperty = "VIN";
        public const string DefaultPagingOptionsServiceSortByProperty = "Price";
        public const string DefaultPagingOptionsVisitSortByProperty = "VisitDate";
        public const string DefaultPagingOptionsCustomerSortByProperty = "UserID";
    }
}
