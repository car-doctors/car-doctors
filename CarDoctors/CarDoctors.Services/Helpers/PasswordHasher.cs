﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Linq;
using System.Security.Cryptography;

namespace CarDoctors.Services.Helpers
{
    public static class PasswordHasher
    {
        public static string HashPassword(string password)
        {
            //Generate salt
            var salt = GetSalt();

            //Store hash and salt
            var hash = KeyDerivation.Pbkdf2(
                            password: password,
                            salt: salt,
                            prf: KeyDerivationPrf.HMACSHA256,
                            iterationCount: 10000,
                            numBytesRequested: 32);

            //Store hash and salt
            var hashedPassword = new byte[48];
            Buffer.BlockCopy(salt, 0, hashedPassword, 0, 16);
            Buffer.BlockCopy(hash, 0, hashedPassword, 16, 32);

            //Return hashed password
            return Convert.ToBase64String(hashedPassword);
        }

        public static bool VerifyPassword(string password, string hashedPassword)
        {
            //Get hashed password and convert it into bytes
            var hashedPasswordInBytes = Convert.FromBase64String(hashedPassword);

            //Extract salt from hashed password in bytes
            var salt = new byte[16];
            Buffer.BlockCopy(hashedPasswordInBytes, 0, salt, 0, 16);

            //Generate hash with the specified password and extracted salt
            var hash = KeyDerivation.Pbkdf2(
                            password: password,
                            salt: salt,
                            prf: KeyDerivationPrf.HMACSHA256,
                            iterationCount: 10000,
                            numBytesRequested: 32);

            //Compare new generated hash bytes with the old ones
            var hashBytes = new byte[32];
            Buffer.BlockCopy(hashedPasswordInBytes, 16, hashBytes, 0, 32);

            return hash.SequenceEqual<byte>(hashBytes);
        }

        public static byte[] GetSalt()
        {
            var rng = new RNGCryptoServiceProvider();
            var salt = new byte[16];

            rng.GetNonZeroBytes(salt);

            return salt;
        }
    }
}
