﻿using CarDoctors.Services.Paging;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;

namespace CarDoctors.Services.Helpers
{
    public static class PagingExtensions
    {
        public static async Task<PaginatedList<T>> ToPaginatedListAsync<T>(this IQueryable<T> source, int pageNumber, int pageSize)
        {
            var count = await source.CountAsync();
            var items = await source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();

            return new PaginatedList<T>(items, count, pageNumber, pageSize);
        }

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string propertyName, bool toDescending)
        {
            StringBuilder orderOpion = new StringBuilder(propertyName);
            orderOpion.Append(" ");
            orderOpion.Append(toDescending ? "descending" : "ascending");
            return source.OrderBy(orderOpion.ToString());
        }
    }
}
