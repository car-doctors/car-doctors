﻿namespace CarDoctors.Services.Paging.Contracts
{
    public interface IPagingOptions
    {
        int PageNumber { get; set; }
        int PageSize { get; set; }
    }
}
