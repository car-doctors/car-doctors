﻿using CarDoctors.Services.Paging.Contracts;
using CarDoctors.Services.Resources;

namespace CarDoctors.Services.Paging
{
    public class PagingOptions : IPagingOptions
    {
        private const int MaxPageSize = Numbers.MaxPageSize;
        private int _pageSize = Numbers.DefaultPageSize;

        public int PageNumber { get; set; } = Numbers.DefaultPage;

        public string SortByPropertyName { get; set; }

        public bool SortInDescendingOrder { get; set; } = false;

        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
        }
    }
}
