﻿using CarDoctors.Data.Models;
using CarDoctors.Services.Models.OutputModels;
using System;
using System.Linq;

namespace CarDoctors.Services.Mappers
{
    public static class MapServiceToDTO
    {
        public static IQueryable<ServiceDTO> MapServiceDTO(this IQueryable<Service> query)
        {
            return query.Select(s => new ServiceDTO()
            {
                ServiceID = s.ServiceID,
                ServiceName = s.ServiceName,
                Price = s.Price,
                Currency = s.Currency
            });
        }
        public static IQueryable<ServiceDTO> MapServiceDTO(this IQueryable<Service> query, double rate, string currency)
        {
            return query.Select(s => new ServiceDTO()
            {
                ServiceID = s.ServiceID,
                ServiceName = s.ServiceName,
                Price = Math.Round(s.Price * rate, 2),
                Currency = currency
            });
        }

        public static ServiceDTO MapServiceDTO(this Service service)
        {
            return new ServiceDTO()
            {
                ServiceID = service.ServiceID,
                ServiceName = service.ServiceName,
                Price = service.Price,
                Currency = service.Currency
            };
        }
    }
}
