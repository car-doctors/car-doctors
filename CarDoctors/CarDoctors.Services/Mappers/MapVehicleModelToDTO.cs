﻿using CarDoctors.Data.Models;
using CarDoctors.Services.Models.OutputModels;
using System.Linq;

namespace CarDoctors.Services.Mappers
{
    public static class MapVehicleModelToDTO
    {
        public static IQueryable<VehicleModelDTO> MapVehicleModelDTO(this IQueryable<VehicleModel> query)
        {
            return query.Select(x => new VehicleModelDTO()
            {
                Model = x.Model,
                Manufacturer = x.Manufacturer.Name,
                VehicleType = x.VehicleType.Name
            });
        }

        public static VehicleModelDTO MapVehicleModelDTO(this VehicleModel vehicle)
        {
            return new VehicleModelDTO()
            {
                Model = vehicle.Model,
                Manufacturer = vehicle.Manufacturer.Name,
                VehicleType = vehicle.VehicleType.Name
            };
        }
    }
}
