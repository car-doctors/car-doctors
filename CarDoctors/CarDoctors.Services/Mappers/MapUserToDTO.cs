﻿using CarDoctors.Data.Models;
using CarDoctors.Services.Models.OutputModels;
using System.Linq;

namespace CarDoctors.Services.Mappers
{
    public static class MapUserToDTO
    {
        public static IQueryable<UserDTO> MapUserDTO(this IQueryable<User> query)
        {
            return query.Select(x => new UserDTO()
            {
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email,
                Phone = x.Phone,
                UserID = x.Id
            });
        }

        public static UserDTO MapUserDTO(this User user)
        {
            return new UserDTO()
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Phone = user.Phone,
                UserID = user.Id
            };
        }
    }
}
