﻿using CarDoctors.Data.Models;
using CarDoctors.Services.Models.OutputModels;
using System.Linq;

namespace CarDoctors.Services.Mappers
{
    public static class MapVisitToDTO
    {
        public static IQueryable<VisitDTO> MapVisitDTO(this IQueryable<Visit> query)
        {
            return query.Select(x => new VisitDTO()
            {
                VisitID = x.VisitID,
                //ServicesDone = x.VisitServices.Select(v => v.Service.ServiceName),
                //ServicesDone =  x.VisitServices.Select(y => y.Service.MapServiceDTO()),
                VIN = x.Vehicle.VIN,
                ModelMake = $"{x.Vehicle.VehicleModel.Manufacturer.Name} {x.Vehicle.VehicleModel.Model}",
                VehicleType = x.Vehicle.VehicleModel.VehicleType.Name,
                Owner = $"{x.Vehicle.Owner.FirstName} {x.Vehicle.Owner.LastName}",
                RegistrationPlate = x.Vehicle.RegistrationPlate,
                VisitDate = x.VisitDate,
                PickUpDate = x.PickUpDate,
                VisitStatus = x.VisitStatus.Name,
                Price = x.VisitServices.Where(y => y.VisitID == x.VisitID).Select(y => y.Service.Price).Sum(),
                Currency = x.VisitServices.FirstOrDefault(y => y.VisitID == x.VisitID).Service.Currency,
                OwnerID = x.Vehicle.OwnerID,
                VehicleID = x.VehicleID
            });
        }

        public static VisitDTO MapVisitDTO(this Visit visit)
        {
            return new VisitDTO()
            {
                VisitID = visit.VisitID,
                //ServicesDone = visit.VisitServices.Select(v => v.Service.ServiceName),
                //ServicesDone = visit.VisitServices.Select(x => x.Service.MapServiceDTO()),
                VIN = visit.Vehicle.VIN,
                ModelMake = $"{visit.Vehicle.VehicleModel.Manufacturer.Name} {visit.Vehicle.VehicleModel.Model}",
                VehicleType = visit.Vehicle.VehicleModel.VehicleType.Name,
                Owner = $"{visit.Vehicle.Owner.FirstName} {visit.Vehicle.Owner.LastName}",
                RegistrationPlate = visit.Vehicle.RegistrationPlate,
                VisitDate = visit.VisitDate,
                PickUpDate = visit.PickUpDate,
                VisitStatus = visit.VisitStatus.Name,
                Price = visit.VisitServices.FirstOrDefault(x => x.VisitID == x.VisitID).Service.Price,
                Currency = visit.VisitServices.FirstOrDefault(x => x.VisitID == x.VisitID).Service.Currency,
                OwnerID = visit.Vehicle.OwnerID,
                VehicleID = visit.VehicleID
            };
        }
    }
}
