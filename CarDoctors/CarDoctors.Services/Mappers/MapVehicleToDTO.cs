﻿using CarDoctors.Data.Models;
using CarDoctors.Services.Models.OutputModels;
using System.Linq;

namespace CarDoctors.Services.Mappers
{
    public static class MapVehicleToDTO
    {
        public static IQueryable<VehicleDTO> MapVehicleDTO(this IQueryable<Vehicle> query)
        {
            return query.Select(x => new VehicleDTO()
            {
                ID = x.VehicleID,
                VIN = x.VIN,
                Model = x.VehicleModel.Model,
                Manufacturer = x.VehicleModel.Manufacturer.Name,
                VehicleType = x.VehicleModel.VehicleType.Name,
                Owner = $"{x.Owner.FirstName} {x.Owner.LastName}",
                RegistrationPlate = x.RegistrationPlate,
                Picture = x.VehicleModel.VehicleType.Picture
            });
        }

        public static VehicleDTO MapVehicleDTO(this Vehicle vehicle)
        {
            return new VehicleDTO()
            {
                VIN = vehicle.VIN,
                Model = vehicle.VehicleModel.Model,
                Manufacturer = vehicle.VehicleModel.Manufacturer.Name,
                VehicleType = vehicle.VehicleModel.VehicleType.Name,
                Owner = $"{vehicle.Owner.FirstName} {vehicle.Owner.LastName}",
                RegistrationPlate = vehicle.RegistrationPlate,
                ID = vehicle.VehicleID,
                Picture = vehicle.VehicleModel.VehicleType.Picture
            };
        }
    }
}
