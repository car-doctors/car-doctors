﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Data.Models;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Helpers;
using CarDoctors.Services.Mappers;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Models.OutputModels;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.Services.Paging;
using CarDoctors.Services.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace CarDoctors.Services.Services
{
    public class VehicleServices : IVehicleServices
    {
        private readonly CarDoctorsContext carDoctorsContext;
        private readonly IVehicleModelServices vehicleModelServices;
        private readonly IMapper _mapper;

        public VehicleServices(CarDoctorsContext carDoctorsContext, IVehicleModelServices vehicleModelServices, IMapper mapper)
        {
            this.carDoctorsContext = carDoctorsContext;
            this.vehicleModelServices = vehicleModelServices;
            this._mapper = mapper;
        }
        /// <summary>
        /// Gets all the vehicles from the database and returns them. The method returns the vehicles of specific owner if there is an owner id provided
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<VehicleDTO> GetAll(int? id)
        {
            if (id == null)
            {
                var vehicles = this.carDoctorsContext.Vehicles
                           .MapVehicleDTO();

                return vehicles;
            }
            else
            {
                var vehicles = this.carDoctorsContext.Vehicles
                        .Where(v => v.OwnerID == id)
                        .MapVehicleDTO();

                return vehicles;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vehicles"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public IQueryable<Vehicle> Filter(IQueryable<Vehicle> vehicles, int id)
        {
            if (id != 0)
            {
                vehicles = vehicles.Where(v => v.OwnerID == id);
            }

            return vehicles;
        }
        /// <summary>
        /// Returns a paginated list of vehicles, sorted and paginated by given criteria
        /// </summary>
        /// <param name="optionsModel"></param>
        /// <returns></returns>
        public async Task<IEnumerable<VehicleDTO>> GetAllAsync(PagingOptionsModelVehicle optionsModel)
        {
            var vehicles = await PaginatedList<VehicleDTO>
                .CreateAsync(this.carDoctorsContext.Vehicles
                .MapVehicleDTO(), optionsModel.Page, optionsModel.PageSize);

            return vehicles.Elements;
        }
        /// <summary>
        /// Returns a paginated list of vehicles, sorted, filtered and paginated by given criteria
        /// </summary>
        /// <param name="options"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<PaginatedListGetModel<VehicleDTO>> GetPaginatedAsync(PagingOptionsModelVehicle options, int id)
        {
            var pagingOptions = _mapper.Map<PagingOptions>(options);

            if (pagingOptions.SortByPropertyName == "Price")
            {
                pagingOptions.SortByPropertyName = "VIN";
            }

            var vehicles = Filter(this.carDoctorsContext.Vehicles, id);

            var query = vehicles.OrderBy(pagingOptions.SortByPropertyName, pagingOptions.SortInDescendingOrder).MapVehicleDTO();

            var result = await query.ToPaginatedListAsync(pagingOptions.PageNumber, pagingOptions.PageSize);

            var page = new PaginatedListGetModel<VehicleDTO>()
            {
                PageSize = result.PageSize,
                CurrentPage = result.CurrentPage,
                Elements = result.Elements,
                NumOfTotalPages = result.TotalPages,
                SortInDescendingOrder = options.SortInDescendingOrder,
                SortBy = options.SortBy
            };

            return page;
        }
        /// <summary>
        /// Gets a vehicle by its id from the database and returns it
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<VehicleDTO> Get(int id)
        {
            var vehicle = await carDoctorsContext.Vehicles
                .MapVehicleDTO().ToListAsync();


            return vehicle.FirstOrDefault(c => c.ID == id);
        }
        /// <summary>
        /// Gets a vehicle from the database by its id and the owner id
        /// </summary>
        /// <param name="vehicleID"></param>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public async Task<VehicleDTO> Get(int vehicleID, int customerID)
        {
            var vehicle = await carDoctorsContext.Vehicles
                .MapVehicleDTO()
                .FirstOrDefaultAsync(v => v.ID == vehicleID);

            if (vehicle == null)
            {
                throw new ObjectNotFoundException(String.Format(Strings.OBJECT_NOT_FOUND_ERROR, "Vehicle", vehicleID));
            }

            var owner = await this.carDoctorsContext.Users.FirstOrDefaultAsync(u => u.Id == customerID);

            if (!vehicle.Owner.Equals($"{owner.FirstName} {owner.LastName}"))
            {
                throw new UserUnauthorizedException(String.Format(Strings.UNAUTHORIZED_USER, $"{owner.FirstName} {owner.LastName}"));
            }

            return vehicle;
        }
        /// <summary>
        /// Creates a vehicle in the database from an input model containing the vehicle properties.
        /// </summary>
        /// <param name="vehicleInputModel"></param>
        /// <returns></returns>
        public async Task<VehicleDTO> Create(VehicleInputModel vehicleInputModel)
        {
            var dbVehicle = await this.carDoctorsContext.Vehicles
                .Include(v => v.Owner)
                .Include(v => v.VehicleModel)
                .ThenInclude(v => v.Manufacturer)
                .IgnoreQueryFilters()
                .FirstOrDefaultAsync(v => v.VIN == vehicleInputModel.VIN);

            if (dbVehicle != null)
            {
                if (dbVehicle.isDeleted == false)
                {
                    throw new ObjectDuplicateException(String.Format(Strings.NAME_DUPLICATE_ERROR, "Vehicle"));
                }

                dbVehicle.isDeleted = false;

                await this.carDoctorsContext.SaveChangesAsync();

                var restoredVehicle = await this.carDoctorsContext.Vehicles.MapVehicleDTO().FirstOrDefaultAsync(v => v.ID == dbVehicle.VehicleID);

                await this.carDoctorsContext.SaveChangesAsync();

                return restoredVehicle;
            }

            var vehicleModel = await carDoctorsContext.VehicleModels
                .FirstOrDefaultAsync(v => v.Manufacturer.Name == vehicleInputModel.Manufacturer && v.Model == vehicleInputModel.Model);

            if (vehicleModel == null)
            {
                await this.vehicleModelServices.Create(vehicleInputModel.Manufacturer, vehicleInputModel.Model, vehicleInputModel.Type);
            }

            vehicleModel = await carDoctorsContext.VehicleModels
                .Include(v => v.Manufacturer)
                .Include(v => v.VehicleType)
                .FirstOrDefaultAsync(v => v.Manufacturer.Name == vehicleInputModel.Manufacturer && v.Model == vehicleInputModel.Model);

            var owner = await carDoctorsContext.Users
                .FirstOrDefaultAsync(u => u.FirstName == vehicleInputModel.OwnerFirstName && u.LastName == vehicleInputModel.OwnerLastName);

            if (owner == null)
            {
                throw new ObjectNullException(String.Format(Strings.NULL_MESSAGE_ERROR, "Owner"));
            }

            var vehicle = new Vehicle()
            {
                VIN = vehicleInputModel.VIN,
                VehicleModelID = vehicleModel.VehicleModelID,
                OwnerID = owner.Id,
                RegistrationPlate = vehicleInputModel.RegistrationPlate
            };

            await this.carDoctorsContext.AddAsync(vehicle);

            await carDoctorsContext.SaveChangesAsync();

            return vehicle.MapVehicleDTO();
        }
        /// <summary>
        /// Updates a vehicle in the database by his id from an input model containing the updated vehicle properties.
        /// </summary>
        /// <param name="vehicleInputModel"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<VehicleDTO> Update(VehicleInputModel vehicleInputModel, int id)
        {
            var dbVehicle = await this.carDoctorsContext.Vehicles
                .FirstOrDefaultAsync(v => v.VehicleID == id);

            if (dbVehicle == null)
            {
                throw new ObjectNullException(String.Format(Strings.NULL_MESSAGE_ERROR, "Vehicle"));
            }

            var vehicleModel = await carDoctorsContext.VehicleModels
                .FirstOrDefaultAsync(v => v.Manufacturer.Name == vehicleInputModel.Manufacturer && v.Model == vehicleInputModel.Model);

            if (vehicleModel == null)
            {
                vehicleModel = await this.vehicleModelServices.Create(vehicleInputModel.Manufacturer, vehicleInputModel.Model, vehicleInputModel.Type);
            }

            var owner = await carDoctorsContext.Users
                .FirstOrDefaultAsync(u => u.FirstName == vehicleInputModel.OwnerFirstName && u.LastName == vehicleInputModel.OwnerLastName);

            if (owner == null)
            {
                throw new ObjectNullException(String.Format(Strings.NULL_MESSAGE_ERROR, "Owner"));
            }

            dbVehicle.VIN = vehicleInputModel.VIN;
            dbVehicle.VehicleModelID = vehicleModel.VehicleModelID;
            dbVehicle.OwnerID = owner.Id;
            dbVehicle.RegistrationPlate = vehicleInputModel.RegistrationPlate;

            await carDoctorsContext.SaveChangesAsync();

            var vehicle = await this.carDoctorsContext.Vehicles
                .MapVehicleDTO()
                .FirstOrDefaultAsync(v => v.ID == dbVehicle.VehicleID);

            return vehicle;
        }
        /// <summary>
        /// Deletes a vehicle from the database by its id
        /// </summary>
        /// <param name="id"></param>
        public void Delete(int id)
        {
            var vehicle = this.carDoctorsContext.Vehicles
                .FirstOrDefault(x => x.VehicleID == id)
                ?? throw new ObjectNotFoundException(String.Format(Strings.OBJECT_NOT_FOUND_ERROR, "Vehicle", id));

            this.carDoctorsContext.Remove(vehicle);

            carDoctorsContext.SaveChanges();
        }
    }
}
