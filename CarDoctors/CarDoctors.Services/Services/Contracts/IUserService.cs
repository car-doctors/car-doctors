﻿using CarDoctors.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarDoctors.Services.Services.Contracts
{
    public interface IUserService
    {
        //AuthenticateResponse Authenticate(AuthenticateRequest model);
        IEnumerable<User> GetAll();
        User GetById(int id);
        Task<User> GetUserByEmailAsync(string email);
        User GetUserByEmail(string email);
    }
}
