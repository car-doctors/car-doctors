﻿using CarDoctors.Data.Models;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Models.OutputModels;
using CarDoctors.Services.Models.PagingModels;
using System.Threading.Tasks;

namespace CarDoctors.Services.Services.Contracts
{
    public interface IAccountServices
    {
        Task<UserDTO> Login(string email, string password);
        string GenerateJWTToken(UserDTO user);
        Task<UserDTO> Register(User user, string password);
        Task<string> ResetPassword(string email);
        Task<UserDTO> ChangePassword(string email, string oldPassword, string newPassword);
        Task DeleteAsync(int id);
        Task ResetPass(string guid, string email, string password);
        Task<string> RequestPasswordReset(string email);
        Task<PaginatedListGetModel<UserDTO>> GetPaginatedAsync(PagingOptionsModelUser options, FilterUser filter);
        Task<UserDTO> UpdateAsync(UserViewInputModel userInputModel, string email);
    }
}
