﻿using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.OutputModels;
using System.Collections.Generic;

namespace CarDoctors.Services.Services.Contracts
{
    public interface IUserServices
    {
        //List<VisitDTO> GetCustomerServices(int id, FilterVisits filterVisits);
        //List<VisitDTO> Filter(List<VisitDTO> visits, FilterVisits filterVisits);
        List<UserDTO> FilterUsers(FilterUser filter);
    }
}
