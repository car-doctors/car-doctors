﻿using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Models.OutputModels;
using CarDoctors.Services.Models.PagingModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarDoctors.Services.Services.Contracts
{
    public interface IVisitServices
    {
        IEnumerable<VisitDTO> GetAll();
        Task<VisitDTO> Get(int id);
        Task<VisitDTO> Create(VisitInputModel visitInputModel);
        Task<VisitDTO> Update(VisitInputModel visitInputModel, int id);
        void Delete(int id);
        Task<PaginatedListGetModel<VisitDTO>> GetPaginatedAsync(PagingOptionsModelVisit options, FilterVisits filter);
        string GenerateReport(string info);

        Task<string> GeneratePDFForCollection(string visitIDs, string currency);

        Task<PaginatedListGetModel<VisitDTO>> GetPaginatedForCustomerAsync(PagingOptionsModelVisit options, FilterVisits filter);
        IQueryable<VisitDTO> FilterForCustomer(IQueryable<VisitDTO> visits, FilterVisits filterVisits);
        Task<double> ConvertCurrency(string currencyTo, string currencyFrom);
        Task<VisitDTO> Get(int visitID, int customerID);
    }
}
