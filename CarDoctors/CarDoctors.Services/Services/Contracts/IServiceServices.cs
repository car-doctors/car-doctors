﻿using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Models.OutputModels;
using CarDoctors.Services.Models.PagingModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarDoctors.Services.Services.Contracts
{
    public interface IServiceServices
    {
        Task<List<ServiceDTO>> GetAll(FilterServices filterServices, string currency);
        Task<PaginatedListGetModel<ServiceDTO>> GetPaginatedAsync(FilterServices filterServices, PagingOptionsModelService options);
        Task<ServiceDTO> ConvertCurrency(string currency, ServiceDTO service);
        Task<ServiceDTO> Get(int id);
        Task<ServiceDTO> Create(ServiceInputModel serviceInputModel);
        Task<ServiceDTO> Update(ServiceInputModel serviceInputModel, int id);
        void Delete(int id);
        void CheckModel(ServiceInputModel serviceInputModel);
    }
}
