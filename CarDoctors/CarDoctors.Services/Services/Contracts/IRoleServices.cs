﻿using CarDoctors.Data.Models;
using System.Threading.Tasks;

namespace CarDoctors.Services.Services.Contracts
{
    public interface IRoleServices
    {
        Task<Role> GetByNameAsync(string name);
    }
}
