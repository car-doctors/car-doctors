﻿using CarDoctors.Data.Models;
using CarDoctors.Services.Models.OutputModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarDoctors.Services.Services.Contracts
{
    public interface IVehicleModelServices
    {
        Task<VehicleModel> Create(string manufacturer, string model, string type);
        Task<VehicleModelDTO> Get(string model);
        IEnumerable<VehicleModelDTO> GetAll();
        void Delete(int id);
    }
}
