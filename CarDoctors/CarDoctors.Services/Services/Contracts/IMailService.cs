﻿using CarDoctors.Data.Models.Mail;
using System.Threading.Tasks;

namespace CarDoctors.Services.Services.Contracts
{
    public interface IMailService
    {
        Task SendEmailAsync(MailRequest mailRequest);
    }
}
