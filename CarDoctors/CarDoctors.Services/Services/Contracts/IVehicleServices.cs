﻿using CarDoctors.Data.Models;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Models.OutputModels;
using CarDoctors.Services.Models.PagingModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarDoctors.Services.Services.Contracts
{
    public interface IVehicleServices
    {
        IEnumerable<VehicleDTO> GetAll(int? id);
        Task<IEnumerable<VehicleDTO>> GetAllAsync(PagingOptionsModelVehicle optionsModel);
        Task<VehicleDTO> Get(int id);
        Task<VehicleDTO> Get(int vehicleID, int customerID);
        IQueryable<Vehicle> Filter(IQueryable<Vehicle> vehicles, int id);
        Task<VehicleDTO> Create(VehicleInputModel vehicleInputModel);
        void Delete(int id);
        Task<VehicleDTO> Update(VehicleInputModel vehicleInputModel, int id);
        //Task<IEnumerable<VehicleDTO>> GetPaginatedAsync(PagingOptionsModel options);
        Task<PaginatedListGetModel<VehicleDTO>> GetPaginatedAsync(PagingOptionsModelVehicle options, int id);
    }
}
