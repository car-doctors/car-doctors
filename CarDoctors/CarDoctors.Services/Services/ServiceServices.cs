﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Data.Models;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Helpers;
using CarDoctors.Services.Mappers;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Models.OutputModels;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.Services.Paging;
using CarDoctors.Services.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net.Http;
using System.Threading.Tasks;

namespace CarDoctors.Services.Services
{
    public class ServiceServices : IServiceServices
    {
        private readonly CarDoctorsContext carDoctorsContext;
        private readonly IMapper mapper;

        public ServiceServices(CarDoctorsContext carDoctorsContext, IMapper mapper)
        {
            this.carDoctorsContext = carDoctorsContext;
            this.mapper = mapper;
        }

        /// <summary>
        /// Returns all user, filtered by a given criteria
        /// </summary>
        /// <param name="filterServices"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        public async Task<List<ServiceDTO>> GetAll(FilterServices filterServices, string currency)
        {
            var services = await carDoctorsContext.Services.MapServiceDTO().ToListAsync();

            var servicesConverted = new List<ServiceDTO>();

            if (currency != null)
            {
                foreach (var service in services)
                {
                    servicesConverted.Add(await ConvertCurrency(currency, service));
                }
            }

            if (servicesConverted.Count != 0)
            {
                services = servicesConverted;
            }

            services = FilterServiceList(filterServices, services);

            return services;
        }

        /// <summary>
        /// Returns a paginated list of services, sorted, filtered and paginated by given criteria
        /// </summary>
        /// <param name="filterServices"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public async Task<PaginatedListGetModel<ServiceDTO>> GetPaginatedAsync(FilterServices filterServices, PagingOptionsModelService options)
        {
            var pagingOptions = this.mapper.Map<PagingOptions>(options);

            if (pagingOptions.SortByPropertyName == null)
            {
                pagingOptions.SortByPropertyName = "Price";
            }

            double rate = 1;

            string currency = "BGN";

            if (filterServices.Currency != null)
            {
                rate = await ConvertCurrency(filterServices.Currency, "BGN");
                currency = filterServices.Currency;
            }

            var query = carDoctorsContext.Services.MapServiceDTO(rate, currency);

            query = await FilterServiceList(filterServices, query);

            query = query.OrderBy(pagingOptions.SortByPropertyName, pagingOptions.SortInDescendingOrder);

            var result = await query.ToPaginatedListAsync(pagingOptions.PageNumber, pagingOptions.PageSize);

            var page = new PaginatedListGetModel<ServiceDTO>()
            {
                Elements = result.Elements,
                PageSize = result.PageSize,
                CurrentPage = result.CurrentPage,
                NumOfTotalPages = result.TotalPages,
                SortInDescendingOrder = options.SortInDescendingOrder,
                SortBy = options.SortBy,
                Name = filterServices.Name,
                PriceFrom = filterServices.PriceFrom,
                PriceIs = filterServices.PriceIs,
                PriceTo = filterServices.PriceTo,
                Currency = currency,
            };


            return page;
        }

        /// <summary>
        /// Filters services given by a criteria and returns an IQueryable
        /// </summary>
        /// <param name="filterServices"></param>
        /// <param name="serviceDTOs"></param>
        /// <returns></returns>
        private static async Task<IQueryable<ServiceDTO>> FilterServiceList(FilterServices filterServices, IQueryable<ServiceDTO> serviceDTOs)
        {
            if (filterServices.PriceFrom != null)
            {
                serviceDTOs = serviceDTOs.Where(s => s.Price > filterServices.PriceFrom);
            }

            if (filterServices.PriceTo != null)
            {
                serviceDTOs = serviceDTOs.Where(s => s.Price < filterServices.PriceTo);
            }

            if (filterServices.PriceIs != null)
            {
                serviceDTOs = serviceDTOs.Where(s => s.Price == filterServices.PriceIs);
            }

            if (filterServices.Name != null)
            {
                serviceDTOs = serviceDTOs.Where(s => s.ServiceName == filterServices.Name);
            }

            return serviceDTOs;
        }

        /// <summary>
        /// Filters services by a given criteria and returns a List
        /// </summary>
        /// <param name="filterServices"></param>
        /// <param name="serviceDTOs"></param>
        /// <returns></returns>
        private static List<ServiceDTO> FilterServiceList(FilterServices filterServices, List<ServiceDTO> serviceDTOs)
        {
            if (filterServices.PriceFrom != null)
            {
                serviceDTOs = serviceDTOs.Where(s => s.Price > filterServices.PriceFrom).ToList();
            }

            if (filterServices.PriceTo != null)
            {
                serviceDTOs = serviceDTOs.Where(s => s.Price < filterServices.PriceTo).ToList();
            }

            if (filterServices.PriceIs != null)
            {
                serviceDTOs = serviceDTOs.Where(s => s.Price == filterServices.PriceIs).ToList();
            }

            if (filterServices.Name != null)
            {
                serviceDTOs = serviceDTOs.Where(s => s.ServiceName == filterServices.Name).ToList();
            }

            return serviceDTOs;
        }

        /// <summary>
        /// Returns the exchange rate between two currencies
        /// </summary>
        /// <param name="currencyTo"></param>
        /// <param name="currencyFrom"></param>
        /// <returns></returns>
        public async Task<double> ConvertCurrency(string currencyTo, string currencyFrom)
        {
            currencyTo = currencyTo.Trim().ToUpper();

            var client = new HttpClient();

            var url = "https://free.currconv.com/api/v7/convert" + $"?q={currencyFrom}_{currencyTo}&compact=ultra&apiKey=52f11353b11228e07d62";

            var responseTask = client.GetAsync(url);

            responseTask.Wait();

            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                var json = await result.Content.ReadAsStringAsync();

                try
                {
                    var rate = Double.Parse(json.Split(':')[1].TrimEnd('}'));

                    var returnResult = 1 * rate;

                    return returnResult;
                }
                catch (Exception)
                {
                    throw new InvalidCurrencyException("Invalid currency");
                }
            }
            else
            {
                throw new InvalidCurrencyException("An error occurred");
            }
        }

        /// <summary>
        /// Converts the price of a given service to a given currency and returns the service
        /// </summary>
        /// <param name="currency"></param>
        /// <param name="service"></param>
        /// <returns></returns>
        public async Task<ServiceDTO> ConvertCurrency(string currency, ServiceDTO service)
        {
            currency = currency.Trim().ToUpper();

            var client = new HttpClient();

            var url = "https://free.currconv.com/api/v7/convert" + $"?q={service.Currency}_{currency}&compact=ultra&apiKey=52f11353b11228e07d62";

            var responseTask = client.GetAsync(url);

            responseTask.Wait();

            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                var json = await result.Content.ReadAsStringAsync();

                var rate = Double.Parse(json.Split(':')[1].TrimEnd('}'));

                service.Currency = currency;

                service.Price = Math.Round(service.Price * rate, 2);

                return service;
            }
            else
            {
                throw new InvalidCurrencyException("An error occurred");
            }

        }

        /// <summary>
        /// Returns a service with a given id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ServiceDTO> Get(int id)
        {
            var service = await carDoctorsContext.Services
                .FirstOrDefaultAsync(s => s.ServiceID == id)
                ?? throw new ObjectNotFoundException(String.Format(Strings.OBJECT_NOT_FOUND_ERROR, "Service", id));

            return service.MapServiceDTO();
        }

        /// <summary>
        /// Creates a service from a given model
        /// </summary>
        /// <param name="serviceInputModel"></param>
        /// <returns></returns>
        public async Task<ServiceDTO> Create(ServiceInputModel serviceInputModel)
        {
            CheckModel(serviceInputModel);

            var serviceDb = await this.carDoctorsContext.Services.IgnoreQueryFilters().FirstOrDefaultAsync(s => s.ServiceName == serviceInputModel.Name && s.Price == serviceInputModel.Price);

            if (serviceDb != null)
            {
                if (serviceDb.isDeleted == false)
                {
                    throw new ObjectDuplicateException(String.Format(Strings.NAME_DUPLICATE_ERROR, "Service"));
                }

                serviceDb.isDeleted = false;

                await carDoctorsContext.SaveChangesAsync();

                return serviceDb.MapServiceDTO();
            }

            var service = new Service()
            {
                ServiceName = serviceInputModel.Name,
                Price = serviceInputModel.Price,
                Currency = serviceInputModel.Currency
            };

            await this.carDoctorsContext.AddAsync(service);

            await carDoctorsContext.SaveChangesAsync();

            return service.MapServiceDTO();
        }

        /// <summary>
        /// Updates a service to a given model
        /// </summary>
        /// <param name="serviceInputModel"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ServiceDTO> Update(ServiceInputModel serviceInputModel, int id)
        {
            CheckModel(serviceInputModel);

            var service = await this.carDoctorsContext.Services
                .FirstOrDefaultAsync(s => s.ServiceID == id)
                ?? throw new ObjectNotFoundException(String.Format(Strings.OBJECT_NOT_FOUND_ERROR, "Service", id));

            service.ServiceName = serviceInputModel.Name;
            service.Price = serviceInputModel.Price;
            service.Currency = serviceInputModel.Currency;

            await carDoctorsContext.SaveChangesAsync();

            return service.MapServiceDTO();
        }

        /// <summary>
        /// Deletes a service with a given id
        /// </summary>
        /// <param name="id"></param>
        public void Delete(int id)
        {
            var service = this.carDoctorsContext.Services
                .FirstOrDefault(s => s.ServiceID == id)
                ?? throw new ObjectNotFoundException(String.Format(Strings.OBJECT_NOT_FOUND_ERROR, "Service", id));

            this.carDoctorsContext.Services.Remove(service);

            carDoctorsContext.SaveChanges();
        }

        /// <summary>
        /// Checks whether a given service model meets the requirements
        /// </summary>
        /// <param name="serviceInputModel"></param>
        public void CheckModel(ServiceInputModel serviceInputModel)
        {
            if (serviceInputModel.Name == null)
            {
                throw new ObjectNullException(String.Format(Strings.NULL_MESSAGE_ERROR, "Service"));
            }

            if (serviceInputModel.Price < 0)
            {
                throw new NegativePriceException(String.Format(Strings.NEGATIVE_ERROR, "Price"));
            }
        }
    }
}
