﻿using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Data.Models;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Helpers;
using CarDoctors.Services.Mappers;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.OutputModels;
using CarDoctors.Services.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarDoctors.Services.Services
{
    public class UserService : IUserService
    {
        private readonly AppSettings _appSettings;
        private readonly CarDoctorsContext carDoctorsContext;

        public UserService(IOptions<AppSettings> appSettings, CarDoctorsContext carDoctorsContext)
        {
            _appSettings = appSettings.Value;
            this.carDoctorsContext = carDoctorsContext;
        }
        /// <summary>
        /// Gets an user from the database by his email and returns him
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<User> GetUserByEmailAsync(string email)
        {
            var model = await this.carDoctorsContext.Users.FirstOrDefaultAsync(x => x.Email == email);

            if (model == null)
            {
                throw new ObjectNotFoundException(String.Format(Strings.OBJECT_NAME_NOT_FOUND_ERROR, "User", email));
            }

            return model;
        }
        /// <summary>
        /// Gets an user from the database by his email and returns him
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public User GetUserByEmail(string email)
        {
            var model = this.carDoctorsContext.Users.FirstOrDefault(x => x.Email == email);

            if (model == null)
            {
                throw new ObjectNotFoundException(String.Format(Strings.OBJECT_NAME_NOT_FOUND_ERROR, "User", email));
            }

            return model;
        }
        /// <summary>
        /// Getting all the users from the database and returns them
        /// </summary>
        /// <returns></returns>
        public IEnumerable<User> GetAll()
        {
            return this.carDoctorsContext.Users;
        }
        /// <summary>
        /// Gets an user by his id from the database and returns him
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User GetById(int id)
        {
            return this.carDoctorsContext.Users.FirstOrDefault(x => x.Id == id);
        }
        /// <summary>
        /// Filters the users from the database by filter properties and returns them
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public List<UserDTO> FilterUsers(FilterUser filter)
        {
            var users = this.carDoctorsContext.Users.MapUserDTO();

            if (filter.FirstName != null)
            {
                users = users.Where(u => u.FirstName == filter.FirstName);
            }

            if (filter.LastName != null)
            {
                users = users.Where(u => u.LastName == filter.LastName);
            }

            if (filter.Email != null)
            {
                users = users.Where(u => u.Email == filter.Email);
            }

            if (filter.Phone != null)
            {
                users = users.Where(u => u.Phone == filter.Phone);
            }

            if (filter.VehicleModel != null)
            {
                var vehicles = this.carDoctorsContext.Vehicles.Where(x => x.VehicleModel.Model == filter.VehicleModel).Select(y => y.OwnerID).ToList();

                users = users.Where(x => vehicles.Contains(x.UserID));
            }

            if (filter.VehicleManufacturer != null)
            {
                var vehicles = this.carDoctorsContext.Vehicles
                    .Where(x => x.VehicleModel.Manufacturer.Name == filter.VehicleManufacturer)
                    .Select(y => y.OwnerID)
                    .ToList();

                users = users.Where(x => vehicles.Contains(x.UserID));
            }

            if (filter.VehicleType != null)
            {
                var vehicles = this.carDoctorsContext.Vehicles
                    .Where(x => x.VehicleModel.VehicleType.Name == filter.VehicleType)
                    .Select(y => y.OwnerID)
                    .ToList();

                users = users.Where(x => vehicles.Contains(x.UserID));
            }

            if (filter.VisitFrom != default)
            {
                var visits = this.carDoctorsContext.Visits
                    .Where(v => DateTime.Compare(v.VisitDate, filter.VisitFrom) > 0)
                    .Select(u => u.Vehicle.OwnerID).ToList();

                users = users.Where(x => visits.Contains(x.UserID));
            }

            if (filter.VisitTo != default)
            {
                var visits = this.carDoctorsContext.Visits.Where(v => DateTime.Compare(v.VisitDate, filter.VisitTo) < 0).Select(u => u.Vehicle.OwnerID).ToList();

                users = users.Where(x => visits.Contains(x.UserID));
            }

            return users.ToList();
        }


    }
}
