﻿using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Data.Models;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Mappers;
using CarDoctors.Services.Models.OutputModels;
using CarDoctors.Services.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarDoctors.Services.Services
{
    public class VehicleModelServices : IVehicleModelServices
    {
        private readonly CarDoctorsContext carDoctorsContext;

        public VehicleModelServices(CarDoctorsContext carDoctorsContext)
        {
            this.carDoctorsContext = carDoctorsContext;
        }
        /// <summary>
        /// Gets a vehicle model from the database by its name and returns it
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<VehicleModelDTO> Get(string model)
        {
            var vehicleModel = await this.carDoctorsContext.VehicleModels
                .MapVehicleModelDTO()
                .FirstOrDefaultAsync(v => v.Model == model)
                 ?? throw new ObjectNotFoundException(String.Format(Strings.OBJECT_NAME_NOT_FOUND_ERROR, "Vehicle", $"model {model}"));

            return vehicleModel;
        }
        /// <summary>
        /// Gets all vehicle models from the database and returns them
        /// </summary>
        /// <returns></returns>
        public IEnumerable<VehicleModelDTO> GetAll()
        {
            var vehicles = this.carDoctorsContext.VehicleModels.MapVehicleModelDTO();

            return vehicles;
        }
        /// <summary>
        /// Creates a vehicle model in the database by manufacturer name, model name and vehicle type
        /// </summary>
        /// <param name="manufacturer"></param>
        /// <param name="model"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public async Task<VehicleModel> Create(string manufacturer, string model, string type)
        {
            var dbModel = await this.carDoctorsContext.VehicleModels.FirstOrDefaultAsync(m => m.Model == model);

            if (dbModel != null)
            {
                throw new ObjectDuplicateException(String.Format(Strings.NAME_DUPLICATE_ERROR, "Vehicle model"));
            }

            var dbManufacturer = await this.carDoctorsContext.Manufacturers.FirstOrDefaultAsync(m => m.Name == manufacturer);

            if (dbManufacturer == null)
            {
                var newManufacturer = new Manufacturer() { Name = manufacturer };

                this.carDoctorsContext.Manufacturers.Add(newManufacturer);

                await this.carDoctorsContext.SaveChangesAsync();
            }

            var vehicleType = await this.carDoctorsContext.VehicleTypes.FirstOrDefaultAsync(t => t.Name == type);

            if (vehicleType == null)
            {
                throw new ObjectNullException(String.Format(Strings.NULL_MESSAGE_ERROR, "Vehicle type"));
            }

            var newModel = new VehicleModel()
            {
                Model = model,
                Manufacturer = await this.carDoctorsContext.Manufacturers.FirstOrDefaultAsync(m => m.Name == manufacturer),
                VehicleType = vehicleType
            };

            this.carDoctorsContext.VehicleModels.Add(newModel);

            await carDoctorsContext.SaveChangesAsync();

            return newModel;
        }
        /// <summary>
        /// Deletes a vehicle type from the database by its id
        /// </summary>
        /// <param name="id"></param>
        public void Delete(int id)
        {
            var vehicleModel = this.carDoctorsContext.VehicleModels
                .FirstOrDefault(v => v.VehicleModelID == id)
                ?? throw new ObjectNotFoundException(String.Format(Strings.OBJECT_NOT_FOUND_ERROR, "Vehicle model", id)); ;

            this.carDoctorsContext.VehicleModels.Remove(vehicleModel);

            carDoctorsContext.SaveChanges();
        }
    }
}
