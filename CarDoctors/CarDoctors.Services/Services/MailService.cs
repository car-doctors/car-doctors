﻿using CarDoctors.Data.Models.Mail;
using CarDoctors.Services.Services.Contracts;
//using MailKit.Net.Smtp;
//using MailKit.Security;
using Microsoft.Extensions.Options;
//using MimeKit;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CarDoctors.Services.Services
{
    public class MailService : IMailService
    {
        private readonly MailSettings _mailSettings;
        public MailService(IOptions<MailSettings> mailSettings)
        {
            _mailSettings = mailSettings.Value;
        }

        /// <summary>
        /// Sends an email 
        /// </summary>
        /// <param name="mailRequest"></param>
        /// <returns></returns>
        public async Task SendEmailAsync(MailRequest mailRequest)
        {
            var email = new MailMessage();

            email.From = new MailAddress(_mailSettings.Mail);
            email.To.Add(new MailAddress(mailRequest.ToEmail));

            if (mailRequest.FileName != null)
            {
                var attachment = new Attachment($"{mailRequest.FileName}.pdf");

                email.Attachments.Add(attachment);
            }

            email.Subject = mailRequest.Subject;
            email.Body = mailRequest.Body;

            var smtp = new SmtpClient();

            smtp.Host = _mailSettings.Host;
            smtp.Port = _mailSettings.Port;
            smtp.Credentials = new System.Net.NetworkCredential("cardocsga@gmail.com", _mailSettings.Password);
            smtp.EnableSsl = true;

            smtp.Send(email);
        }
    }
}
