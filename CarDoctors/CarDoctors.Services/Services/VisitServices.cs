﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Data.Models;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Helpers;
using CarDoctors.Services.Mappers;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Models.OutputModels;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.Services.Paging;
using CarDoctors.Services.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net.Http;
using System.Threading.Tasks;

namespace CarDoctors.Services.Services
{
    public class VisitServices : IVisitServices
    {
        private readonly CarDoctorsContext carDoctorsContext;
        private readonly IMapper mapper;

        public VisitServices(CarDoctorsContext carDoctorsContext, IMapper mapper)
        {
            this.carDoctorsContext = carDoctorsContext;
            this.mapper = mapper;
        }
        /// <summary>
        /// Generates a PDF report 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public string GenerateReport(string info)
        {
            //var visit = await this.carDoctorsContext.Visits.FirstOrDefaultAsync(v => v.VisitID == id);

            var Renderer = new IronPdf.HtmlToPdf();
            var guid = Guid.NewGuid().ToString();
            Renderer.RenderHtmlAsPdf(info).SaveAs($"{guid}.pdf");

            return guid;
        }
        /// <summary>
        /// Gets all the visits from the database and returns them
        /// </summary>
        /// <returns></returns>
        public IEnumerable<VisitDTO> GetAll()
        {
            var visits = carDoctorsContext.Visits.MapVisitDTO().ToList();

            foreach (var visit in visits)
            {
                var services = carDoctorsContext.VisitServices
                .Include(s => s.Service)
                .Where(s => s.VisitID == visit.VisitID && s.isDeleted == false)
                .Select(s => s.Service.ServiceName)
                .ToList();

                visit.ServicesDone = string.Join(" ", services);
            }

            return visits;
        }
        /// <summary>
        /// Returns a paginated list of services, sorted, filtered and paginated by given criteria
        /// </summary>
        /// <param name="options"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<PaginatedListGetModel<VisitDTO>> GetPaginatedAsync(PagingOptionsModelVisit options, FilterVisits filter)
        {
            var pagingOptions = this.mapper.Map<PagingOptions>(options);

            if (pagingOptions.SortByPropertyName == "Price")
            {
                pagingOptions.SortByPropertyName = "VisitDate";
            }

            var visits = Filter(this.carDoctorsContext.Visits, filter);

            var query = visits.MapVisitDTO().OrderBy(pagingOptions.SortByPropertyName, pagingOptions.SortInDescendingOrder);

            var result = await query.ToPaginatedListAsync(pagingOptions.PageNumber, pagingOptions.PageSize);

            return this.mapper.Map<PaginatedListGetModel<VisitDTO>>(result);
        }
        /// <summary>
        /// Converts currency from default currency - to new one by external API 
        /// </summary>
        /// <param name="currencyTo"></param>
        /// <param name="currencyFrom"></param>
        /// <returns></returns>
        public async Task<double> ConvertCurrency(string currencyTo, string currencyFrom)
        {
            currencyTo = currencyTo.Trim().ToUpper();

            var client = new HttpClient();

            var url = "https://free.currconv.com/api/v7/convert" + $"?q={currencyFrom}_{currencyTo}&compact=ultra&apiKey=52f11353b11228e07d62";

            var responseTask = client.GetAsync(url);

            responseTask.Wait();

            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
            {
                var json = await result.Content.ReadAsStringAsync();

                var rate = Double.Parse(json.Split(':')[1].TrimEnd('}'));

                var returnResult = 1 * rate;

                return returnResult;
            }
            else
            {
                throw new ArgumentException();
            }

        }
        /// <summary>
        /// Generates PDF document for collection of services and can set and convert the currency of the report
        /// </summary>
        /// <param name="visitIDs"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        public async Task<string> GeneratePDFForCollection(string visitIDs, string currency)
        {
            var arrVisits = visitIDs.Split(" ").Select(x => int.Parse(x)).ToList();

            List<VisitDTO> visitDTOs = new List<VisitDTO>();

            double totalPrice = 0;

            var rate = await this.ConvertCurrency(currency, "BGN");

            var info = $"" +
                $"<html>" +
                $"<head>" +
                $"<title>Visit:</title>" +
                $"</head>" +
                $"<body>";

            foreach (var id in arrVisits)
            {
                var visit = await this.Get(id);



                info += $"Visit Report: {visit.VisitDate}" +
                $"<ul>Services done: {visit.ServicesDone}" +
                $"</ul>" +
                $"<ul>Vehicle ID: {visit.VehicleID}" +
                $"</ul>" +
                $"<ul>Vehicle type: {visit.VehicleType}" +
                $"</ul>" +
                $"<ul>Vehicle model: {visit.ModelMake}" +
                $"</ul>" +
                $"<ul>Vehicle VIN: {visit.VIN}" +
                $"</ul>" +
                $"<ul>Vehicle Registration Plate: {visit.RegistrationPlate}" +
                $"</ul>" +
                $"<ul>Vehicle Owner ID: {visit.OwnerID}" +
                $"</ul>" +
                $"<ul>Vehicle Owner: {visit.Owner}" +
                $"</ul>" +
                $"<ul>Service Price: {Math.Round(visit.Price * rate, 2)} {currency}" +
                $"</ul>" +
                $"<ul>Visit Status: {visit.VisitStatus}" +
                $"</ul>" +
                $"<ul>Pick-up Date: {visit.PickUpDate}" +
                $"</ul>";

                totalPrice += visit.Price * rate;
            }

            info += $"<ul>TotalPrice: {Math.Round(totalPrice, 2)} {currency}</ul></body><html>";

            var name = GenerateReport(info);

            return name;
        }
        /// <summary>
        /// Gets a visit by its id from the database and returns it
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<VisitDTO> Get(int id)
        {
            var visit = await carDoctorsContext.Visits
                .MapVisitDTO()
                .FirstOrDefaultAsync(s => s.VisitID == id)
                ?? throw new ObjectNotFoundException(String.Format(Strings.OBJECT_NOT_FOUND_ERROR, "Visit", id));

            var services = await carDoctorsContext.VisitServices
                .Include(s => s.Service)
                .Where(s => s.VisitID == visit.VisitID)
                .Select(s => s.Service.ServiceName)
                .ToListAsync();

            visit.ServicesDone = string.Join(", ", services);

            return visit;
        }
        /// <summary>
        /// Gets a specific visit of a customer by customer's id and visit's id
        /// </summary>
        /// <param name="visitID"></param>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public async Task<VisitDTO> Get(int visitID, int customerID)
        {
            var visit = await carDoctorsContext.Visits
                .MapVisitDTO()
                .FirstOrDefaultAsync(s => s.VisitID == visitID)
                ?? throw new ObjectNotFoundException(String.Format(Strings.OBJECT_NOT_FOUND_ERROR, "Visit", visitID));

            var services = await carDoctorsContext.VisitServices
                .Include(s => s.Service)
                .Where(s => s.VisitID == visit.VisitID)
                .Select(s => s.Service.ServiceName)
                .ToListAsync();

            visit.ServicesDone = string.Join(", ", services);

            var owner = await this.carDoctorsContext.Users.FirstOrDefaultAsync(u => u.Id == customerID);

            if (!visit.Owner.Equals($"{owner.FirstName} {owner.LastName}"))
            {
                throw new UserUnauthorizedException(String.Format(Strings.UNAUTHORIZED_USER, $"{owner.FirstName} {owner.LastName}"));
            }

            return visit;
        }
        /// <summary>
        /// Creates a visit in the database from an input model containing the visit properties.
        /// </summary>
        /// <param name="visitInputModel"></param>
        /// <returns></returns>
        public async Task<VisitDTO> Create(VisitInputModel visitInputModel)
        {
            var dbVisit = await this.carDoctorsContext.Visits
                .IgnoreQueryFilters()
                .FirstOrDefaultAsync(v => v.VehicleID == visitInputModel.VehicleID && v.PickUpDate == visitInputModel.PickupDate && v.VisitDate == visitInputModel.VisitDate);

            if (dbVisit != null)
            {
                if (dbVisit.isDeleted == false)
                {
                    throw new ObjectDuplicateException(String.Format(Strings.NAME_DUPLICATE_ERROR, "Visit"));
                }

                dbVisit.isDeleted = false;

                await this.carDoctorsContext.SaveChangesAsync();

                var dbVisitDTO = await this.carDoctorsContext.Visits
                .IgnoreQueryFilters()
                .MapVisitDTO()
                .FirstOrDefaultAsync(v => v.VehicleID == visitInputModel.VehicleID && v.PickUpDate == visitInputModel.PickupDate && v.VisitDate == visitInputModel.VisitDate);

                return dbVisitDTO;
            }

            var vehicle = await carDoctorsContext.Vehicles
                .FirstOrDefaultAsync(v => v.VehicleID == visitInputModel.VehicleID);

            if (vehicle == null)
            {
                throw new ObjectNullException(String.Format(Strings.NULL_MESSAGE_ERROR, "Vehicle"));
            }

            var visit = new Visit()
            {
                VehicleID = visitInputModel.VehicleID,
                VisitDate = visitInputModel.VisitDate,
                PickUpDate = visitInputModel.PickupDate,
                VisitStatusID = visitInputModel.VisitStatusID,
            };

            await this.carDoctorsContext.Visits.AddAsync(visit);

            await carDoctorsContext.SaveChangesAsync();

            var services = visitInputModel.ServiceID.Split().ToList();

            foreach (var serviceID in services)
            {
                var service = await this.carDoctorsContext.Services
                .FirstOrDefaultAsync(s => s.ServiceID == int.Parse(serviceID));

                var visitService = new VisitService()
                {
                    VisitID = visit.VisitID,
                    ServiceID = service.ServiceID
                };

                await this.carDoctorsContext.VisitServices.AddAsync(visitService);

                await this.carDoctorsContext.SaveChangesAsync();
            }

            var visitDTO = await this.carDoctorsContext.Visits
                 .MapVisitDTO()
                 .FirstOrDefaultAsync(v => v.VisitID == visit.VisitID);

            return visitDTO;
        }
        /// <summary>
        /// Updates a visit in the database by its id from an input model containing the updated visit properties.
        /// </summary>
        /// <param name="visitInputModel"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<VisitDTO> Update(VisitInputModel visitInputModel, int id)
        {
            var visit = await this.carDoctorsContext.Visits
                 .FirstOrDefaultAsync(v => v.VisitID == id)
                 ?? throw new ObjectNotFoundException(String.Format(Strings.OBJECT_NOT_FOUND_ERROR, "Visit", id));

            visit.VehicleID = visitInputModel.VehicleID;
            visit.VisitDate = visitInputModel.VisitDate;
            visit.PickUpDate = visitInputModel.PickupDate;
            visit.VisitStatusID = visitInputModel.VisitStatusID;

            var visitServices = this.carDoctorsContext.VisitServices.Where(v => v.VisitID == id && v.isDeleted == false).ToList();

            var services = visitInputModel.ServiceID.Split().ToList();

            foreach (var service in visitServices)
            {
                if (!services.Contains(service.ServiceID.ToString()))
                {
                    this.carDoctorsContext.VisitServices.Remove(service);

                    await this.carDoctorsContext.SaveChangesAsync();
                }
            }

            foreach (var serviceID in services)
            {
                var service = await this.carDoctorsContext.Services
                .FirstOrDefaultAsync(s => s.ServiceID == int.Parse(serviceID));

                var visitService = new VisitService()
                {
                    VisitID = visit.VisitID,
                    ServiceID = service.ServiceID
                };

                if (!this.carDoctorsContext.VisitServices.Contains(visitService))
                {
                    await this.carDoctorsContext.VisitServices.AddAsync(visitService);

                    await this.carDoctorsContext.SaveChangesAsync();
                }
            }

            await this.carDoctorsContext.SaveChangesAsync();

            var visitDTO = await this.Get(id);

            return visitDTO;
        }
        /// <summary>
        /// Deletes a visit by its id from the database
        /// </summary>
        /// <param name="id"></param>
        public void Delete(int id)
        {
            {
                var visit = this.carDoctorsContext.Visits
                    .FirstOrDefault(s => s.VisitID == id)
                    ?? throw new ObjectNotFoundException(String.Format(Strings.OBJECT_NOT_FOUND_ERROR, "Visit", id));

                this.carDoctorsContext.Visits.Remove(visit);

                carDoctorsContext.SaveChanges();
            }
        }
        /// <summary>
        /// Returns customers visit by its id filtered
        /// </summary>
        /// <param name="id"></param>
        /// <param name="filterVisits"></param>
        /// <returns></returns>
        public List<VisitDTO> GetCustomerServices(int id, FilterVisits filterVisits)
        {
            var visits = this.GetAll().Where(v => v.OwnerID == id).ToList();

            visits = this.Filter(visits, filterVisits);

            return visits;
        }
        /// <summary>
        /// Filters the visits from the database by filter properties and returns them
        /// </summary>
        /// <param name="visits"></param>
        /// <param name="filterVisits"></param>
        /// <returns></returns>
        public IQueryable<Visit> Filter(IQueryable<Visit> visits, FilterVisits filterVisits)
        {
            if (filterVisits.Email != null)
            {
                var vehicle = this.carDoctorsContext.Vehicles.FirstOrDefault(v => v.Owner.Email == filterVisits.Email);

                visits = visits.Where(v => v.VehicleID == vehicle.VehicleID);
            }

            if (filterVisits.VehicleID != null)
            {
                visits = visits.Where(v => v.VehicleID == filterVisits.VehicleID);
            }

            if (filterVisits.DateFrom != default)
            {
                visits = visits.Where(v => DateTime.Compare((DateTime)filterVisits.DateFrom, v.VisitDate) < 0);
            }

            if (filterVisits.DateTo != default)
            {
                visits = visits.Where(v => DateTime.Compare((DateTime)filterVisits.DateTo, v.VisitDate) > 0);
            }

            return visits;
        }
        /// <summary>
        /// Filters the visits from the database by filter properties and returns them
        /// </summary>
        /// <param name="visits"></param>
        /// <param name="filterVisits"></param>
        /// <returns></returns>
        public List<VisitDTO> Filter(List<VisitDTO> visits, FilterVisits filterVisits)
        {
            if (filterVisits.VehicleID != null)
            {
                visits = visits.Where(v => v.VehicleID == filterVisits.VehicleID).ToList();
            }

            if (filterVisits.DateFrom != null)
            {
                visits = visits.Where(v => v.VisitDate.CompareTo(filterVisits.DateFrom) > 0).ToList();
            }

            if (filterVisits.DateTo != null)
            {
                visits = visits.Where(v => v.VisitDate.CompareTo(filterVisits.DateTo) < 0).ToList();
            }

            return visits;
        }
        /// <summary>
        /// Filters the visits from the database for a specific customer by filter properties and returns them
        /// </summary>
        /// <param name="visits"></param>
        /// <param name="filterVisits"></param>
        /// <returns></returns>
        public IQueryable<VisitDTO> FilterForCustomer(IQueryable<VisitDTO> visits, FilterVisits filterVisits)
        {
            var owner = this.carDoctorsContext.Users.FirstOrDefault(v => v.Email == filterVisits.Email);

            visits = visits.Where(v => v.OwnerID == owner.Id);


            if (filterVisits.VehicleID != null)
            {
                visits = visits.Where(v => v.VehicleID == filterVisits.VehicleID);
            }

            if (filterVisits.DateFrom != default)
            {
                visits = visits.Where(v => DateTime.Compare((DateTime)filterVisits.DateFrom, v.VisitDate) < 0);
            }

            if (filterVisits.DateTo != default)
            {
                visits = visits.Where(v => DateTime.Compare((DateTime)filterVisits.DateTo, v.VisitDate) > 0);
            }

            return visits;
        }
        /// <summary>
        /// Returns a paginated list of visits, sorted, filtered and paginated by given criteria
        /// </summary>
        /// <param name="options"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<PaginatedListGetModel<VisitDTO>> GetPaginatedForCustomerAsync(PagingOptionsModelVisit options, FilterVisits filter)
        {
            var pagingOptions = this.mapper.Map<PagingOptions>(options);

            if (pagingOptions.SortByPropertyName == null)
            {
                pagingOptions.SortByPropertyName = "VisitDate";
            }

            var query = this.carDoctorsContext.Visits.MapVisitDTO();

            query = FilterForCustomer(query, filter);

            query = query.OrderBy(pagingOptions.SortByPropertyName, pagingOptions.SortInDescendingOrder);

            var result = await query.ToPaginatedListAsync(pagingOptions.PageNumber, pagingOptions.PageSize);

            var page = new PaginatedListGetModel<VisitDTO>()
            {
                PageSize = result.PageSize,
                CurrentPage = result.CurrentPage,
                Elements = result.Elements,
                NumOfTotalPages = result.TotalPages,
                SortInDescendingOrder = options.SortInDescendingOrder,
                SortBy = options.SortBy,
                VehicleID = filter.VehicleID,
                DateFrom = filter.DateFrom,
                DateTo = filter.DateTo
            };

            return page;
        }
    }
}
