﻿using AutoMapper;
using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Data.Models;
using CarDoctors.Services.Exceptions;
using CarDoctors.Services.Helpers;
using CarDoctors.Services.Mappers;
using CarDoctors.Services.Models.FilterModels;
using CarDoctors.Services.Models.InputModels;
using CarDoctors.Services.Models.OutputModels;
using CarDoctors.Services.Models.PagingModels;
using CarDoctors.Services.Paging;
using CarDoctors.Services.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CarDoctors.Services.Services
{
    public class AccountServices : IAccountServices
    {
        private readonly CarDoctorsContext carDoctorsContext;
        private readonly IUserService userService;
        private readonly IRoleServices roleServices;
        private readonly IMapper mapper;
        private readonly AppSettings appSettings;

        public AccountServices(CarDoctorsContext carDoctorsContext, IUserService userService, IOptions<AppSettings> appSettings, IRoleServices roleServices, IMapper mapper)
        {
            this.carDoctorsContext = carDoctorsContext;
            this.userService = userService;
            this.roleServices = roleServices;
            this.mapper = mapper;
            this.appSettings = appSettings.Value;
        }

        /// <summary>
        /// Login using email and password. Returns User DTO
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<UserDTO> Login(string email, string password)
        {
            var user = await userService.GetUserByEmailAsync(email);

            if (PasswordHasher.VerifyPassword(password, user.PasswordHash))
            {
                return user.MapUserDTO();
            }
            else
            {
                throw new InvaludUsernameOrPasswordException(String.Format(Strings.USERNAME_OR_PASSWORD_ERROR));
            }
        }


        /// <summary>
        /// Sends request for password reset
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<string> RequestPasswordReset(string email)
        {
            var user = await this.userService.GetUserByEmailAsync(email);

            if (user != null)
            {
                var guid = Guid.NewGuid();

                var resetPass = new ResetPass()
                {
                    UserId = user.Id,
                    Time = DateTime.Now,
                    Guid = guid
                };

                this.carDoctorsContext.ResetPasses.Add(resetPass);

                await this.carDoctorsContext.SaveChangesAsync();

                return guid.ToString();
            }

            throw new InvalidPasswordResetRequestException(String.Format(Strings.PASSWORD_ERROR, "reset request."));
        }

        /// <summary>
        /// Resets the password of a user setting it to a given one
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task ResetPass(string guid, string email, string password)
        {
            var user = await this.userService.GetUserByEmailAsync(email);

            var resetPass = await this.carDoctorsContext.ResetPasses.FirstOrDefaultAsync(x => x.UserId == user.Id && DateTime.Compare(x.Time.AddHours(1), DateTime.Now) > 0);

            if (resetPass == null)
            {
                throw new InvalidPasswordResetException(String.Format(Strings.PASSWORD_ERROR, "reset link."));
            }

            if (DateTime.Compare(resetPass.Time.AddHours(1), DateTime.Now) > 0 && resetPass.Guid.ToString() == guid)
            {
                var newPassHash = PasswordHasher.HashPassword(password);

                user.PasswordHash = newPassHash;

                this.carDoctorsContext.ResetPasses.Remove(resetPass);

                await this.carDoctorsContext.SaveChangesAsync();
            }
            else
            {
                throw new InvalidPasswordResetException(String.Format(Strings.PASSWORD_ERROR, "reset link."));
            }
        }

        /// <summary>
        /// Generates a JWT token for a given user
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        public string GenerateJWTToken(UserDTO userDTO)
        {
            var user = this.userService.GetUserByEmail(userDTO.Email);

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(appSettings.Key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new List<Claim> {
                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                new Claim(JwtRegisteredClaimNames.Aud, appSettings.Audience),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds().ToString()),
                new Claim(ClaimTypes.Name, user.Id.ToString())
            };

            var userRoles = this.carDoctorsContext.UserRoles
                .Include(u => u.Role)
                .Where(u => u.UserID == user.Id);

            foreach (var role in userRoles)
            {
                if (role.Role.Name == "Customer")
                {
                    claims.Add(new Claim(ClaimTypes.Role, "Customer"));
                }
                else if (role.Role.Name == "Employee")
                {
                    claims.Add(new Claim(ClaimTypes.Role, "Employee"));
                }
                else if (role.Role.Name == "Admin")
                {
                    claims.Add(new Claim(ClaimTypes.Role, "Admin"));
                }
            }

            var token = new JwtSecurityToken(
                issuer: appSettings.Issuer,
                audience: null,
                claims: claims,

                expires: DateTime.UtcNow.Add(TimeSpan.FromMinutes(appSettings.ExpirationInMinutes)),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        /// <summary>
        /// Creates a new user with given details
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<UserDTO> Register(User user, string password)
        {
            var role = await this.roleServices.GetByNameAsync("Customer");

            user.PasswordHash = PasswordHasher.HashPassword(password);

            await this.carDoctorsContext.Users.AddAsync(user);

            await this.carDoctorsContext.SaveChangesAsync();

            user = await this.carDoctorsContext.Users.FirstOrDefaultAsync(x => x.Email == user.Email);

            var userRole = new UserRole()
            {
                RoleID = role.RoleID,
                UserID = user.Id
            };

            await this.carDoctorsContext.UserRoles.AddAsync(userRole);

            await this.carDoctorsContext.SaveChangesAsync();

            return user.MapUserDTO();
        }

        /// <summary>
        /// Resets the password of a given user, setting it to a random one
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<string> ResetPassword(string email)
        {
            var user = await this.userService.GetUserByEmailAsync(email);

            var newPass = Guid.NewGuid().ToString();

            user.PasswordHash = PasswordHasher.HashPassword(newPass);

            await this.carDoctorsContext.SaveChangesAsync();

            return newPass;
        }


        /// <summary>
        /// Changes the password of a user with a new one
        /// </summary>
        /// <param name="email"></param>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public async Task<UserDTO> ChangePassword(string email, string oldPassword, string newPassword)
        {
            var user = await this.userService.GetUserByEmailAsync(email);

            if (PasswordHasher.VerifyPassword(oldPassword, user.PasswordHash))
            {
                user.PasswordHash = PasswordHasher.HashPassword(newPassword);

                await this.carDoctorsContext.SaveChangesAsync();

                return user.MapUserDTO();
            }
            else
            {
                throw new InvalidPasswordResetException(String.Format(Strings.PASSWORD_ERROR, "change attempt."));
            }
        }

        /// <summary>
        /// Deletes a user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteAsync(int id)
        {
            var user = await this.carDoctorsContext.Users.FirstOrDefaultAsync(u => u.Id == id);

            this.carDoctorsContext.Users.Remove(user);

            var userRoles = this.carDoctorsContext.UserRoles.Where(u => u.UserID == id);

            foreach (var role in userRoles)
            {
                this.carDoctorsContext.UserRoles.Remove(role);
            }

            await this.carDoctorsContext.SaveChangesAsync();
        }

        /// <summary>
        /// Returns a paginated list of user with options
        /// </summary>
        /// <param name="options"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public async Task<PaginatedListGetModel<UserDTO>> GetPaginatedAsync(PagingOptionsModelUser options, FilterUser filter)
        {
            var pagingOptions = this.mapper.Map<PagingOptions>(options);

            if (pagingOptions.SortByPropertyName == "Price")
            {
                pagingOptions.SortByPropertyName = "FirstName";
            }

            var query = this.FilterUsers(filter);

            query = query.OrderBy(pagingOptions.SortByPropertyName, pagingOptions.SortInDescendingOrder);

            var result = await query.ToPaginatedListAsync(pagingOptions.PageNumber, pagingOptions.PageSize);

            var page = new PaginatedListGetModel<UserDTO>()
            {
                PageSize = result.PageSize,
                CurrentPage = result.CurrentPage,
                Elements = result.Elements,
                NumOfTotalPages = result.TotalPages,
                SortInDescendingOrder = options.SortInDescendingOrder,
                SortBy = options.SortBy,
                VehicleType = filter.VehicleType,
                VisitTo = filter.VisitTo,
                VisitFrom = filter.VisitFrom,
                VehicleModel = filter.VehicleModel,
                VehicleManufacturer = filter.VehicleManufacturer,
                Phone = filter.Phone,
                Email = filter.Email,
            };

            return page;
        }

        /// <summary>
        /// Filters the users by a given criteria
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IQueryable<UserDTO> FilterUsers(FilterUser filter)
        {
            var users = this.carDoctorsContext.Users
                .Where(u => u.UserRoles.Any(r => r.Role.Name == "Admin") == false)
                .MapUserDTO();

            if (filter.FirstName != null)
            {
                users = users.Where(u => u.FirstName == filter.FirstName);
            }

            if (filter.LastName != null)
            {
                users = users.Where(u => u.LastName == filter.LastName);
            }

            if (filter.Email != null)
            {
                users = users.Where(u => u.Email == filter.Email);
            }

            if (filter.Phone != null)
            {
                users = users.Where(u => u.Phone == filter.Phone);
            }

            if (filter.VehicleModel != null)
            {
                var vehicles = this.carDoctorsContext.Vehicles.Where(x => x.VehicleModel.Model == filter.VehicleModel).Select(y => y.OwnerID).ToList();

                users = users.Where(x => vehicles.Contains(x.UserID));
            }

            if (filter.VehicleManufacturer != null)
            {
                var vehicles = this.carDoctorsContext.Vehicles
                    .Where(x => x.VehicleModel.Manufacturer.Name == filter.VehicleManufacturer)
                    .Select(y => y.OwnerID)
                    .ToList();

                users = users.Where(x => vehicles.Contains(x.UserID));
            }

            if (filter.VehicleType != null)
            {
                var vehicles = this.carDoctorsContext.Vehicles
                    .Where(x => x.VehicleModel.VehicleType.Name == filter.VehicleType)
                    .Select(y => y.OwnerID)
                    .ToList();

                users = users.Where(x => vehicles.Contains(x.UserID));
            }

            if (filter.VisitFrom != default)
            {
                var visits = this.carDoctorsContext.Visits
                    .Where(v => DateTime.Compare(v.VisitDate, filter.VisitFrom) > 0)
                    .Select(u => u.Vehicle.OwnerID).ToList();

                users = users.Where(x => visits.Contains(x.UserID));
            }

            if (filter.VisitTo != default)
            {
                var visits = this.carDoctorsContext.Visits.Where(v => DateTime.Compare(v.VisitDate, filter.VisitTo) < 0).Select(u => u.Vehicle.OwnerID).ToList();

                users = users.Where(x => visits.Contains(x.UserID));
            }

            return users;
        }

        /// <summary>
        /// Updates a user by a given model
        /// </summary>
        /// <param name="userInputModel"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<UserDTO> UpdateAsync(UserViewInputModel userInputModel, string email)
        {
            var user = await this.carDoctorsContext.Users.FirstOrDefaultAsync(x => x.Email == email)
                 ?? throw new ObjectNotFoundException(String.Format(Strings.OBJECT_NOT_FOUND_ERROR, "User", email)); ;

            user.FirstName = userInputModel.FirstName;
            user.LastName = userInputModel.LastName;
            user.Email = userInputModel.Email;
            user.Phone = userInputModel.Phone;

            await this.carDoctorsContext.SaveChangesAsync();

            return user.MapUserDTO();
        }
    }
}
