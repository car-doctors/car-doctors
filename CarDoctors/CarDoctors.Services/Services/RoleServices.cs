﻿using CarDoctors.Data.CarDoctorsDbContext;
using CarDoctors.Data.Models;
using CarDoctors.Services.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace CarDoctors.Services.Services
{
    public class RoleServices : IRoleServices
    {
        private readonly CarDoctorsContext carDoctorsContext;

        public RoleServices(CarDoctorsContext carDoctorsContext)
        {
            this.carDoctorsContext = carDoctorsContext;
        }

        /// <summary>
        /// Returns a role by a given name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<Role> GetByNameAsync(string name)
        {
            var role = await this.carDoctorsContext.Roles.FirstOrDefaultAsync(x => x.Name == name);

            return role;
        }
    }
}
