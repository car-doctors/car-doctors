﻿namespace CarDoctors.Services
{
    public class Strings
    {
        public const string NAME_INVALID_ERROR = "{0} should be between {1} and {2} characters long.";
        public const string NAME_DUPLICATE_ERROR = "{0} already exists.";
        public const string NULL_MESSAGE_ERROR = "{0} cannot be null.";
        public const string NEGATIVE_ERROR = "{0} cannot be negative.";
        public const string OBJECT_NOT_FOUND_ERROR = "{0} with ID {1} does not exist.";
        // public const string USER_NOT_FOUND_ERROR = "{0} with email '{1}' does not exist.";
        // public const string NOT_AUTHORIZED_ERROR = "Customer with email '{0}' does not have permission for this action.";
        // public const string OBJECT_LENGTH_ERROR = "{0} should be between {1} and {2} symbols long.";
        public const string PASSWORD_ERROR = "Invalid password {0}";
        public const string USERNAME_OR_PASSWORD_ERROR = "Invalid username or password";
        public const string OBJECT_NAME_NOT_FOUND_ERROR = "{0} with {1} does not exist.";
        public const string CURRENCY_INVALID_ERROR = "Currency {0} does not exist.";
        public const string UNAUTHORIZED_USER = "User {0} is not authorized for this action.";
    }
}
