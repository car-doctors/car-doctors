﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace CarDoctors.Services.Models.PagingModels
{
    public class PaginatedListGetModel<T>
    {
        public IEnumerable<T> Elements { get; set; }

        public int CurrentPage { get; set; }

        public int PageSize { get; set; }

        public int NumOfTotalPages { get; set; }

        public bool Previous => CurrentPage > 1;

        public bool Next => CurrentPage < NumOfTotalPages;

        [DisplayName("Price From")]
        public double? PriceFrom { get; set; }

        [DisplayName("Price To")]
        public double? PriceTo { get; set; }

        [DisplayName("Price Is")]
        public double? PriceIs { get; set; }

        [DisplayName("Service Name")]
        public string Name { get; set; }

        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Phone")]
        public string Phone { get; set; }

        [DisplayName("Vehicle Manufacturer")]
        public string VehicleManufacturer { get; set; }

        [DisplayName("Vehicle Model")]
        public string VehicleModel { get; set; }

        [DisplayName("Vehicle Type")]
        public string VehicleType { get; set; }

        public string Currency { get; set; }

        public double Price { get; set; }

        [DisplayName("Sort By")]
        public string SortBy { get; set; } = Resources.Strings.DefaultPagingOptionsServiceSortByProperty;

        [DisplayName("Sort In Descending Order")]
        public bool SortInDescendingOrder { get; set; } = false;

        public int ServiceID { get; set; }

        [DisplayName("Owner ID")]
        public int OwnerID { get; set; }

        [DisplayName("Vehicle ID")]
        public int? VehicleID { get; set; }

        [DisplayName("Date From")]
        public DateTime? DateFrom { get; set; }

        [DisplayName("Date To")]
        public DateTime? DateTo { get; set; }

        [DisplayName("Visit From")]
        public DateTime VisitFrom { get; set; }

        [DisplayName("Visit To")]
        public DateTime VisitTo { get; set; }
    }
}
