﻿using CarDoctors.Services.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Services.Models.PagingModels
{
    public class PagingOptionsModelUser
    {
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public int Page { get; set; } = Numbers.DefaultPage;

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public int PageSize { get; set; } = Numbers.DefaultPageSize;

        public string SortBy { get; set; } = Resources.Strings.DefaultPagingOptionsCustomerSortByProperty;

        public bool SortInDescendingOrder { get; set; } = false;

    }
}
