﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Services.Models.InputModels
{
    public class VisitInputModel
    {
        public VisitInputModel()
        {
            VisitStatusID = 1;
        }

        [DisplayName("Vehicle")]
        public int VehicleID { get; set; }

        [DisplayName("Visit Date")]
        public DateTime VisitDate { get; set; }

        [DisplayName("Pick Up Date")]
        public DateTime PickupDate { get; set; }

        [DisplayName("Visit")]
        public int VisitStatusID { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        [DisplayName("Services done")]
        public string ServiceID { get; set; }
    }
}
