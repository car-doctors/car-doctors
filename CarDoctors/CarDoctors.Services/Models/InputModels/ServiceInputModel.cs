﻿using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Services.Models.InputModels
{
    public class ServiceInputModel
    {
        public int ServiceID { get; set; }
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Service name should be between 3 and 50 characters")]
        public string Name { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Price cannot be negative")]
        public double Price { get; set; }
        public string Currency { get; set; } = "BGN";
    }
}
