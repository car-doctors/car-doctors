﻿using CarDoctors.Services.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Services.Models.InputModels
{
    public class CustomerPaginatedModel
    {
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public int Page { get; set; } = Numbers.DefaultPage;

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public int PageSize { get; set; } = Numbers.DefaultPageSize;

        public string SortBy { get; set; } = Resources.Strings.DefaultPagingOptionsCustomerSortByProperty;

        public bool SortInDescendingOrder { get; set; } = false;
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string VehicleModel { get; set; }
        public string VehicleManufacturer { get; set; }
        public string VehicleType { get; set; }
        public DateTime VisitFrom { get; set; }
        public DateTime VisitTo { get; set; }
    }
}
