﻿using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Services.Models.InputModels
{
    public class UserViewInputModel
    {

        [Required]
        [EmailAddress]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "FirstName")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "LastName")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "PhoneNumber")]
        public string Phone { get; set; }
    }
}
