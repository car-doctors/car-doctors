﻿using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Services.Models.InputModels
{
    public class VehicleViewInputModel
    {
        [Required(ErrorMessage = "This Field is required.")]
        [RegularExpression("[A-HJ-NPR-Z0-9]{13}[0-9]{4}", ErrorMessage = "Invalid Vehicle Identification Number Format.")]
        public string VIN { get; set; }

        [Required(ErrorMessage = "This Field is required.")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Model must be between 2 and 50 characters long.")]
        public string Model { get; set; }

        [Required(ErrorMessage = "This Field is required.")]
        public int VehicleTypeID { get; set; }

        public int OwnerID { get; set; }

        [Required(ErrorMessage = "This Field is required.")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Manufacturer must be between 2 and 50 characters long.")]
        public string Manufacturer { get; set; }

        [Required(ErrorMessage = "This Field is required.")]
        [RegularExpression("(?:(?:E|A|B|BT|BH|BP|EB|TX|K|KH|OB|M|PA|PK|EH|PB|PP|P|CC|CH|CM|CO|C|CB|CA|CT|T|X|H|Y)[0-9]{4}(?:E|A|B|H|P|X|K|O|C|T|Y){1,2})|(?:E|A|B|BT|BH|BP|EB|TX|K|KH|OB|M|PA|PK|EH|PB|PP|P|CC|CH|CM|CO|C|CB|CA|CT|T|X|H|Y)[0-9]{5,6}|(?:E|A|B|BT|BH|BP|EB|TX|K|KH|OB|M|PA|PK|EH|PB|PP|P|CC|CH|CM|CO|C|CB|CA|CT|T|X|H|Y)(?:E|A|B|H|P|X|K|O|C|T|Y){5,6}", ErrorMessage = "Invalid Registration Plate Number")]
        public string RegistrationPlate { get; set; }

        public int VehicleID { get; set; }
    }
}
