﻿using CarDoctors.Services.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Services.Models.InputModels
{
    public class ServicePaginatedModel
    {
        public double? PriceFrom { get; set; }
        public double? PriceTo { get; set; }
        public double? PriceIs { get; set; }
        public string Name { get; set; }

        public string Currency { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public int Page { get; set; } = Numbers.DefaultPage;

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public int PageSize { get; set; } = Numbers.DefaultPageSize;

        public string SortBy { get; set; } = Resources.Strings.DefaultPagingOptionsServiceSortByProperty;

        public bool SortInDescendingOrder { get; set; } = false;
    }
}
