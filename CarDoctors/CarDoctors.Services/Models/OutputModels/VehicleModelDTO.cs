﻿using CarDoctors.Data.Models;

namespace CarDoctors.Services.Models.OutputModels
{
    public class VehicleModelDTO
    {
        public VehicleModelDTO()
        {

        }
        public VehicleModelDTO(VehicleModel vehicleModel)
        {
            this.Model = vehicleModel.Model;
            this.Manufacturer = vehicleModel.Manufacturer.Name;
            this.VehicleType = vehicleModel.VehicleType.Name;
        }
        public string Model { get; set; }
        public string Manufacturer { get; set; }
        public string VehicleType { get; set; }
    }
}
