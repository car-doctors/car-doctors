﻿using CarDoctors.Data.Models;

namespace CarDoctors.Services.Models.OutputModels
{
    public class ServiceDTO
    {
        public ServiceDTO(Service service)
        {
            this.ServiceID = service.ServiceID;
            this.ServiceName = service.ServiceName;
            this.Price = service.Price;
            this.Currency = service.Currency;
        }
        public ServiceDTO()
        {

        }
        public int ServiceID { get; set; }
        public string ServiceName { get; set; }
        public double Price { get; set; }
        public string Currency { get; set; }
    }
}
