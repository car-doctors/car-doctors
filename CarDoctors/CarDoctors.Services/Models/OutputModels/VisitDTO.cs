﻿using System;

namespace CarDoctors.Services.Models.OutputModels
{
    public class VisitDTO
    {
        //public VisitDTO(Visit visit)
        //{
        //    this.VIN = visit.Vehicle.VIN;
        //    this.ModelMake = $"{visit.Vehicle.VehicleModel.Manufacturer.Name} {visit.Vehicle.VehicleModel.Model}";
        //    this.VehicleType = visit.Vehicle.VehicleModel.VehicleType.Name;
        //    this.Owner = $"{visit.Vehicle.Owner.FirstName} {visit.Vehicle.Owner.LastName}";
        //    this.RegistrationPlate = visit.Vehicle.RegistrationPlate;
        //    this.VisitDate = visit.VisitDate;
        //    this.PickUpDate = visit.PickUpDate;
        //    this.VisitStatus = visit.VisitStatus.Name;
        //}
        public VisitDTO()
        {

        }

        [System.Text.Json.Serialization.JsonIgnore]
        public int VisitID { get; set; }
        public string VIN { get; set; }
        public string ModelMake { get; set; }
        public string VehicleType { get; set; }
        [System.Text.Json.Serialization.JsonIgnore]
        public int VehicleID { get; set; }
        [System.Text.Json.Serialization.JsonIgnore]
        public int OwnerID { get; set; }
        public string Owner { get; set; }
        public string RegistrationPlate { get; set; }
        public string ServicesDone { get; set; }
        public double Price { get; set; }
        public string Currency { get; set; }
        public DateTime VisitDate { get; set; }
        public DateTime PickUpDate { get; set; }
        public string VisitStatus { get; set; }

    }
}
