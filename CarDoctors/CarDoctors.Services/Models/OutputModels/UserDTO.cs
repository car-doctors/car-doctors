﻿namespace CarDoctors.Services.Models.OutputModels
{
    public class UserDTO
    {
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        //public IEnumerable<VehicleDTO> Vehicles { get; set; }
    }
}
