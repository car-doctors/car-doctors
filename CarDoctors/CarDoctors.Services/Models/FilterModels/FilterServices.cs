﻿namespace CarDoctors.Services.Models.FilterModels
{
    public class FilterServices : IFilter
    {
        public double? PriceFrom { get; set; }
        public double? PriceTo { get; set; }
        public double? PriceIs { get; set; }
        public string Name { get; set; }
        public string Currency { get; set; }
    }
}
