﻿using System;

namespace CarDoctors.Services.Models.FilterModels
{
    public class FilterVisits : IFilter
    {
        public int? VehicleID { get; set; }
        public string Email { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
    }
}
