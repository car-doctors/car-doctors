﻿namespace CarDoctors.Services.Models.FilterModels
{
    public class FilterVehiclesByOwner : IFilter
    {
        public int OwnerID { get; set; }
    }
}
