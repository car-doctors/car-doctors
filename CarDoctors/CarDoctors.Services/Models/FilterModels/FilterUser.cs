﻿using System;

namespace CarDoctors.Services.Models.FilterModels
{
    public class FilterUser : IFilter
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string VehicleModel { get; set; }
        public string VehicleManufacturer { get; set; }
        public string VehicleType { get; set; }
        public DateTime VisitFrom { get; set; }
        public DateTime VisitTo { get; set; }
    }
}
