﻿using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Services.Exceptions
{
    public class InvalidPasswordResetException : ValidationException
    {
        public InvalidPasswordResetException(string msg) : base(msg)
        {

        }
    }
}
