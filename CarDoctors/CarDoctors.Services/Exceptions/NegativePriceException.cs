﻿using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Services.Exceptions
{
    public class NegativePriceException : ValidationException
    {
        public NegativePriceException(string msg) : base(msg)
        {

        }
    }
}
