﻿using System;

namespace CarDoctors.Services.Exceptions
{
    public class InvalidCurrencyException : Exception
    {
        public InvalidCurrencyException(string message) : base(message)
        {

        }
    }
}
