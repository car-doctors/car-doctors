﻿using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Services.Exceptions
{
    public class ObjectNotFoundException : ValidationException
    {
        public ObjectNotFoundException(string msg) : base(msg)
        {

        }
    }
}
