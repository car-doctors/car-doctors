﻿using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Services.Exceptions
{
    public class InvaludUsernameOrPasswordException : ValidationException
    {
        public InvaludUsernameOrPasswordException(string msg) : base(msg)
        {

        }
    }
}
