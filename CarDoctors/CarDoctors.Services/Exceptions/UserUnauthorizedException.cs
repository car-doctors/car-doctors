﻿using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Services.Exceptions
{
    public class UserUnauthorizedException : ValidationException
    {
        public UserUnauthorizedException(string msg) : base(msg)
        {

        }
    }
}
