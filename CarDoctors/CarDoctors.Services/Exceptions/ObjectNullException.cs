﻿using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Services.Exceptions
{
    public class ObjectNullException : ValidationException
    {
        public ObjectNullException(string msg) : base(msg)
        {

        }
    }
}
