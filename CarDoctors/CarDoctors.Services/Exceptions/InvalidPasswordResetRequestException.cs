﻿using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Services.Exceptions
{
    public class InvalidPasswordResetRequestException : ValidationException
    {
        public InvalidPasswordResetRequestException(string msg) : base(msg)
        {

        }
    }
}
