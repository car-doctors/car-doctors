﻿using System.ComponentModel.DataAnnotations;

namespace CarDoctors.Services.Exceptions
{
    public class ObjectDuplicateException : ValidationException
    {
        public ObjectDuplicateException(string msg) : base(msg)
        {

        }
    }
}
